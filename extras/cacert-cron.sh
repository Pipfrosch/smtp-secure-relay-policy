#!/bin/bash

# optional method of maintaining an up to date Root Trust Anchor store

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as the root user"
  exit 1
fi

TMP=`mktemp -d /tmp/mtapolicy.XXXXXXXX`
trap "rm -rf ${TMP}" EXIT TERM

pushd ${TMP} > /dev/null 2>&1
umask 022
if [ -f /etc/pki/tls/certs/mozilla-cacert.pem ]; then
  STATUS=`curl -w '%{http_code}' --silent --remote-name --time-cond /etc/pki/tls/certs/mozilla-cacert.pem https://curl.haxx.se/ca/cacert.pem`
else
  STATUS=`curl -w '%{http_code}' --silent --remote-name https://curl.haxx.se/ca/cacert.pem`
fi

[ "x${STATUS}" != "x200" ] && exit

[ ! -d /etc/pki/tls/certs ] && mkdir -p /etc/pki/tls/certs
[ -f cacert.pem  ] && cp -p cacert.pem /etc/pki/tls/certs/mozilla-cacert.pem

popd > /dev/null 2>&1

exit 0
  