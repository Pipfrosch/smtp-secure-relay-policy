Things To Do
============

Security Considerations
-----------------------

Create a SecurityConsideration.md README file containing possible security
considerations related to deploying this project.


Exim Support
------------

Scripts to generate policies for Exim and to harvest mailbox domains from the
Exim log file need to be written.
[Issue #7](https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/issues/7)


MTA-STS Trust Anchor Backend
----------------------------

Design a system to detect which Trust Anchors the X509 certificates used for
MTA-STS policy web servers most commonly point to, so that system admins can be
warned when their CA bundle does not contain one of them.
