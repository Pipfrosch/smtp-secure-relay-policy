<?php

/* PHP script setting. */

/*
  (c) 2019 Michael A. Peters except where otherwise noted.
  MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE
*/

// Database Settings

/* these two probably do not need to be changed unless you are using a different database table name
   or running the database on a different host, which I do NOT recommend. */
$dbname = 'mtastsdb';
$dbhost = 'localhost';

/* these two do need to be changed */
$dbuser = 'dbuser';
$dbpass = 'dbpass';

/* Postfix policymap.php specific settings */

/* Treat MTA-STS testing mode as if it was enforce. I recommend against this. */
$mtaTestingAsEnforce = false;

/* Treat STARTTLS Everywhere testing mode as if it was enforce. Currently I do recommend this
   as STARTTLS Everywhere is not yet publishing *any* policies in enforce mode. However I do
   not feel comfortable setting that as the default, so default is false. */
$starttlsTestingAsEnforce = false;

/* Add policies for domains not being monitored in mboxdomains table but are in the improperdane
   table. When Postfix has a default policy of 'dane' there will be delivery problems with messages
   sent to mailbox domains with improper DANE implementation. Setting this to true will result in
   these domains having a specified policy of either encrypt or may so that DANE validation is not
   attempted even thought the hosts have TLSA records.
   
   The default is true. If you want a slightly smaller policy file, you can change this to false
   and most of the time there will not be any issues, as this only impacts whether or not policies
   are defined for hosts NOT ordinarily monitored within mboxdomains. */
$addBadDane = true;

/* By default when a host has known bad DANE configuration, the generated policy is encrypt. Some
   system administrators may wish to change this to a value of may.*/
$failedDanePolicyMode = 'encrypt';



/* you should NOT need to alter these */

/* MariaDB database uses UTC so do not change this */
date_default_timezone_set('UTC');

/* attempt to connect to the database */
try {
$pdo = new PDO('mysql:dbname=' . $dbname . ';host=' . $dbhost . ';charset=utf8', $dbuser, $dbpass);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}
catch(PDOException $e) {
  print('Database Connection Failure');
  exit;
}

function logMessage($pdo, $type, $context, $message) {
  $sql = "INSERT INTO logmessages(type,mode,context,message, tstamp) VALUES (?, 'web API', ?, ?, ?)";
  $q = $pdo->prepare($sql);
  $arr = array($type, $context, $message, date('Y-m-d H:i:s'));
  $q->execute($arr);
}

function checkDatabaseTableVersion($pdo, $context, $version) {
  $sql = "SELECT COUNT(id) AS count FROM metadata WHERE compatv = ? AND active = 1";
  $q = $pdo->prepare($sql);
  $arr = array($version);
  $q->execute($arr);
  if ($rs = $q->fetchAll()) {
    if (intval($rs[0]->count) === 1) {
      return true;
    }
  }
  logMessage($pdo, 'Error', $context, 'Incorrect database compatibility table version.');
  return false;
}
function checkDatabaseMajorMinorPatch($pdo, $context, $major, $minor, $patch) {
  $return = false;
  $sql = "SELECT majorv, minorv, patchv FROM metadata WHERE active = 1 LIMIT 1";
  $q = $pdo->query($sql);
  if ($rs = $q->fetchAll()) {
    $dbmajor = intval($rs[0]->majorv, 10);
    $dbminor = intval($rs[0]->minorv, 10);
    $dbpatch = intval($rs[0]->patchv, 10);
    if($dbmajor === $major) {
      if($dbminor < $minor) {
        return true;
      }
      if($dbminor === $minor) {
        if($dbpatch <= $patch) {
          return true;
        }
      }
    }
  }
  logMessage($pdo, 'Error', $context, 'Incorrect database major,minor,patch table version.');
  return false;
}

?>