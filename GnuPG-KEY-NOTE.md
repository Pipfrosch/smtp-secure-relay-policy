GnuPG Key Note
==============

This note applies to *my* GnuPG key, it does not apply to the EFF GnuPG that is
used to verify the STARTTLS Everywhere `policy.json` file.

The key is a sign-only key. It will only be used for the purpose of validation
of the JSON file I am distributing to maintain the `daneprovider` table, the
`pkixprivider` table, and the `rootanchor` table.

I may use the same key in the future for similar types of things, but really
what it exists for is to provide confidence to automated scripts that download
files and use them that the files they are downloading are not trojan.

It is not used for automated signing. I manually sign any file signed with the
key, using a detached signature. E.g. to sign this file:

    gpg --digest-algo SHA256 --detach-sig -a GnuPG-KEY-NOTE.md

The e-mail address in the key is currently not valid. I own the domain, but the
user account does not exist and it is possible it never will.

The public key is here in the gitlab project, titled `pipfrosch-gpg.asc`

The fingerprint is hard-coded into the checkDomains.py script.

    [pipfrosch@localhost ~]$ gpg --fingerprint
    /home/pipfrosch/.gnupg/pubring.gpg
    ----------------------------------
    pub   4096R/50452023 2019-02-28 [expires: 2024-02-27]
          Key fingerprint = 24AB 54ED A43B 9063 EE07  B073 E901 B23E 5045 2023
    uid                  Michael A. Peters (Signing Key for Automated File Retrieval) <pipfrosch@deviant.email>

The key has been uploaded to hkp server keys.gnupg.net:

    [pipfrosch@localhost ~]$ gpg --send-keys 50452023
    gpg: sending key 50452023 to hkp server keys.gnupg.net
