#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) 2019 Michael A. Peters except where otherwise noted.
# MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE

# define these to use local database instead of submit by post
#  can also be set at runtime with --dbuser=someuser [ -u someuser ] and --dbpass=somepassword [ -p somepassword ] switches
dbuser = None
dbpass = None

# define for Redis database
#  can also be set at runtime with --redis-pass=whatever [ -r whatever ] switch
redispass = None
# redis prefix
prefix = "mxdomainscan:"

LOGFILE = "/var/log/maillog"

# Define to URL to post to if not using local database
#  can also be set at runtime with --post-url=https://someurl.tld/importEmail.php switch
#  Note this is meaningless if dbuser and dbpass are defined.
postURL = None

# path to certificate authority bundle
#  can also be set at runtime with --pem=/path/to/some/bundle.pem switch
#  Note this is meaningless if dbuser and dbpass are defined.
cabundle = '/etc/pki/tls/certs/mozilla-cacert.pem'

# theoretically nothing below needs modification
import sys, os, getopt, re
import time
import redis
import json
from datetime import datetime

PYV = int(sys.version_info[0])

domainList = ['localhost']
submitList = []
vmode = 'noisy'

# for argv
longswitches = []
shortnoarg = []
shortwarg = []

# switches w/o arguments
longswitches.append("silent")
shortnoarg.append("s")
longswitches.append("help")
shortnoarg.append("h")
longswitches.append("use-current-log")
longswitches.append("delete-cache")
# switches with arguments
longswitches.append("post-url=")
longswitches.append("dbuser=")
shortwarg.append("u")
longswitches.append("dbpass=")
shortwarg.append("p")
longswitches.append("redis-pass=")
shortwarg.append("r")
longswitches.append("pem=")
longswitches.append("logfile=")

shortswitches = ''.join(shortnoarg) + ':'.join(shortwarg) + ':'

ARGVusage = 'extractFromPostfix.py -r <redis password> [-u <marisdb user> -p <mariadb password>] OR [post-url=https://someurl.tld/importEmail.php]' + "\n" + '  [optional args: --pem=/path/to/cabundle.pem --delete-cache --use-current-log --logfile=/path/to/logfile --silent]'

os.environ['TZ'] = 'UTC'
time.tzset()
timestamp = int(time.time())
today = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')

#globals set by functions
dbase = None
connector = None
r = None
SCANLOG = False

# connect to MariaDB
def mariadbConnect():
    global dbase
    global cursor
    import mysql.connector as mariadb
    try:
        dbase = mariadb.connect(user=dbuser, password=dbpass, database='mtastsdb')
    except:
        sys.exit("Database connection failed.")
    cursor = dbase.cursor()

def redisConnect(rpass):
    global r
    r = redis.Redis(
        host='127.0.0.1',
        port='6379',
        password=rpass)

# eats error message and like does does with it.
def outputMessage(mtype, context, message):
    if dbase is None:
        context = None
    if context is not None:
        rightnow = datetime.utcfromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        commit = True
        try:
            cursor.execute('INSERT INTO logmessages(type,context,mode,message,tstamp) VALUES(%s,%s,%s,%s)', (mtype, context, vmode, message, rightnow))
        except:
            commit = False
            print("Could not log message in database.")
        if commit:
            dbase.commit()
    if vmode == "noisy":
        mlist = message.split("|")
        for strng in mlist:
            print(mtype + ": " + strng)

def checkDatabaseVersion():
    if dbase is None:
        return
    dbcompat = '00000100'
    ErrorContext = 'extractFromPostfix.py'
    try:
        cursor.execute('SELECT COUNT(id) FROM metadata WHERE compatv = %s AND active = 1',(dbcompat,))
    except:
        outputMessage('Error', ErrorContext, 'Could not query database table version.|Exiting now.')
        terminalError()
    for row in cursor:
        if int(row[0]) != 1:
            outputMessage('Error', ErrorContext, 'Incorrect database table version.|Exiting now.')
            terminalError()

# stolen from https://stackoverflow.com/questions/2532053/validate-a-hostname-string/2532344
def validateHostname(strng):
    if len(strng) > 255:
        return False
    if strng[-1] == ".":
        strng = strng[:-1]
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in strng.split("."))

def checkIfScanned():
    """Determines whether or not the rotated log has already been scanned today."""
    global SCANLOG
    key = prefix + "logscanned"
    try:
        value = r.get(key)
    except:
        sys.exit("I can not connect to Redis database. Is it running? Is the password correct?")
    if value is None:
        SCANLOG = True
        return False
    if value == LOGFILE:
        return True
    SCANLOG = True
    return False

def useRotatedLog():
    global LOGFILE
    yesterday = timestamp - 86400
    appendString = "-" + datetime.utcfromtimestamp(yesterday).strftime('%Y%m%d')
    LOGFILE += appendString
    if checkIfScanned():
        outputMessage("Notice", None, "The log file " + LOGFILE + " has already been scanned today. Exiting.")
        if dbase is not None:
            cursor.close()
            dbase.close()
        sys.exit(0)

def isBounceDomain(strng):
    bounceList = ['bounce', 'bounces', 'noreply']
    for word in bounceList:
        addedDot = word + "."
        if strng.startswith(addedDot):
            return True
    return False

def getListFromQMGR():
    global domainList
    f = open(LOGFILE, 'r')
    for line in f:
        if re.search("postfix/qmgr\[", line):
            if re.search(" from=<", line):
                fields = line.strip().split()
                if "@" in fields[6]:
                    foo = fields[6].split('@')[1]
                    domain = re.sub('>,', '', foo).strip().lower()
                    if not isBounceDomain(domain):
                        if validateHostname(domain):
                            if domain not in domainList:
                                if not domain.endswith('.localdomain'):
                                    domainList.append(domain)
    f.close()
    domainList.sort()

def getListFromSMTP():
    global domainList
    f = open(LOGFILE, 'r')
    for line in f:
        if re.search("postfix/smtp\[", line):
            if re.search(" to=<", line):
                fields = line.strip().split()
                if "@" in fields[6]:
                    foo = fields[6].split('@')[1]
                    domain = re.sub('>,', '', foo).strip().lower()
                    if validateHostname(domain):
                        if domain not in domainList:
                            if not domain.endswith('.localdomain'):
                                domainList.append(domain)
    f.close()
    domainList.sort()

def checkRedis():
    global submitList
    for hostname in domainList:
        key = prefix + hostname
        try:
            value = r.get(key)
        except:
            sys.exit("I can not connect to Redis database. Is it running? Is the password correct?")
        if value is None:
            r.set(key, today)
            r.expire(key, 60)
            submitList.append(hostname)

# this is only executed on success
def setExpire():
    for hostname in submitList:
        key = prefix + hostname
        # 120 days
        try:
            r.expire(key, 10368000)
        except:
            sys.exit("I can not connect to Redis database. Is it running? Is the password correct?")

def clearHostnameCache():
    try:
        r.set(prefix + 'redistest', 'test')
    except:
        sys.exit("I can not connect to Redis database. Is it running? Is the password correct?")
    for key in r.scan_iter(prefix + "*"):
        r.delete(key)

def detectTrustAnchorBundle():
    global cabundle
    pathList = [cabundle,
        "/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem", # RHEL/CentOS/Fedora location
        "/etc/pki/tls/certs/ca-bundle.crt",                  # Common location
        "/etc/ssl/certs/ca-bundle.crt"                       # Common location
        "/etc/ssl/certs/ca-certificates.crt",                # Common location
        "/usr/share/ssl/certs/ca-bundle.crt",                # Common location
        "/usr/local/etc/openssl/cert.pem",                   # Common OS X Homebrew location
        "/opt/local/etc/openssl/cert.pem"]                   # Common OS X Homebrew location
    for bundle in pathList:
        found = True
        try:
            fh = open(bundle, 'r')
        except IOError:
            found = False
        if found:
            fh.close()
            cabund = bundle
            outputMessage("Notice", None, "Using CA Root Anchor bundle found at " + bundle)
            return
    sys.exit("No suitable Certificate Authority root bundle was found.")

def submitToPost():
    import pycurl
    if PYV < 3:
        import urllib
        from StringIO import StringIO
        buff = StringIO()
    else:
        import urllib.parse
        from io import BytesIO
        buff = BytesIO()
    detectTrustAnchorBundle()
    postData = {'domains': json.dumps(submitList)}
    curl = pycurl.Curl()
    curl.setopt(pycurl.CAINFO, cabundle)
    curl.setopt(pycurl.SSL_VERIFYPEER, 1)
    curl.setopt(pycurl.SSL_VERIFYHOST, 2)
    curl.setopt(pycurl.TIMEOUT, 15)
    try:
        curl.setopt(pycurl.WRITEDATA, buff)
    except:
        curl.setopt(pycurl.WRITEFUNCTION, buff.write)
    curl.setopt(pycurl.POST, len(postData))
    if PYV < 3:
        curl.setopt(pycurl.POSTFIELDS, urllib.urlencode(postData))
    else:
        curl.setopt(pycurl.POSTFIELDS, urllib.parse.urlencode(postData))
    curl.setopt(pycurl.URL, postURL)
    curl.perform()
    RC = int(curl.getinfo(pycurl.RESPONSE_CODE))
    curl.close()
    if RC == 200:
        setExpire()

def submitToLocalDatabase():
    for domain in submitList:
        try:
            cursor.execute('SELECT COUNT(id) FROM mboxdomains WHERE domain = %s', (domain,))
        except:
            sys.exit('Could not count id in mboxdomains')
        N = 0
        for row in cursor:
            N = int(row[0])
        if N == 0:
            try:
                cursor.execute('INSERT INTO mboxdomains(domain) VALUES(%s)', (domain,))
            except:
                sys.exit('Could not insert domain into mboxdomains')
        else:
            try:
                cursor.execute('UPDATE mboxdomains SET lasthit = %s WHERE domain = %s', (today, domain,))
            except:
                sys.exit('Could not update lasthit column in mboxdomains')
    dcount = str(len(submitList))      
    outputMessage("Notice", "extractFromPostfix.py", "Submitted " + dcount + " domains.")
    dbase.commit()
    setExpire()

def main(argv):
    global vmode
    global dbuser
    global dbpass
    global redispass
    global postURL
    global cabundle
    global LOGFILE
    disableRotatedLog = False
    deleteCache = False
    try:
        opts, args = getopt.getopt(argv,shortswitches,longswitches)
    except getopt.GetoptError:
        sys.exit(ARGVusage)
    for opt, arg in opts:
        if opt in ("-s", "--silent"):
            vmode = 'silent'
        elif opt in ("-h", "--help"):
            print(ARGVusage)
            sys.exit(0)
        elif opt in ("-u", "--dbuser"):
            dbuser = arg
        elif opt in ("-p", "--dbpass"):
            dbpass = arg
        elif opt in ("-r", "--redis-pass"):
            redispass = arg
        elif opt in ("--post-url"):
            postURL = arg
        elif opt in ("--pem"):
            cabundle = arg
        elif opt in ("--use-current-log"):
            disableRotatedLog = True
        elif opt in ("--logfile"):
            LOGFILE = arg
            disableRotatedLog = True
        elif opt in ("--delete-cache"):
            deleteCache = True

    if redispass is None:
        sys.exit("No password specified for Redis database.")
    redisConnect(redispass)
    if deleteCache:
        clearHostnameCache()

    usedb = False
    if dbuser is not None:
        if dbpass is not None:
            usedb = True
            mariadbConnect()
            checkDatabaseVersion()
        else:
            sys.exit("You defined a MariaDB user but did not define a password.")
    elif dbpass is not None:
        sys.exit("You defined a MariaDB password but did not define a user.")
    if not usedb:
        if postURL is None:
            sys.exit("No database user/pass configured and no POST URL configured.")

    if not disableRotatedLog:
        useRotatedLog()
    if os.path.exists(LOGFILE):
        getListFromQMGR()
        getListFromSMTP()
        checkRedis()
        if SCANLOG:
            key = prefix + "logscanned"
            r.set(key, LOGFILE)
            r.expire(key, 86400)
    else:
        outputMessage("Notice", None, "Log file " + LOGFILE + " not present.")
        sys.exit(0)

    if len(submitList) > 0:
        if dbase is None:
            submitToPost()
        else:
            submitToLocalDatabase()
            cursor.close()
            dbase.close()
    sys.exit(0)

if __name__ == "__main__":
    main(sys.argv[1:])
