Fixing MTA Client to MX Hop Permanently
=======================================

This is an idealistic dream. On the one hand, it seems like it will not ever be
a reality because of the necessity of backwards compatibility.

On the other hand, as we approach Quantum Computing, current TLS protocols and
cipher suites will become obsolete, they will no longer provide the security
advertised. Radical upgrade to something new will be mandated, an update to
something like this could be done at the same time.

I call it my Magic Unicorn in acknowledgment that it is an idealistic dream,
but I also believe it is possible if industry chooses to support it.

An alternate name I thought about:

__Opportunistic Deprecation of Opportunistic TLS__ ;)

This is not intended to either replace DANE for SMTP or to be an alternative to
DANE for SMTP. Rather, it builds upon DANE for SMTP.

I will not be writing an official proposal to IETF. If this has merit, then I
hope someone who is not a nobody sees the merit and take on that burden.

* [Deprecate the MX Record](#deprecate-the-mx-record)
* -- [DNSSEC Requirement](#dnssec-requirement)
* [MTA Client Requirements](#mta-client-requirements)
* -- [Client Authentication Justification](#client-authentication-justification)
* [SMTP Server Requirements](#smtp-server-requirements)
* [Additional Idea - X509 Whitelisting](#addition-idea-x509-whitelisting)
* [Barriers to Adoption](#barriers-to-adoption)


Deprecate the `MX` Record
-------------------------

Instead of an `MX` Record, what I call a __m2x SRV__ record would be used. This
would allow existing MX hosts to continue to run the current standard while the
transition is made.

I would suggest specifying that when TLS 1.2 is officially obsolete, that is
when the `MX` record should also be officially be made obsolete. Until then, an
SMTP server that accepts mail from the public Internet __MUST__ continue to
implement `MX` records with SMTP servers that listen on port 25 and __MAY__
implement my so-called __m2x SRV__ record.

If `example.net` outsource MX services to Google and Google implements what is
in this recommendation, this is what the `SRV RRset` record might look like:

    _m2x._tcp.example.net.    3600    IN    SRV     30 10 425 alt2.aspmx.l.google.com.
    _m2x._tcp.example.net.    3600    IN    SRV     40 10 425 alt3.aspmx.l.google.com.
    _m2x._tcp.example.net.    3600    IN    SRV     10 10 425 aspmx.google.com.
    _m2x._tcp.example.net.    3600    IN    SRV     20 10 425 alt1.aspmx.l.google.com.
 
The `SRV` record is defined in [RFC 2782](https://tools.ietf.org/html/rfc2782).
 
In this example, I used `_tcp` for the `_proto` part of the record owner. I
understand Google is working on some rather wicked magic with UDP in the
context of HTTPS, I do not fully understand what they are doing, but if it is
also applicable to SMTP then I do not see why a __m2x SRV__ record would have to
be restricted to TCP.

In this example, I used the `priority` part of the record data the same way
that the current `MX` record specifies priority. That is what I recommend.

In this example, I always specified a `weight` of `10`. It is my *opinion* that
MTA clients are allowed to ignore the weight. However they *may* take weight
into consideration when they pick between two or more records of the same
priority, if they want to add the code to do so. I personally do not see much
value to the weight part of the field, at least not in this context.

In this example, I always specified `425` for the `port` part of the record
data. Given that a port number __must__ be specified in an `SRV` record, I do
not believe IANA needs to specify a specific port for this. However, if this
magic unicorn proposal has merit, I would recommend the requirement that a port
under 1024 be used and that ports 25, 465, and 587 be forbidden from use in the
__m2x SRV__ record.

I would not object to a reserved port, I just do not see it as needed since the
`SRV` record already allows specification of port to connect to.

### DNSSEC Requirement

In order to be valid, the `RRset` __MUST__ be signed by DNSSEC with a chain of
trust that leads to the root zone.

The justification for this: When trying to implement python code to discover
and translate MTA-STS ([RFC 8461](https://tools.ietf.org/html/rfc8461))
policies into Postfix policies, I ran across several problems:

* Sometimes the HTTPS server hosting the policy was not reachable.
* Sometimes the HTTPS server hosting the policy was behind an appliance
  firewall that blocked libcurl.
* Sometimes the policy was malformed
* Sometimes the policy was clearly stale
* Sometimes the policy was updated but the serial number in DNS was not

It is my humble *opinion* that HTTPS is not reliable enough to secure the `MX`
hosts. Too many things can go wrong causing that mechanism to fail.

When the `SRV RRset` is not authenticated with DNSSEC, at least in the context
of __m2x SRV__ it should be ignored.


MTA Client Requirements
-----------------------

Obviously an MTA Client implementing this magic unicorm __MUST__ have the
ability to validate DNSSEC.

Furthermore, it should be required to implement TLS 1.3 (or newer) so that only
AEAD cipher suites with forward secrecy are used. The client __MUST__ refuse to
connect if the specified server does not support a minimum of TLS 1.3.

It is possible to do things safely with TLS 1.2 but since my hope is for the
`MX` record to become obsolete when TLS 1.2 is deprecated, this experiment
should require TLS 1.3 or newer.

Given that clear channel communication will not be allowed, the `STARTTLS`
command should not be used, similar to submission over port 465.

The MTA client should be required to authenticate to the server with a X509
certificate. The certificate will be secured with a record *similar* to the
`TLSA` records currently used by DANE for SMTP
([RFC 7672](https://tools.ietf.org/html/rfc7672)):

    _mta._client.reversedns.tld.    3600    IN    TLSA    3 1 1 C4B5183E1564D2A18E084A2374D2B73A67B6105C74999F88A31EE71715584318

Notice that instead of a `_port._proto` number at the beginning of the record
owner, it uses `_mta._client`. This is because specifying a port does not make
sense in the context of a client. The `reversedns.tld.` references the full
reverse dns of the IP address the MTA client connects from, e.g. for
`104.200.18.67 / 2600:3c00::f03c:91ff:fe56:d6a2` the record owner would be
`_mta._client.mail.domblogger.net.`.

If the `TLSA` specification does not allow that, then perhaps a `TXT` record
could be used instead, I do not particularly see a need to a new record type
being invented other than it would make it easier for zone files to be
validated before signing and deployment, reducing incorrect records.

If a new record type does need to be invented for client certificate
authentication, I would suggest that it be generic in scope rather than
specific to SMTP so that it can be re-purposed to other applications.

The `RDATA` in my opinion should otherwise be identical to what is already
required for `RDATA` in the DANE for SMTP specification, specifically the
`TLSA Certificate Usage` should be restricted to `DANE-TA` or `DANE-EE` (see
[RFC 7218](https://tools.ietf.org/html/rfc7218#section-2.1)). Certificate
authorities should not be needed for DANE secured client certificates, so
servers that validate a DANE secured client certificate should not need to
perform PKIX validation.

When the SRV record DNSSEC validates, the MTA client can only connect to a
specified SMTP server if the specified server has a DNSSEC validating
`TLSA RRset` and may only only connect if at least one TLSA record in the set
complies with the DANE for SMTP specification, with the noted exception that
the port number is whatever port is specified in the __m2x SRV__ record rather
that port 25.

When the connection is established, the client __MUST__ send the X509
certificate that corresponds with the `_mta._client` record and the client
__MUST__ verify that the SMTP server sent an X509 certificate that validates
against the TLSA record.

Currently I would suggest the client __MUST__ revert to using current standard
with the `MX` record (which may have different host names) if the client can
not validate the __m2x SRV__ record but if it can validate the __m2x SRV__
record then the client __MUST__ bounce messages if __m2x SRV__ servers can not
be connected to for other reasons.

The `MX` record would remain only for clients that do not support __m2x SRV__
or for cases when the __m2x SRV__ record itself does not validate.

1. Does __m2x SRV__ `RRset` exist? No? Use `MX RRset`. Yes? proceed to 2.
2. Does __m2x SRV__ `RRset` DNSSEC validate? No? Use `MX RRset`. Yes? proceed
   to 3.
3. Do hosts in __m2x SRV__ `RRset` have valid `TLSA` records? No? Bounce. Yes,
   proceed to Step 4.
4. Do hosts in __m2x SRV__ `RRset` support TLS 1.3+? No? Bounce. Yes, proceed
   to step 5.
5. Does X509 certificate presented by server match the `TLSA RRset`? No?
   Bounce. Yes, proceed with connection and deliver message.

Yes, this is basically what happens with a Postfix `dane-only` policy, except
that policy does not validate the `MX RRset` nor require a validating client
X509.

What I am hoping for the future with the deprecation of `MX` records is
mandatory DANE in the MTA Client to MX hop, requiring modern TLS, so that
Opportunistic TLS becomes a thing of the past.


### Client Authentication Justification

In my personal experience administering small mail servers, the vast majority
of spam and scam e-mail originates from hosts that are compromised. The way the
industry fights this is with over-zealous IP based DNS blacklists that block an
entire subnet when one host on the subnet sends spam.

Even though my hosts NEVER send spam and only send a relatively small volume of
e-mail, I am continually placed on blacklists because it is not economically
feasible to purchase my own subnet, and with the shortage of IPv4 address space
it would be irresponsible for me to do so.

Most small mail administrators I know get around this by relaying their
outgoing traffic through Google. However that is leading to centralization of
e-mail services and there are serious privacy implications when such a high
percentage of e-mail goes through SMTP relays controlled by companies that
literally make billions from user tracking.

It is especially frustrating because Spamhaus, the blacklist most commonly
used and that I most commonly end up one, does not even acknowledge my support
email requests to them asking how I can prevent my servers from ending up on
their blacklist. They simply __never__ even bother to acknowledge that they
even received my e-mail.

It is my hope that by requiring the MTA client to validate its outgoing
connection with an X509 certificate that validates via DANE, IP blacklists
will no longer be necessary.

It is unlikely that compromised servers will have a Reverse IP address that
matches the `HELO/EHLO` and even more unlikely that they will have the needed
record in DNS that allows the server to authenticate the X509 certificate they
present.

A DANE authenticated Client MTA certificate would authenticate to the SMTP
server that the connecting client is authorized by the zone to relay email
using SMTP.

Instead of IP based blacklists, zone based blacklists could be used. This
would mean spammers would have to frequently register new domains, sign their
zone with DNSSEC, and set up valid Reverse IP addresses. Some will do that but
I believe it will radically reduce the amount of spam relayed through
compromised hosts where they do not control the zone file.


SMTP Server Requirements
------------------------

The SMTP server obviously must be in a zone that is DNSSEC signed and must have
a valid DANE for SMTP record that corresponds with the X509 certificate used.

The SMTP server (in the context of __m2x SRV__) __MUST__ require a minimum of TLS
1.3 and __MUST__ require the client presents an X509 certificate that validates
against the previously mentioned `_mta._client.reversedns.tld.` record.

The SMTP server __MUST__ have an IPv6 `AAAA` record. Presently it __SHOULD__
also have an IPv4 `A` record.

The point of that requirement is to help move the Internet forward as the IPv4
address space becomes saturated.


Addition Idea - X509 Whitelisting
=================================

Self-signed client X509 certificates should always be allowed. However, ACME
Internet Services could offer a certificate signing service. A host agrees to
have strict policies against relaying spam / scam / malware and procures a
signed certificate from ACME Internet Services to use as their client X509
certificate.

SMTP servers could then see the certificate is signed by ACME and allow the
mail to bypass any IP based spam filtering it performs.

If the host breaks their promise and starts behaving badly, ACME then revokes
the certificate.

This would require OCSP validation, preferably with certificates that are
required to OCSP staple with a maximum 24 hour TTL for the response, and it
would require the SMTP server to choose which root certificates it trusts for
this. For example, trusting a free or cheap CA that does weak validation of the
entity behind the CSR would be a bad idea.

But this would be attractive to me, if servers implemented it I would probably
be separated from some of my cash if something like this was implemented.


Barriers to Adoption
====================

Before this magic unicorn can become a reality, DNSSEC deployment __MUST__
increase. Now that P-256 is broadly supported by DNSSEC validation software
and Ed25519 is on the near horizon, DNSSEC adoption is becoming easier, the
Zone Signing Keys do not need to be rotated as often to remain small in size
and still be secure.

If the `_mta._client.reversedns.tld` can not be done with an `TLSA` record but
requires a new DNS record type, that needs to be standardized before this can
become a reality, though a `TXT` record could be used. The only thing that
scares me about using a `TXT` record, invalid `RDATA` would still pass zone
file validation which could result in some of the same problems MTA-STS has
where typos in the policy file are deployed.

SMTP server software and MTA client software will need to be updated. However
as no changes to SMTP itself are necessary, those changes are minor.

For Postfix as a compliant SMTP server, almost everything could be done just
by configuration withing the `master.conf` file, I suspect the only change it
would need is the ability to DANE validate the client certificate.

For Postfix as a compliant MTA client, it would need code to request and then
validate the specified `SRV` RRset, it already has the code needed to DANE
validate the server, send a client certificate, and require a specific minimum
TLS version. It also would have to be able to fall back to requesting the `MX`
record if the `SRV` record fails to validate or if the servers specified in the
`SRV` record fail to validate (or do not support TLS 1.3)

I am not suggesting either would be a fast weekend of code, but I am suggesting
that most mail servers that already support DANE could probably be ported to
implement this in a Google Summer of Code scale project.

Another barrier to adoption, MTA clients would require a little more
configuration before they are capable of relaying messages to __m2x SRV__
servers, specifically they will require Reverse DNS be properly configured,
they will require a DNSSEC validating zone the Reverse DNS falls within, and
they will require a fingerprint of their client X509 certificate in DNS.

I see no reason why typical WordPress blogs could not continue relaying their
messages through a service if they do not want to take those steps. Those steps
will however reduce spam and malicious scam mail.
