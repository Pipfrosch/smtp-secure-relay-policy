SMTP Secure Relay Policy
========================

At least in the United States, there is a stranglehold on the e-mail market.
What was designed to be a distributed federated system is now largely
consolidated under the control of three companies- Yahoo (owned by Verizon),
Google, and Outlook (Microsoft).

The fact that the vast majority of e-mail travels through servers those three
companies own is a serious privacy concern, not just with the content of the
messages, but the tracking of the metadata associated with the messages, who
people communicate and network with.

Yet so many companies feel like they have to use them for e-mail services. To
which I would like to reply with this quote:

> “Stop trying to navigate systems of power and start building your own
> power.” -- Alexandria Ocasio-Cortez

To run your own mail services rather than outsourcing to the large providers,
it should be done as securely as possible. The SMTP protocol is old and easy to
attack, but it can be considerably hardened.

This project provides a tool that can be used to discover security policies
that should be used in the SMTP hop between an MTA client and an MX server.

The target of this project is SMTP system administrators who wish to protect
their outgoing mail from both passive and active malicious attackers that are
prevalent on the Internet.

In this document, I refer to the domain name after the `@` in an e-mail address
as a ‘Mailbox Domain’ and an MTA client that communicates with the incoming
Mail Exchange (MX) servers for a Mailbox Domain as an ‘SMTP Relay’.

There are three modes of communication an SMTP Relay can use to communicate
with an MX server:

1. __Clear Channel__: No encryption is used for the communication. Anyone who
  is listening to the network traffic can see and often modify the traffic.
2. __Encrypted Channel__: The communication uses Transport Layer Security to
  encrypt the communication but does not attempt to authentication the
  destination server really is an authorized MX server for the destination
  domain.
3. __Secure Channel__: The communication uses Transport Layer Security to
  encrypt the communication using an X509 certificate that the SMTP Relay has
  validated as belonging to an MX server authorized to receive email on behalf
  of the mailbox domain.

Traditionally SMTP uses what is called ‘Opportunistic TLS’. Opportunistic TLS
attempts to use the second method above, Encryption without Authentication, but
falls back to Clear Channel without either Encryption or Authentication, if an
encrypted channel can not be established for any reason. This is less than
ideal.

* [Policy Detection Software Requirements](#policy-detection-software-requirements)
* [SMTP Relay Software Requirements](#smtp-relay-software-requirements)
* -- [Postfix](#postfix)
* [Project Description](#project-description)
* [DANE Detection](#dane-detection)
* -- [DANE Policy Note](#dane-policy-note)
* -- [DANE Fail List](#dane-fail-list)
* [MTA-STS Detection](#mta-sts-detection)
* [PKIX Provider Detection](#pkix-provider-detection)
* [STARTTLS Everywhere Detection](#starttls-everywhere-detection)
* [Policy Overrides](#policy-overrides)
* [External Resource Retrieval](#external-resource-retrieval)
* [Mailbox Scanning Logic](#mailbox-scanning-logic)
* -- [Bogus Domains](#bogus-domains)
* -- [High Volume Scaling](#high-volume-scaling)
* [Message Logging](#message-logging)
* -=-=-=-
* [__POLICY STANDARDS__](#policy-standards)
* [DANE for SMTP (IETF Standard)](#dane-for-smtp-ietf-standard)
* [MTA-STS (IETF Standard)](#mta-sts-ietf-standard)
* -- [MTA-STS Policy Retrieval Issue](#mta-sts-policy-retrieval-issue)
* -- [MTA-STS Testing Mode](#mta-sts-testing-mode)
* [STARTTLS Everywhere](#starttls-everywhere)
* -- [STLSE Testing Mode](#stlse-testing-mode)



Policy Detection Software Requirements
--------------------------------------

The host running this project needs to be running a local DNSSEC enforcing
Recursive Resolver. I recommend
[Unbound](https://nlnetlabs.nl/projects/unbound/about/) but others work too.

Refer to `Documentation/UnboundHowto.md`

The host running this project needs to have a fairly complete Certificate
Authority bundle in a single PEM encoded file. This is usually provided by
your operating system vendor.

Refer to `Documentation/PKIX-ChainOfTrust.md`

The host running this project needs to running a Redis (`key => value`)
database.

Refer to `Documentation/RedisHowto.md`

The host running this project needs to be running a MariaDB Database. I am
using the 10.2.x series but I *suspect* the older 5.5 series will work too.

Refer to `Documentation/DatabaseSetupHowto.md` and
`Documentation/DatabaseTables.md`

The host running this project needs to have Python and several modules
installed. The Python versions this project is tested with are Python 2.7.5 and
Python 3.4.9 as packaged for CentOS 7 with EPEL.

Refer to `Documentation/DatabaseSetupHowto.md`

If the host running this project is different than the SMTP Relays that will be
implementing the generated the policies, then a web server with PHP is needed
to both provide an API by which new domains to monitor can be submitted and for
the generation of policy files.

Refer to `Documentation/DatabaseSetupHowto.md`

If the host running this project is the same as the SMTP Relay and it does not
need to provide services to other SMTP Relays, then a command line PHP
interpreter is needed but a web server is not.


SMTP Relay Software Requirements
--------------------------------

An SMTP Relay enforcing policies generated by this project needs to be running
a local DNSSEC enforcing Recursive Resolver. I recommend
[Unbound](https://nlnetlabs.nl/projects/unbound/about/) but others work too.

Refer to `Documentation/UnboundHowto.md`

An SMTP Relay enforcing policies generated by this project needs to have a
fairly complete Certificate Authority bundle. Whether it needs to be in a single
PEM encoded file or not depends upon your SMTP Relay software. This is usually
provided by your operating system vendor.

Refer to `Documentation/PKIX-ChainOfTrust.md`

The SMTP Relay enforcing policies generated by this project running this
project needs to running a Redis (`key => value`) database.

Refer to `Documentation/RedisHowto.md`

### Postfix

Currently only policies are generated that work with Postfix. Exim support is
planned but has not yet been started.

Refer to `Documentation/PosfixRelayHowto.md`


Project Description
-------------------

This project continually attempts to discover the security capabilities of the
mailbox domains your SMTP Relays are likely to need to communicate with so that
secure policies specific to the mailbox domains can be generated allowing for
authenticated secure channel communication when authentication of the remote MX
server is possible, and non-authenticated encrypted communication when there is
reason to believe the destination does in fact support encryption.

While this project *can* be run on am SMTP Relay, it is actually intended to be
run from a different host with one or more SMTP Relays submitting domains to
be monitored to the project and retrieving secure policy definitions from it.

It is important to note the code in this project makes no actual attempt to
connect to MX servers. Security policies are discovered using a combination of
what a mailbox domain has published in DNS or via a policy served from either
an MTA-STS policy server or the
[STARTTLS Everywhere](https://starttls-everywhere.org/) initiative.

The heart of this project is a MariaDB database that stores information about
a mailbox domain and the `checkDomains.py` script that discovers the security
policies that should be applied when communicating to the MX servers that
accept e-mail on behalf of a mailbox domain.

When the `checkDomains.py` script detects the capabilities of a mailbox domain,
it starts with DNS.

DANE Detection
--------------

The first thing it does is check to see whether or not the zone for the
`MX RRset` defining what hosts accept e-mail for the domain is a DNSSEC signed
zone.

[DNSSEC](https://www.internetsociety.org/deploy360/dnssec/) is by far the most
secure mechanism a mailbox domain can use to allow SMTP Relays to validate the
`MX RRset` is authentic and has not been altered by a malicious actor.

When the mailbox domain is protected by DNSSEC, the hosts specified within the
`MX RRset` are then checked to see if they offer DANE for SMTP validation.

DANE is a standard mechanism by which the fingerprint of the X509 certificate
the MX server uses is published in DNS, secured by DNSSEC. When implemented, an
SMTP Relay will refuse to connect to an MX server that is not using an X509
certificate that matches the validated published fingerprint.

When the `MX RRset` is not protected by DNSSEC, then the MX hosts are __not__
checked for DANE support. The vast majority of MX hosts are not protected by
DANE, only checking when the `MX RRset` is DANE protected greatly reduces the
number of DNS queries that are made.

However the hosts in the `MX RRset` are compared against MX hosts provided by
commercial MX service providers that do provide DANE secured MX hosts, and will
be noted as such if they match so that DANE validation can be used.

As the `MX RRset` in this case is not authenticated, it is *possible* that a
fraudulent `MX RRset` is being used to compare with the these known providers
but there is little point in an attacker doing this.

An attacker could *possibly* prevent discovery that a mailbox domain does use
a MX server provider that provides DANE secured MX hosts, but that result would
be no different than if the check was not performed. An attacker could cause a
mailbox domain to have a policy that requires DANE but the policy does not have
an impact on what MX hosts are used, so at most this would result in bounced
messages if the real MX hosts are not DANE capable.

### DANE Policy Note

Many SMTP Relays are very capable of discovering DANE support themselves. When
those relays are running in a mode to auto-detect DANE support, policies that
are generated that specify DANE is required should be filtered out as they will
be detected. However sometimes those relays are using a default policy (such as
`encrypt` mode in Postfix) that prevent auto-discovery of DANE policies. In
those cases the DANE policies discovered by this project should be applied.

### DANE Fail List

One of the features of this project, it makes automatic use of the
[DANE Fail List](https://danefail.org/) so that mailbox domains with bad DANE
records that are the result of system administrator configuration errors are
not flagged as domains with DANE policies but instead are flagged either with
a policy that requires Encrypted Channel or Opportunistic TLS (depending upon
your preference).


MTA-STS Detection
-----------------

When a mailbox domain does not a have a DANE policy discovered for it, the
`checkDomains.py` will attempt to discover whether or not the mailbox domain
has an applicable MTA-STS policy.

If the necessary DNS record exists indicating that there is a MTA-STS policy
then the `checkDomains.py` script will attempt to retrieve the MTA-STS policy
from the secure server where it is hosted so that the policy can be applied.

When a policy is found, the policy is cached in the database so that it does
not need to be retrieved again until the cache has expired, at which point it
is only retrieved again if the specified serial number in DNS has changed.

Note that when the `TXT RRset` from DNS that indicates whether or not a mailbox
domain has an MTA-STS policy is not protected by DNSSEC it is possible for the
record set to be fraudulent.

The worst the attacker could do is prevent the *discovery* that a policy
exists, that record has no bearing on the policy itself. This would only be
temporary as about every twelve hours, domains without a MTA-STS policy are
checked for one.

An attacker can also potentially use a DoS attack to try to prevent the
retrieval of the policy. Again, at most this would delay the discovery of the
policy.

Once a policy is discovered it is cached for the length of time specified in
the securely retrieved policy to cache it, so the above attacks would not work
when a securely retrieved policy is already cached. Typical cache life is 30
days, but some are shorter and some are longer. When a cache does expire, if
the serial number in DNS has not changed it is not retrieved again, the cache
just starts over.

If the `MX RRset` is not protected with DNSSEC it too could be fraudulent.
These domains are compared with the domains in the securely retrieved policy
file, so at most the attacker could exclude some valid domain names, and it
would only be temporary as the `MX RRset` is re-retrieved every twelve hours.


PKIX Provider Detection
-----------------------

When a mailbox domain does not have a detected DANE or MTA-STS policy, the
`checkDomains.py` script check of hosts in its `MX RRset` against commercial
MX service providers that are known to provide quality TLS service with X509
certificates that validate against the PKIX Chain of Trust.

When discovered, the mailbox domain is noted as using the provider so that
a secure policy can be generated for the mailbox domain.

It should be noted that if the `MX RRset` for a mailbox domain is not protected
with DNSSEC, it is possible a fraudulent record set is being used.

Such an attack could prevent the discovery of a secure policy for that mailbox
domain but that would be no different than if we did not try to discover the
policy.

Such an attack could also result in a secure policy that results in your SMTP
Relay refusing the connection if the MX hosts it tries to send the messages to
do not have valid certificates that match the domain names in the policy, but
that is only beneficial to an attacker if they are also able to send a
fraudulent `MX RRset` to your SMTP Relay *and* have either compromised that
service provider or have a fraudulently signed X509 certificate that matches
the MX hostname(s) the provider uses. A state-level attacker may be able to
pull that off, but few others could, and it would be more difficult to pull off
than if no policy at all was generated.


STARTTLS Everywhere Detection
-----------------------------

If a secure policy has not already been detected by one of the previously
mentioned methods and the mailbox domain has a secure policy listed with the
STARTTLS Everywhere project, then the policy from the STARTTLS Everywhere
project will be applied.

Again it should be noted that if the `MX RRset` for a mailbox domain is not
protected with DNSSEC, it is possible a fraudulent record set is being used.

The most such an attack can do is reduce the number of domain names that are
considered valid for the PKIX verified X509 certificate that the SMTP Relay
will require when it connects to deliver mail. It can not add domain names that
do not match the policy.


Policy Overrides
----------------

Several database tables exist that allow for overriding the detected policies
when generating a policy file. These tables are described within the
`Documentation/DatabaseTables.md` file.

With the exception of the `improperdane` table which is maintained by the Dane
Fail List that is periodically retrieved, no mechanism is in place for
automatically adding domains to those tables. Automatically adding domains to
those tables provides a potential attack surface for downgrading the generated
policy. As those override tables should very rarely be needed, with the
exception of the `improperdane` table, it did not seem worth the risk of
exposing an attack surface to automate the addition of any domains to those
tables.


External Resource Retrieval
---------------------------

About every twenty-six hours, the `checkDomains.py` script will attempt to
retrieve three external resources:

* JSON file for Commercial MX Providers (see `Documentation/MX-Providers-JSON.md`)
* JSON file for STARTTLS Everywhere policies (see https://starttls-everywhere.org/)
* DANE Fail List (see https://danefail.org/)

All three resources are retrieved using TLS requiring X509 certificates that
PKIX validate.

Additionally, the two JSON files must be validated against PGP signatures using
keys that match fingerprints hard-coded in the `checkDomains.py` script.


Mailbox Scanning Logic
----------------------

The `checkDomains.py` script is intended to be run once an hour as an automated
`cron` job. Each time it runs, it will process up to just over one-sixth of the
total number of mailbox domains it knows about.

The actual query for domains to process asks for domains that have never been
processed, have successfully been processed but not within the last twelve,
hours, or for which processing failed during the last attempt.

When it is first set up, it will take about six hours before all of the initial
domains have been processed. You can speed that up if you wish by manually
running the script six times.

As new domains for monitoring are submitted by your SMTP Relay, they will have
priority for processing over domains that have already been processed. It will
not take long until the number of freshly submitted domains is always less than
one-sixth of the total number after they are submitted, resulting in them being
scanned for security policies within an hour.

### Bogus Domains

A common tactic spammers use to try and get past spam filters is to register
valid domain names to spam from. They often usually use fraudulent information
when registering the domain or otherwise violate the policy of the registrar,
resulting in the domain name being revoked.

These domains will end up in the database wasting resources when they no longer
resolve.

When the `checkDomains.py` script comes across a mailbox domain it can not
resolve, the failure is counted. The higher the count, the lower the priority
it has in processing so that domains that are not resolving do not displace the
processing of freshly added domains or domains that do resolve.

After 36 consecutive failed attempts, a domain is removed from the database.
That usually takes two to three days which should be plenty of time for a
legitimate mailbox domain simply having DNS problems to resolve its issues.

### High Volume Scaling

The `checkDomains.py` script processes domains one at a time. For systems with
tens of thousands of domains to monitor, it is possible that is not a workable
method.

If there is a need, I will work on a mechanism that allows batches of domains
to be scanned in parallel allowing for much better scaling.


Message Logging
---------------

The scripts often are automated and do not output to the screen. Messages that
may need system administrator attention are logged in the database. On Sunday
(UTC) the `checkDomains.py` script will attempt to email the previous weeks
logs to the system administrator.

You can configure who the email is sent to, and you can also turn it off.


POLICY STANDARDS
================

A brief description of the policy standards used:


DANE for SMTP (IETF Standard)
-----------------------------

DANE for SMTP leverages DNSSEC to provide a mechanism by which the X509
certificate can be securely validated regardless of whether or not it is
signed by a Certificate Authority that the SMTP Relay happens to trust.

DANE in the generic sense is defined in
[RFC 6698](https://tools.ietf.org/html/rfc6698), updated by
[RFC 7218](https://tools.ietf.org/html/rfc7218) and
[RFC 7671](https://tools.ietf.org/html/rfc7671).

DANE for SMTP is defined in [RFC 7672](https://tools.ietf.org/html/rfc7672).

Other than hard-coding a public key (or certificate) fingerprint in your
configuration file, this is probably the most secure way of validating a TLS
connection with an MX server.

Nutshell, it uses the DNS system to allow discovery of the expected fingerprint
and leverages DNSSEC to securely validate the expected fingerprint from DNS has
not been manipulated by an attacker.


MTA-STS (IETF Standard)
-----------------------

MTA-STS leverages the HTTPS protocol to secure the MX records. Within the
context of Postfix this allows for a more secure policy of `secure` rather than
`verify` to be used, as the MTA-STS policy allows for secure discovery of MX
hosts. Any records in the `MX RRset` that do not validate against the securely
retrieved policy are not trusted, but records that match can be trusted.

While MTA-STS is not as secure as DANE for SMTP (in my *opinion*), it also does
not have the same technical cost of requiring DNSSEC to provide a validating
Chain of Trust.

MTA-STS is defined in [RFC 8461](https://tools.ietf.org/html/rfc8461).

In a nutshell, a mailbox domain uses a DNS `TXT` record to announce that it
implements MTA-STS but the DNS system is not used to define a policy. The DNS
record simply specifies that MTA-STS is being used and defines a serial number
for the current policy.

The policy itself consists of a mode (`none`, `testing`, or `enforce`), one or
more definitions for the MX hosts, and a cache-life. This policy is contained
in a text file at a specific path on a secure web server.

When the DNS `TXT` record exists, a client can then query the secure server to
discover the policy. Upon a successful query of the policy, it will cache the
policy for up to the number of seconds defined in the policy to cache it for.

The client can optionally check the DNS record more frequently than that to see
if the serial number has changed and attempt to fetch the new policy if it has
changed, or the client can wait for the policy cache to expire before it looks
at the DNS record to see if there is a new serial number.

When the policy mode is set to either `testing` or `enforce` then an MTA Relay
should attempt to only use MX servers that match what is in the policy, and it
should require that the MX servers present X.509 certificates that have a
hostname that matches the certificate and are signed by a Certificate Authority
that the SMTP Relay trusts.

If none of the MX servers match the policy or if the X.509 certificates do not
validate, the SMTP Relay *may* send a notification to the mailbox domain using
the TLSRPT specification ([RFC 8460](https://tools.ietf.org/html/rfc8460)). In
`testing` mode, the SMTP Relay is *suppose* to connect anyway, but in `enforce`
mode, the SMTP Relay should drop the connection and try again later.

### MTA-STS Policy Retrieval Issue

Some MTA-STS policies are served from web servers that aggressively block bots.
Policies on those servers may not be retrievable.

As far as I am concerned, those are mis-configured policies.

### MTA-STS Testing Mode

According to [RFC 8461](https://tools.ietf.org/html/rfc8461), when the mode is
specified as `testing` an SMTP Relay *may* make use of TLSRPT to notify the
mailbox domain of a validation problem but is suppose to deliver the message
anyway.

How to achieve that with a Postfix policy map is not obvious to me, as Postfix
itself does not have built-in MTA-STS support.

Instead what I do is by default, MTA-STS `testing` mode translates to a Postfix
security level of `encrypt` for that mailbox domain. The MX hosts that passed
the policy test are then listed the same way they would be with `secure` mode
but I am not sure whether or not Postfix will honor that or ignore it.

In either case, a TLS 1.2 connection is still required, but no attempt is made
to validate the X.509 certificate since a mode of `testing` specifically says
to continue with the connection even if the certificate does not validate.

If you want to treat MTA-STS policies in `testing` mode as if they were in
`enforce` you can change the line

    $mtaTestingAsEnforce = false;

to a value of `true`, resulting in a policy that technically violates the RFC.
I do not recommend making that change, although the vast majority of MTA-STS
policies I have seen that are in `testing` mode would in fact work in
`enforce`.


STARTTLS Everywhere
-------------------

The [STARTTLS Everywhere](https://starttls-everywhere.org/) project is a
project of the EFF that is intended to help promote better security within the
SMTP world.

The project came after DANE for SMTP but it predates MTA-STS and was intended
to serve the same purpose, to provide a mechanism by which SMTP Relay admins
could identify mailbox domains that are committed to providing TLS service
using X.509 certificates that validate through the PKI Chain of Trust.

Even though MTA-STS provides almost identical function in a now IETF endorsed
fashion, the STARTTLS Everywhere project still has incredible value:

A) Most SMTP servers do not directly support MTA-STS and the requirements for
querying a web server and caching policies makes it seem unlikely to me that
many will. Philosophically I prefer the mechanism of MTA-STS as it leverages
DNS as a distributed database that scales well, however it is a lot easier for
system administrators to query STARTTLS Everywhere to get a centrally maintained
list of mailbox domains that should require validating TLS.

B) The STARTTLS Everywhere project provides a single place where we (System
Administrators) can get a decent list of mailbox domains run by security-minded
system administrators so we can sniff appropriate DANE or MTA-STS support even
before any of our users attempt to send an e-mail to the domain.

### STLSE Testing Mode

Presently (March 11, 2019) every single STARTTLS Everywhere policy is listed as
being in `testing` mode, I do not know when they will start switching some
domains to `enforce`.

By default, `testing` mode is treated the same way it is treated for MTA-STS in
`testing` mode, but since every domain is listed that way, I do recommend that
the

    $starttlsTestingAsEnforce = false;

in the `settings.inc.php` file be changed to `true`, at least until that
project starts serving some domains that way.
