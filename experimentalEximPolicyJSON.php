<?php

/* DO NOT USE
 *  When read for use, the filename will change and a readme will appear.
 *
 * The point of this is to generate a JSON file with policies that can be
 *  parsed in a similar fashion as what is described at
 *  https://github.com/Exim/exim/wiki/starttls-everywhere
 *
 * The JSON format almost certainly will go through radical changes before
 *  it gets to a point where that is possible.
 *
 * Four policy modes for a mailbox domain:
 *
 *  DANE (Secure TLS via DANE validation)
 *  PKIX (Secure TLS via PKIX validation)
 *  encrypt (TLS Required but MX not authenticated)
 *  opportunistic (Same as encrypt if MX SMTP supports it, otherwise clear channel)
 *
 * Two strength levels to describe the TLS:
 *
 * high (Requires TLS 1.2+ with high quality cipher suites)
 * medium (Requires TLS 1.0+ with medium quality cipher suites)
 */

// set this to the full path of wherever you put the settings.inc.php file
//  if it is put in different directory than this script.
$settingsFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.inc.php';

if(php_sapi_name() === 'cli') {
  $phpcli = true;
} else {
  $phpcli = false;
}
if(! file_exists($settingsFile)) {
  if($phpcli) {
    print ('Could not locate settings file.' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
require($settingsFile);

if(! isset($pdo)) {
  if($phpcli) {
    print('No PDO object. This should not have happened' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
if(! checkDatabaseTableVersion($pdo, 'importEmail.php', '00000100')) {
  if($phpcli) {
    print('Incorrect Database Table Version' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
if(! isset($mtaTestingAsEnforce)) {
  $mtaTestingAsEnforce = false;
}
if(! isset($starttlsTestingAsEnforce)) {
  $starttlsTestingAsEnforce = false;
}
if(! isset($addBadDane)) {
  $addBadDane = true;
}
if(! isset($failedDanePolicyMode)) {
  $failedDanePolicyMode = 'encrypt';
}

$weakTLS = array();
$sql='SELECT domain FROM deprecatedtls';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $weakTLS[] = $result->domain;
  }
}
$badDane = array();
$sql='SELECT domain FROM improperdane';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $badDane[] = $result->domain;
  }
}

$insecure = array();
$securePolicy = array();
$sql='SELECT domain FROM lackstls';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $insecure[] = $result->domain;
  }
}

function getProtocolCiphers($domain, $arr) {
  if(in_array($domain, $arr)) {
    return 'medium';
  }
  return 'high';
}
function getDaneStatus($domain, $arr, $badMode) {
  if($badMode == 'may') {
    $badMode = 'opportunistic';
  }
  if(in_array($domain, $arr)) {
    return $badMode;
  }
  return "DANE";
}
/*
function getPkixStatus($domain, $arr, $badMode) {
  if($badMode == 'may') {
    $badMode = 'opportunistic';
  }
  if(in_array($domain, $arr)) {
    return $badMode;
  }
  return "PKIX";
}
*/

function makePolicy($mode, $strength, $mxlist) {
  $policy = array();
  $policy['mode'] = $mode;
  $policy['strength'] = $strength;
  if (strlen($mxlist) > 0) {
    $policy['mxs'] = explode(':', $mxlist);
  }
  return $policy;
}

$now = time();
$exp = $now + 259200; //three days
$yest = ($now - 86400);
$yesterday = date('Y-m-d H:i:s', $yest);
$today = date('Y-m-d H:i:s', $now);
$policies = array();

// DANE policies

// may not query rawmx in future
$sql='SELECT domain,rawmx FROM mboxdomains WHERE dane = 1 AND lastcheck > "' . $yesterday . '" ORDER BY domain';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $domain = $result->domain;
    $mode = getDaneStatus($domain, $badDane, $failedDanePolicyMode);
    $strength = getProtocolCiphers($domain, $weakTLS);
    $mxlist = $result->rawmx;
    //$mxlist = '';
    $policies[$domain] = makePolicy($mode, $strength, $mxlist);
  }
}

// These are mailbox domains that use a MX provider which provides DANE validated MX hosts, but the mailbox domain itself is not DNSSEC signed
$sql='SELECT mboxdomains.domain,mboxdomains.rawmx FROM mboxdomains,daneprovider WHERE mboxdomains.danealias = daneprovider.aliaskey AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 ORDER BY mboxdomains.danealias,mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $domain = $result->domain;
    $mode = getDaneStatus($domain, $badDane, $failedDanePolicyMode);
    $strength = getProtocolCiphers($domain, $weakTLS);
    $mxlist = $result->rawmx;
    //$mxlist = '';
    $policies[$domain] = makePolicy($mode, $strength, $mxlist);
  }
}

// MTA-STS policies

$sql='SELECT mboxdomains.domain,mtacache.cleanmx,mtacache.mode FROM mboxdomains,mtacache WHERE mboxdomains.domain = mtacache.domain AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 1 AND mtacache.expires > "' . $today . '" ORDER BY mtacache.domain';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $domain = $result->domain;
    $mode = "encrypt";
    if($mtaTestingAsEnforce) {
      $mode = "PKIX";
    } elseif($result->mode == "enforce") {
      $mode = "PKIX";
    }
    $strength = getProtocolCiphers($domain, $weakTLS);
    if($mode == "PKIX") {
      $mxlist = $result->cleanmx;
    } else {
      $mxlist = '';
    }
    $policies[$domain] = makePolicy($mode, $strength, $mxlist);
  }
}

// secure alias policies

// These are mailbox domains that use an MX provider known to have PKIX validating MX servers
$sql='SELECT mboxdomains.domain,mboxdomains.rawmx FROM mboxdomains,pkixprovider WHERE mboxdomains.pkixalias = pkixprovider.aliaskey AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 AND mboxdomains.danealias IS NULL ORDER BY mboxdomains.pkixalias,mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $domain = $result->domain;
    $mode = "PKIX";
    $strength = "high";
    $mxlist = $result->rawmx;
    $policies[$domain] = makePolicy($mode, $strength, $mxlist);
  }
}





$preJSON = array();
// c is ISO 8601
$preJSON['timestamp'] = date('c', $now);
$preJSON['expires'] = date('c', $exp);



$preJSON['policies'] = $policies;

$output = json_encode($preJSON, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

header("Content-Type: text/plain");
// header("Content-Type: application/json");

print($output);


?>