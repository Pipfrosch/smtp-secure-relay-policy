#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) 2019 Michael A. Peters except where otherwise noted.
# MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE

# define these to connect to MariaDB database
#  can also be set at runtime with --dbuser=someuser [ -u someuser ] and --dbpass=somepassword [ -p somepassword ] switches
dbuser = None
dbpass = None

# define for Redis database
#  can also be set at runtime with --redis-pass=whatever [ -r whatever ] switch
redispass = None

# attempt to send logged messages once a week via email
sendMessageLog = True
# e-mail address to use in To: header.   Will use system account if left as None
emailLogToAddress = None
# e-mail address to use in Fron: header. Will use system account if left as None
emailLogFromAddress = None
# set to True to have the e-mailed log skip Notice level
emailLogIgnoreNotice = True

import sys, os, getopt, re
import time, calendar
from datetime import datetime
from random import randint
import json
# dns related
import dns.query
import dns.message
import dns.resolver
import dns.rdatatype

PYV = int(sys.version_info[0])
if PYV >= 3:
    import codecs
    curl_headers = {}

# path to certificate authority bundle
cabundle = '/etc/pki/tls/certs/mozilla-cacert.pem'
# path to a directory specifically for the purpose of GnuPG
dirforgnupg = '/usr/local/misc/eff_gpg'
# Path to GnuPG Binary
gnupgbin = '/usr/bin/gpg2'

lockfilekey = 'checkDomains.py-lock'

#globals set by main
vmode = 'noisy'
LOCALDNS = None
myResolver = None

today = None
lastcheck = None

# for argv
longswitches = []
shortnoarg = []
shortwarg = []

# switches without arguments
longswitches.append("silent")
shortnoarg.append("s")
longswitches.append("help")
shortnoarg.append("h")
longswitches.append("delete-lock")
# switches with arguments
longswitches.append("dbuser=")
shortwarg.append("u")
longswitches.append("dbpass=")
shortwarg.append("p")
longswitches.append("redis-pass=")
shortwarg.append("r")
longswitches.append("pem=")

shortswitches = ''.join(shortnoarg) + ':'.join(shortwarg) + ':'

ARGVusage = 'checkDomains.py -r <redis password> -u <marisdb user> -p <mariadb password> [optional args: --pem=/path/to/cabundle.pem --delete-lock --silent]'

os.environ['TZ'] = 'UTC'
time.tzset()

#globals set by functions
timestamp = None
dbase = None
connector = None
r = None

###
# Base Function
###

def redisConnect():
    """Defines the Redis connection in global r"""
    import redis
    global r
    r = redis.Redis(
        host='127.0.0.1',
        port='6379',
        password=redispass)

def setLockFile():
    """Returns False if lockfile already set. Otherwise sets lockfile and
    returns True. Lockfile is actually a Redis key, expires after 45
    minutes if not deleted."""
    try:
        value = r.get(lockfilekey)
    except:
        exit("I can not connect to Redis database. Is it running? Is the password correct?")
    if value is None:
        r.set(lockfilekey, 'locked')
        # lock for 45 minutes
        r.expire(lockfilekey, 2700)
        return True
    return False

def unsetLockFile():
    """Deletes the Redis key being used as a lockfile."""
    if r is None:
        outputMessage("Error", None, "Could not delete Redis lock file.")
        return
    r.delete(lockfilekey)

def mariadbConnect():
    """Establishes connection to MariaDB database, sets dbase and cursor global.
    Terminates script on failure."""
    global dbase
    global cursor
    import mysql.connector as mariadb
    try:
        dbase = mariadb.connect(user=dbuser, password=dbpass, database='mtastsdb')
    except:
        outputMessage("Error", None, "Database connection failed.")
        terminalError()
    cursor = dbase.cursor()

def outputMessage(mtype, context, message):
    """Handles output messages, printing to console if in noisy mode and logging in database if requested.
    
    Keyword arguments:
    mtype -- The type of output message (Notice, Warning, Error, or Debug)
    context -- A string inserted into database to clarify the context of message generation.
    message -- The message being output to the console and/or logged in the database.
    """
    if context is not None:
        rightnow = datetime.utcfromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        commit = True
        try:
            cursor.execute('INSERT INTO logmessages(type,context,mode,message,tstamp) VALUES(%s,%s,%s,%s,%s)', (mtype, context, vmode, message, rightnow,))
        except:
            commit = False
            print("Could not log message in database.")
        if commit:
            dbase.commit()
    if vmode == "noisy":
        mlist = message.split("|")
        for strng in mlist:
            print(mtype + ": " + strng)

def terminalError():
    """Cleanly exits when there is a condition under which the script knows it can not successfully
    continue."""
    unsetLockFile()
    if dbase is not None:
        dbase.close()
    if vmode == "silent":
        sys.exit('checkDomains.py Bad Exit. There may be error in logmessages database table.')
    sys.exit(1)

def checkDatabaseVersion():
    """Verifies the database has the right structure to work with this script. Terminates the
    script if it does not."""
    dbcompat = '00000100'
    ErrorContext = 'checkDatabaseVersion()'
    try:
        cursor.execute('SELECT COUNT(id) FROM metadata WHERE compatv = %s AND active = 1',(dbcompat,))
    except:
        outputMessage('Error', ErrorContext, 'Could not query database table version.|Exiting now.')
        terminalError()
    for row in cursor:
        if int(row[0]) != 1:
            outputMessage('Error', ErrorContext, 'Incorrect database table version.|Exiting now.')
            terminalError()

def roundupTen(n):
    """Rounds a number up to the nearest ten returning an integer.
    
    Keyword arguments:
    n -- the number to be rounded up to the nearest 10.
    """
    import math
    return int(math.ceil(n / 10.0)) * 10

def datetimeDaysAgo(days):
    """Returns a MariaDB DATETIME string in UTC for argument days before script runtime timestamp.
    
    Keyword arguments:
    days -- the number of days in past we want a datetime for.
    """
    global timestamp
    if timestamp is None:
        timestamp = int(time.time())
    myts = int(timestamp - (days * 86400))
    return datetime.utcfromtimestamp(myts).strftime('%Y-%m-%d %H:%M:%S')

def datetimeDaysAhead(days):
    """Returns a MariaDB DATETIME string in UTC for argument days in the future of script runtime
    timestamp.
    
    Keyword arguments:
    days -- the number of days in future we want a datetime for.
    """
    global timestamp
    if timestamp is None:
        timestamp = int(time.time())
    myts = int(timestamp + (days * 86400))
    return datetime.utcfromtimestamp(myts).strftime('%Y-%m-%d %H:%M:%S')

def startOfLastSunday():
    """Returns YYYY-MM-DD 00:00:00 for the previous Sunday"""
    global timestamp
    if timestamp is None:
        timestamp = int(time.time())
    Ndays = (datetime.utcfromtimestamp(timestamp).weekday() + 1)
    strng = str(datetimeDaysAgo(Ndays))[:10] + ' 00:00:00'
    return strng

def startOfFourWreeksAgoSunday():
    """Returns YYYY-MM-DD 00:00:00 for 4 weeks ago Sunday"""
    global timestamp
    if timestamp is None:
        timestamp = int(time.time())
    Ndays = (datetime.utcfromtimestamp(timestamp).weekday() + 27)
    strng = str(datetimeDaysAgo(Ndays))[:10] + ' 00:00:00'
    return strng

def calculateNewExpires(seconds):
    """Returns a MariaDB DATETIME string in UTC for argument seconds past script runtime timestamp.
    
    Keyword arguments:
    seconds -- the number of seconds in future.
    """
    global timestamp
    if timestamp is None:
        timestamp = int(time.time())
    myts = int(timestamp + seconds)
    return datetime.utcfromtimestamp(myts).strftime('%Y-%m-%d %H:%M:%S')

def datestampToEpoch(strng):
    """Converts YYYY-MM-DD HH:MM:SS to epoch seconds - NOT CURRENTLY IN USE."""
    myTuple = time.strptime(strng, '%Y-%m-%d %H:%M:%S')
    return int(calendar.timegm(myTuple))

def dproviderTimestampToEpoch(strng):
    """Converts the YYYY-MM-DD HH:MM:SS +00:00 used by mx-providers.json to seconds from epoch."""
    # seems %z does not work with my python, but I always use UTC when generating the
    #  timestamp so I can just strip it.
    withoutOffset = strng[:-6]
    myTuple = time.strptime(withoutOffset, '%Y-%m-%d %H:%M:%S')
    return int(calendar.timegm(myTuple))

def stlsTimestampToEpoch(strng):
    """Converts the YYYY-MM-DD HH:MM:SS.mmmmmm[+|-]HH:MM used by policy.json to seconds from
    epoch."""
    withoutOffset = strng[:-6]
    offset = strng[-6:]
    myTuple = time.strptime(withoutOffset, '%Y-%m-%dT%H:%M:%S.%f')
    epochAnchor = int(calendar.timegm(myTuple))
    hours = int(offset[1:3])
    minutes = int(offset[-2:])
    Tseconds = (3600 * hours) + (60 * minutes)
    if offset.startswith("-"):
        return (epochAnchor + Tseconds)
    else:
        return (epochAnchor - Tseconds)

def header_function(header_line):
    """Sets dictionary for headers returned from a pycurl request.
    Adapted from http://pycurl.io/docs/latest/quickstart.html documentation."""
    global curl_headers
    header_line = header_line.decode('iso-8859-1')
    if ':' not in header_line:
        return
    name, value = header_line.split(':', 1)
    name = name.strip()
    value = value.strip()
    name = name.lower()
    curl_headers[name] = value

def secureGetResource(someURL):
    """Uses pycurl to retrieve a remote resource, enforcing PKIX validation for secure remote
    resources.
    Adapted from http://pycurl.io/docs/latest/quickstart.html documentation."""
    import pycurl
    if PYV < 3:
        from StringIO import StringIO
        buff = StringIO()
    else:
        global curl_headers
        from io import BytesIO
        buff = BytesIO()
    curl = pycurl.Curl()
    curl.setopt(pycurl.CAINFO, cabundle)
    try:
        curl.setopt(pycurl.WRITEDATA, buff)
    except:
        curl.setopt(pycurl.WRITEFUNCTION, buff.write)
    curl.setopt(pycurl.SSL_VERIFYPEER, 1)
    curl.setopt(pycurl.SSL_VERIFYHOST, 2)
    curl.setopt(pycurl.USERAGENT, "MTA-STSbot/0.1 (via libcurl " + pycurl.version_info()[1] + ")")
    curl.setopt(pycurl.TIMEOUT, 5)
    curl.setopt(pycurl.URL, someURL)
    if PYV >= 3:
        curl_headers = {}
        curl.setopt(pycurl.HEADERFUNCTION, header_function)
    curl.perform()
    RC = int(curl.getinfo(pycurl.RESPONSE_CODE))
    curl.close()
    if RC != 200:
        raise Exception('Response code: {}'.format(RC))
    content = buff.getvalue()
    if PYV < 3:
        return content
    else:
        encoding = None
        if 'content-type' in curl_headers:
            content_type = curl_headers['content-type'].lower()
            match = re.search('charset=(\S+)', content_type)
            if match:
                encoding = match.group(1)
        if encoding is None:
            encoding = 'iso-8859-1'
        return content.decode(encoding)

###
# Network Tests
###

def testForIPv6(domainList):
    """Performs queries to Google pDNS on IPv6 to determine external IPv6 capability. Returns True
    if success, otherwise False.
    
    Keyword arguments:
    domainList -- A list of domain names to attempt to query.
    """
    for domain in domainList:
        request = dns.message.make_query(domain, dns.rdatatype.SOA)
        try:
            response = dns.query.udp(request, '2001:4860:4860::8844')
        except:
            try:
                response = dns.query.udp(request, '2001:4860:4860::8888')
            except:
                response = 'Failed'
        strng = str(response)
        if strng.count("rcode NOERROR") != 0:
            return True
    return False

def testForIPv4(domainList):
    """Performs queries to Google pDNS on IPv4 to determine external IPv4 capability. Returns True
    if success, otherwise False.
    
    Keyword arguments:
    domainList -- A list of domain names to attempt to query.
    """
    for domain in domainList:
        request = dns.message.make_query(domain, dns.rdatatype.SOA)
        try:
            response = dns.query.udp(request, '8.8.4.4')
        except:
            try:
                response = dns.query.udp(request, '8.8.8.8')
            except:
                response = 'Failed'
        strng = str(response)
        if strng.count("rcode NOERROR") != 0:
            return True
    return False

def testInternetConnectivity():
    """Tests whether or not connection to the public Internet can be made."""
    ErrorContext = 'testInternetConnectivity()'
    domainList = ['icann.org', 'internic.com', 'gnu.org', 'co.uk']
    if testForIPv6(domainList):
        if not testForIPv4(domainList):
            outputMessage("Warning", ErrorContext, "No external IPv4 network capabilities detected.|Some MTA-STS policies may not be retrievable.")
    else:
        if not testForIPv4(domainList):
            outputMessage("Error", ErrorContext, "No external network capabilities detect.|Exiting now.")
            terminalError()
        else:
            outputMessage("Notice", None, "No external IPv6 network capabilities detected.| -- Please consider joining the 21st century.")

def getLocalDNS():
    """Determines whether ::1 or 127.0.0.1 should be used for local DNS and returns value.
    Terminates if neither works."""
    request = dns.message.make_query('dnssec.icu', dns.rdatatype.SOA)
    try:
        response = dns.query.udp(request, '::1', timeout = 15)
    except:
        response = 'failed'
    strng = str(response)
    if strng.count("rcode NOERROR") == 1:
        outputMessage("Notice", None, "Using ::1 for local Recursive Resolver")
        return '::1'
    try:
        response = dns.query.udp(request, '127.0.0.1', timeout = 15)
    except:
        response = 'failed'
    if strng.count("rcode NOERROR") == 1:
        outputMessage("Notice", None, "Using 127.0.0.1 for local Recursive Resolver")
        return '127.0.0.1'
    outputMessage("Error", "getLocalDNS()", "No local Recursive Resolver detected.|Exiting now.")
    terminalError()

def testLocalResolverForDNSSEC(mydns):
    """Determines if the local caching resolver properly enforces DNSSEC. Terminates if it does not."""
    ErrorContext = 'testLocalResolverForDNSSEC()'
    outputMessage("Notice", None, "Checking DNSSEC capabilities of local resolver. This could take a minute…")
    # proper.dnssec.icu - should have a valid RRSIG
    request = dns.message.make_query('proper.dnssec.icu', dns.rdatatype.SOA, want_dnssec=True)
    try:
        response = dns.query.udp(request, mydns, timeout = 15)
    except:
        outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
        terminalError()
    strng = str(response)
    if strng.count("rcode SERVFAIL") == 1:
        # Sometimes it needs to be primed
        try:
            response = dns.query.udp(request, mydns, timeout = 15)
        except:
            outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
            terminalError()
        strng = str(response)
    if strng.count("IN RRSIG SOA") == 0:
        outputMessage("Error", ErrorContext, "Something is wrong with your local Recursive Resolver.|Queried SOA from proper.dnssec.icu which is a properly DNSSEC signed zone, but the response lacked an RRSIG.|Exiting now.")
        terminalError()
    # invalid-ds.dnssec.icu - DS record in parent zone does not match KSK
    request = dns.message.make_query('invalid-ds.dnssec.icu', dns.rdatatype.SOA)
    try:
        response = dns.query.udp(request, mydns, timeout = 15)
    except:
        outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
        terminalError()
    strng = str(response)
    if strng.count("rcode SERVFAIL") == 1:
        # Double check
        try:
            response = dns.query.udp(request, mydns, timeout = 15)
        except:
            outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
            terminalError()
        strng = str(response)
    if strng.count("rcode SERVFAIL") == 0:
        outputMessage("Error", ErrorContext, "You do not have a proper DNSSEC enforcing Recursive Resolver on the localhost.|Queried SOA from invalid-ds.dnssec.icu which intentionally has Key Signing Key that does not match DS in parent zone.|The response should have been a SERVFAIL but the query was successful.|Exiting now.")
        terminalError()
    # valid-dnskey-with-bad-sigs.dnssec.icu - DNSKEY RRset validates but records signed different ZSK
    request = dns.message.make_query('valid-dnskey-with-bad-sigs.dnssec.icu', dns.rdatatype.SOA)
    try:
        response = dns.query.udp(request, mydns, timeout = 15)
    except:
        outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
        terminalError()
    strng = str(response)
    if strng.count("rcode SERVFAIL") != 1:
        # Double check
        try:
            response = dns.query.udp(request, mydns, timeout = 15)
        except:
            outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
            terminalError()
        strng = str(response)
    if strng.count("rcode SERVFAIL") == 0:
        outputMessage("Error", ErrorContext, "You do not have a proper DNSSEC enforcing Recursive Resolver on the localhost.|Queried SOA from valid-dnskey-with-bad-sigs.dnssec.icu which intentionally has bad DNSSEC signatures.|The response should have been a SERVFAIL but the query was successful.|Exiting now.")
        terminalError()
    # expired-sigs.dnssec.icu - signatures are expired
    request = dns.message.make_query('expired-sigs.dnssec.icu', dns.rdatatype.SOA)
    try:
        response = dns.query.udp(request, mydns, timeout = 15)
    except:
        outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
        terminalError()
    strng = str(response)
    if strng.count("rcode SERVFAIL") == 1:
        # Double check
        try:
            response = dns.query.udp(request, mydns, timeout = 15)
        except:
            outputMessage("Error", ErrorContext, "Local resolver running at " + mydns + " seems to have stopped responding.|Exiting now.")
            terminalError()
        strng = str(response)
    if strng.count("rcode SERVFAIL") == 0:
        outputMessage("Error", ErrorContext, "You do not have a proper DNSSEC enforcing Recursive Resolver on the localhost.|Queried SOA from expired-sigs.dnssec.icu which intentionally has expired DNSSEC signatures.|The response should have been a SERVFAIL but the query was successful.|Exiting now.")
        terminalError()
    try:
        r.set('mtacachedb-ResolverTest','passes')
    except:
        exit("I can not connect to Redis database. Is it running? Is the password correct?")
    # remember for three days
    r.expire('mtacachedb-ResolverTest',259200)

###
# Validate certificate bundle
###

def parseCertBundle(myBundle):
    """Loads a PEM encoded Certificate bundle and returns a list of hex encoded Public Key Identifiers for certificates in the bundle."""
    from cryptography import x509
    from cryptography.hazmat.backends import default_backend
    ErrorContext = 'parseCertBundle()'
    skiList = []
    fh = open(myBundle, 'r')
    pemList = []
    currentState = None
    currentPEM = None
    for line in fh:
        if "-----BEGIN CERTIFICATE-----" in line:
            if currentState is not None:
                outputMessage("Warning", ErrorContext, "The certificate bundle " + myBundle + " appears to be malformed.")
                return []
            currentPEM = str(line)
            currentState = 'extract'
        elif "-----END CERTIFICATE-----" in line:
            if currentState is None:
                outputMessage("Warning", ErrorContext, "The certificate bundle " + myBundle + " appears to be malformed.")
                return []
            currentState = None
            currentPEM += str(line)
            if PYV < 3:
                pemList.append(currentPEM)
            else:
                pemList.append(str.encode(currentPEM))
        elif currentState is not None:
            currentPEM += str(line)
    fh.close()
    for pem_data in pemList:
        CONT = True
        try:
            cert = x509.load_pem_x509_certificate(pem_data, default_backend())
        except:
            outputMessage("Warning", ErrorContext, "The certificate bundle " + myBundle + " contains a certificate I can not read.")
            CONT = False
        if CONT:
            try:
                ski = cert.extensions.get_extension_for_oid(x509.oid.ExtensionOID.SUBJECT_KEY_IDENTIFIER)
            except:
                CONT = False
        if CONT:
            if PYV < 3:
                skiList.append(str(ski.value.digest).encode('hex'))
            else:
                skiList.append(str(codecs.encode(ski.value.digest, 'hex_codec'),'ascii'))
    return skiList

def validateCertBundle(bundle):
    """Checks whether a specified Certificate Bundle contains the mandatory Trust Anchors that are needed. Returns True or False."""
    ErrorContext = 'validateCertBundle()'
    global cabundle
    try:
        fh = open(bundle, 'r')
    except IOError:
        return False
    fh.close()
    skiList = parseCertBundle(bundle)
    # verify bundle has trust anchors we need
    mandatorySKI = {}
    suggestedSKI = {}
    try:
        cursor.execute('SELECT common,ski,required FROM rootanchor')
    except:
        outputMessage('Warning', ErrorContext, 'Could not select Subject Key Identifier info from rootanchor table.|Continuing anyway.')
        cabundle = bundle
        return True
    for row in cursor:        
        cn = str(row[0]).strip()
        ski = str(row[1]).strip()
        if int(row[2]) > 0:
            mandatorySKI[cn] = ski
        else:
            suggestedSKI[cn] = ski
    #MandatoryCheck
    keyList = mandatorySKI.keys()
    for cn in keyList:
        ski = mandatorySKI[cn]
        if not ski in skiList:
            return False
    # Warn about missing certs
    keyList = suggestedSKI.keys()
    for cn in keyList:
        ski = suggestedSKI[cn]
        if not ski in skiList:
            outputMessage("Warning", ErrorContext, "Missing root Trust Anchor: " + cn + "| -- Some MTA-STS policies may not be retrieved.")
    cabundle = bundle
    return True

def detectTrustAnchorBundle():
    """Looks for a suitable Certificate Bundle to use for PKIX validation. Terminates script if one is not found."""
    pathList = [cabundle,
        "/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem", # RHEL/CentOS/Fedora location
        "/etc/pki/tls/certs/ca-bundle.crt",                  # Common location
        "/etc/ssl/certs/ca-bundle.crt"                       # Common location
        "/etc/ssl/certs/ca-certificates.crt",                # Common location
        "/usr/share/ssl/certs/ca-bundle.crt",                # Common location
        "/usr/local/etc/openssl/cert.pem",                   # Common OS X Homebrew location
        "/opt/local/etc/openssl/cert.pem"]                   # Common OS X Homebrew location
    for bundle in pathList:
        if validateCertBundle(bundle):
            outputMessage("Notice", None, "Using CA Root Anchor bundle found at " + bundle)
            return
    outputMessage("Error", "detectTrustAnchorBundle()", "No suitable Certificate Authority root bundle was found.|Exiting now.")
    terminalError()

###
# Retrieval / validation of JSON and DANE Fail List
###

# detect need to retrieve
def detectNeedJSON():
    """Determines whether or not it is time to retrieve the JSON resources again. Returns True or False."""
    N = 0
    #twentyeightago = int(timestamp - (28 * 3600))
    #triggercheck = datetime.utcfromtimestamp(twentyeightago).strftime('%Y-%m-%d %H:%M:%S')
    triggercheck = datetimeDaysAgo(1.667)
    try:
        cursor.execute('SELECT COUNT(id) FROM rootanchor WHERE insdate > %s', (triggercheck,))
    except:
        outputMessage("Warning", "detectNeedJSON()", "Could not perform SQL query to determine if JSON policies need to be queried.|Continuing anyway.")
        return False;
    for row in cursor:
        N = int(row[0])
    if N == 0:
        return True
    else:
        return False

# sleep for a period of time
def staggarJSONFetch():
    """When retrieving JSON files, determines how long to sleep before attempting and sleeps for that many seconds."""
    global timestamp
    global today
    global lastcheck
    N = 0
    try:
        cursor.execute('SELECT COUNT(id) FROM rootanchor WHERE required = 0')
    except:
        outputMessage("Warning", "staggarJSONFetch()", "Could not perform SQL query to determine if JSON data has been retrieved before.|Continuing anyway.")
        return
    for row in cursor:
        N = int(row[0])
    if N == 0:
        return
    # now calculate a random number
    N = randint(15,900)
    ctime = datetime.utcfromtimestamp(timestamp).strftime('%H:%M:%S')
    MINUTES = N / 60
    SECONDS = N % 60
    if MINUTES == 1:
        minstring = " minute and "
    else:
        minstring = " minutes and "
    if SECONDS == 1:
        secstring = " second "
    else:
        secstring = " seconds "
    outputMessage("Notice", None, "Current time: " + ctime  + " UTC.|Sleeping for " + str(MINUTES) + minstring + str(SECONDS) + secstring + "before continuing.")
    time.sleep(N)
    timestamp = None
    today = datetimeDaysAgo(0)
    lastcheck = datetimeDaysAgo(0.5)

def testGnuPGhome():
    """Determines whether or not the script has write permission to the specified GnuPG home directory. Returns True or False."""
    dummy = dirforgnupg + '/dummy.txt'
    try:
        fh = open(dummy, 'w')
    except IOError:
        return False
    fh.close()
    os.remove(dummy)
    return True

def testImportedKeys(pubkeys, fprint):
    """Determines whether or not the needed PGP public key has already been imported. Returns True or False."""
    for pkey in pubkeys:
        fingerprint = pkey['fingerprint']
        if fingerprint == fprint:
            return True
    return False;

def testKeyBeforeImport(pubkey, fprint):
    """Makes sure a downloaded PGP key has the right fingerprint. Returns True or False."""
    pkey = pubkey[0]
    fingerprint = pkey['fingerprint']
    if fingerprint == fprint:
        return True
    return False

def getSignedJSON(myDict):
    """Retrieves a PGP signed JSON file and validates it. Returns the JSON data on success, empty JSON on failure."""
    import gnupg
    import tempfile
    import shutil
    ErrorContext = 'getSignedJSON()'
    permDir = testGnuPGhome()
    jsonURL = myDict['json']
    jsonASC = myDict['asc']
    publicKEY = myDict['pubkey']
    fingerprint = myDict['fingerprint']
    try:
        jsondata = secureGetResource(jsonURL)
    except:
        outputMessage("Warning", ErrorContext, "Could not retrieve jsondata.|Continuing anyway.")
        return '{}'
    try:
        sigdata = secureGetResource(jsonASC)
    except:
        outputMessage("Warning", ErrorContext, "Could not retrieve JSON data PGP signature.|Not using the retrieved JSON data.")
        return '{}'
    if permDir:
        dirpath = dirforgnupg
    else:
        dirpath = tempfile.mkdtemp()
    homedir = dirpath + "/.gnupg"
    gpg = gnupg.GPG(gnupghome=homedir, gpgbinary=gnupgbin)
    gpg.encoding = 'utf-8'
    sigfile = open(dirpath + '/file.json.asc', 'w+')
    sigfile.write(sigdata)
    sigfile.close()
    if PYV < 3:
        sigfile = open(dirpath + '/file.json.asc', 'r')
    else:
        sigfile = open(dirpath + '/file.json.asc', 'rb')
    jsonfile = open(dirpath + '/file.json', 'w+')
    jsonfile.write(jsondata)
    jsonfile.close()
    public_keys = gpg.list_keys()
    haveKey = testImportedKeys(public_keys, fingerprint)
    if not haveKey:
        try:
            keydata = secureGetResource(publicKEY)
        except:
            print ("Could not retrieve public key")
            return '{}'
        fh = open(dirpath + '/pubkey.txt', 'w+')
        fh.write(keydata)
        fh.close()
        keyFile = gpg.scan_keys(dirpath + '/pubkey.txt')
        correctKey = testKeyBeforeImport(keyFile, fingerprint)
        os.remove(dirpath + '/pubkey.txt')
        if correctKey:
            gpg.import_keys(keydata)
        else:
            outputMessage("Warning", ErrorContext, "Retrieved Public Key Has Incorrect Fingerprint, not importing.")
            return '{}'
    verified = gpg.verify_file(sigfile, dirpath + '/file.json')
    sigfile.close()
    os.remove(dirpath + '/file.json.asc')
    os.remove(dirpath + '/file.json')
    if not permDir:
        shutil.rmtree(dirpath)
    if not verified:
        outputMessage("Warning", ErrorContext, "Remote JSON data signature verify failed.|Not using the retrieved JSON data")
        return '{}'
    return jsondata

def getMXProviderPolicy():
    """Retrieves the mx-providers.json file."""
    policyDict = {}
    policyDict['json'] = 'https://dnssec.icu/mtastsdb/mx-providers.json'
    policyDict['asc'] = 'https://dnssec.icu/mtastsdb/mx-providers.json.asc'
    policyDict['pubkey'] = 'https://dnssec.icu/mtastsdb/pipfrosch-gpg.asc'
    # key is my key
    policyDict['fingerprint'] = '24AB54EDA43B9063EE07B073E901B23E50452023'
    return getSignedJSON(policyDict)

def templateMXTableCreate(myList, tablename):
    """Maintains the database table for the DANE MX providers and PKIX MX providers."""
    ErrorContext = 'templateMXTableCreate()'
    for myDict in myList:
        N = 0
        try:
            cursor.execute('SELECT COUNT(id) FROM ' + tablename + ' WHERE aliaskey = %s', (myDict['aliaskey'],))
        except:
            outputMessage("Error", ErrorContext, "Could not count entries in " + tablename + " table.|Exiting now.")
            terminalError()
        for row in cursor:
            N = int(row[0])
        if N == 0:
            try:
                cursor.execute('INSERT INTO ' + tablename + '(aliaskey,policymx,lastcheck,expires,insdate) VALUES (%s, %s, %s, %s, %s)', (myDict['aliaskey'], myDict['policymx'], myDict['lastcheck'], myDict['expires'], today,))
            except:
                outputMessage("Error", ErrorContext, "Could not insert entry into " + tablename + " table.|Exiting now.")
                terminalError()
        else:
            try:
                cursor.execute('UPDATE ' + tablename + ' SET policymx = %s, lastcheck = %s, expires = %s, insdate = %s WHERE aliaskey = %s', (myDict['policymx'], myDict['lastcheck'], myDict['expires'], today, myDict['aliaskey'],))
            except:
                outputMessage("Error", ErrorContext, "Could not update entry in " + tablename + " table.|Exiting now.")
                terminalError()
    try:
        cursor.execute('DELETE FROM ' + tablename + ' WHERE insdate < %s',(today,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete expired entries from " + tablename + " table.")
    try:
        cursor.execute('DELETE FROM ' + tablename + ' WHERE expires < %s',(today,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete expired entries from " + tablename + " table.")
    dbase.commit()

def updateMXproviders(jsondata, jsonkey):
    """Creates a list of dictionary list from JSON data for maintaining MX provider tables."""
    templateMXlist = []
    aliasDict = jsondata[jsonkey]
    keylist = aliasDict.keys()
    for mykey in keylist:
        cleanAliasDict = {}
        cleanMXlist = []
        cleanAliasDict['aliaskey'] = str(mykey).strip()
        vdate = aliasDict[mykey]['lastcheck'].strip()
        vepoch = dproviderTimestampToEpoch(vdate)
        #65 days after last validated
        vexpires = vepoch + 5616000
        cleanAliasDict['lastcheck'] = datetime.utcfromtimestamp(vepoch).strftime('%Y-%m-%d %H:%M:%S')
        cleanAliasDict['expires'] = datetime.utcfromtimestamp(vexpires).strftime('%Y-%m-%d %H:%M:%S')
        mxlist = aliasDict[mykey]['mxs']
        for host in mxlist:
            strng = str(host).strip().lower()
            cleanMXlist.append(strng)
        smx = sorted(cleanMXlist)
        strng = str(':'.join(smx))
        cleanAliasDict['policymx'] = strng
        templateMXlist.append(cleanAliasDict)
    return templateMXlist

def templateTrustAnchorTableCreate(myList):
    """Maintains the rootanchor table for Subject Key Identifiers that are required or recommended."""
    ErrorContext = 'templateTrustAnchorTableCreate()'
    for myDict in myList:
        N = 0
        try:
            cursor.execute('SELECT COUNT(id) FROM rootanchor WHERE ski = %s', (myDict['ski'],))
        except:
            outputMessage("Warning", ErrorContext, "Could not count entries in rootanchor table.|Continuing anyway.")
            return
        for row in cursor:
            N = int(row[0])
        if N == 0:
            try:
                cursor.execute('INSERT INTO rootanchor(common,ski,required,expires,insdate) VALUES(%s,%s,%s,%s,%s)', (myDict['cn'], myDict['ski'], myDict['required'], myDict['expires'], today,))
            except:
                outputMessage("Warning", ErrorContext, "Could not insert entry in rootanchor table.|Continuing anyway.")
                return
        else:
            try:
                cursor.execute('UPDATE rootanchor SET common = %s, required = %s, insdate = %s WHERE ski = %s', (myDict['cn'], myDict['required'], today, myDict['ski'],))
            except:
                outputMessage("Warning", ErrorContext, "Could not update entry in rootanchor table.|Continuing anyway.")
                return
    try:
        cursor.execute('DELETE FROM rootanchor WHERE insdate < %s OR expires < %s', (today, today,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete outdated entries from rootanchor table.|Continuing anyway.")
    dbase.commit()

def updateTrustAnchors(jsondata):
    """Creates a list of dictionaries from Trust Anchor SKI JSON data."""
    templateList = []
    aliasDict = jsondata['trust-anchors']
    keylist = aliasDict.keys()
    for mykey in keylist:
        cleanAliasDict = {}
        cleanAliasDict['cn'] = str(mykey).strip()
        cleanAliasDict['ski'] = str(aliasDict[mykey]['ski']).strip().lower()
        exdate = aliasDict[mykey]['expires'].strip()
        exepoch = dproviderTimestampToEpoch(exdate)
        cleanAliasDict['expires'] = datetime.utcfromtimestamp(exepoch).strftime('%Y-%m-%d %H:%M:%S')
        if 'required' in aliasDict[mykey]:
            cleanAliasDict['required'] = 1
        else:
            cleanAliasDict['required'] = 0
        templateList.append(cleanAliasDict)
    return templateList

def queryMXProviderPolicyJSON():
    """Takes the JSON data for MX Providers and PKIX Subject Key Identifiers and calls the functions that uses the data."""
    policydata = getMXProviderPolicy()
    jsondata = json.loads(policydata)
    if 'dane-providers' not in jsondata:
        return
    if 'pkix-providers' not in jsondata:
        return
    if 'trust-anchors' not in jsondata:
        return
    templateList = updateMXproviders(jsondata, 'dane-providers')
    templateMXTableCreate(templateList, 'daneprovider')
    templateList = updateMXproviders(jsondata, 'pkix-providers')
    templateMXTableCreate(templateList, 'pkixprovider')
    templateList = updateTrustAnchors(jsondata)
    templateTrustAnchorTableCreate(templateList)

def getStartTlsPolicy():
    """Retrieves the policy.json file."""
    policyDict = {}
    policyDict['json'] = 'https://dl.eff.org/starttls-everywhere/policy.json'
    policyDict['asc'] = 'https://dl.eff.org/starttls-everywhere/policy.json.asc'
    policyDict['pubkey'] = 'https://dl.eff.org/starttls-everywhere/public-key.txt'
    # fingerprint value from https://starttls-everywhere.org/policy-list/
    policyDict['fingerprint'] = 'B693F33372E965D76D55368616EEA65D03326C9D'
    return getSignedJSON(policyDict)

def getDomainPolicies(jsondata):
    """Returns a list of dictionaries for STARTTLS Everywhere policies from the JSON data."""
    cacheList = []
    domainDict = jsondata['policies']
    if PYV < 3:
        domainList = domainDict.keys()
    else:
        domainList = list(domainDict)
    domainList.sort()
    for mbdm in domainList:
        domain = str(mbdm).strip()
        myDict = {}
        myDict['domain'] = domain
        if 'mode' in domainDict[domain]:
            myDict['mode'] = str(domainDict[domain]['mode']).strip()
        else:
            myDict['mode'] = None
        if 'mxs' in domainDict[domain]:
            oldPolicyList = domainDict[domain]['mxs']
            newPolicyList = []
            for policy in oldPolicyList:
                newpolicy = str(policy).strip()
                newPolicyList.append(newpolicy)
            myDict['policymx'] = ':'.join(newPolicyList)
        else:
            myDict['policymx'] = None
        cacheList.append(myDict)
    return cacheList

def updateSTLScache(myList, expires):
    """Updates the STARTTLS Everywhere policy cache table."""
    ErrorContext = "updateSTLScache()"
    for myDict in myList:
        #first make sure domain is in general list
        starttls = '0'
        if myDict['mode'] is not None:
            starttls = '1'
        N = 0
        try:
            cursor.execute('SELECT COUNT(id) FROM mboxdomains WHERE domain = %s',(myDict['domain'],))
        except:
            outputMessage("Error", ErrorContext, "Could not determine if STARTTLS added domain is already in mboxdomains.|Exiting now.")
            terminalError()
        for row in cursor:
            N = int(row[0])
        if N == 0:
            try:
                cursor.execute('INSERT INTO mboxdomains(domain,starttls) VALUES(%s,%s)',(myDict['domain'], starttls,))
            except:
                outputMessage("Error", ErrorContext, "Could not insert STARTTLS added domain into mboxdomains table.|Exiting now.")
                terminalError()
        else:
            try:
                cursor.execute('UPDATE mboxdomains SET starttls = %s WHERE domain = %s',(starttls, myDict['domain'],))
            except:
                outputMessage("Warning", ErrorContext, "Could not update starttls column in mboxdomains row.|Continuing.")
        #now make the cache entry
        if int(starttls) == 1:
            N = 0
            try:
                cursor.execute('SELECT COUNT(id) FROM stlscache WHERE domain = %s',(myDict['domain'],))
            except:
                outputMessage("Error", ErrorContext, "Could not determine if " + myDict['domain'] + " is already in STARTTLS policy cache.|Exiting now.")
                terminalError()
            for row in cursor:
                N = int(row[0])
            if N == 0:
                try:
                    cursor.execute('INSERT INTO stlscache(domain,policymx,mode,lastcheck,expires) VALUES (%s,%s,%s,%s,%s)',(myDict['domain'], myDict['policymx'], myDict['mode'], today, expires,))
                except:
                    outputMessage("Error", ErrorContext, "Could not insert " + myDict['domain'] + " into STARTTLS Policy cache.|Exiting now.")
                    terminalError()
            else:
                try:
                    cursor.execute('UPDATE stlscache SET policymx = %s, mode = %s, lastcheck = %s, expires = %s WHERE domain = %s',(myDict['policymx'], myDict['mode'], today, expires, myDict['domain'],))
                except:
                    outputMessage('Error', ErrorContext, "Could not update policy for " + myDict['domain'] + " in STARTTLS policy cache.|Exiting now.")
                    terminalError()
    dbase.commit()
    try:
        cursor.execute('DELETE FROM stlscache WHERE lastcheck < %s', (today,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries from STARTTLS policy cache that are no longer in STARTTLS Everywhere policy JSON file.")
    dbase.commit()

def querySTLSeverywherePolicyJSON():
    """Takes the data from the STARTTLS Everywhere JSON data and calls the functions that uses it."""
    policydata = getStartTlsPolicy()
    jsondata = json.loads(policydata)
    #if 'policy-aliases' not in jsondata:
    #    return
    if 'policies' not in jsondata:
        return
    if 'expires' not in jsondata:
        return
    epochExpires = stlsTimestampToEpoch(jsondata['expires'])
    stlsExpires = datetime.utcfromtimestamp(epochExpires).strftime('%Y-%m-%d %H:%M:%S')
    newCacheList = getDomainPolicies(jsondata)
    updateSTLScache(newCacheList, stlsExpires)

def getDaneFailList():
    """Retrieves the DANE Fail List from the remote source and updates the improperdane database table."""
    ErrorContext = 'getDaneFailList()'
    DANEfail = "https://raw.githubusercontent.com/danefail/list/master/dane_fail_list.dat"
    try:
        response = secureGetResource(DANEfail)
    except:
        return
    badDaneList = []
    res = response.splitlines( )
    for line in res:
        if len(line) > 0:
           if not line.startswith("#"):
               if line not in badDaneList:
                   badDaneList.append(line)
    badDaneList.sort()
    addToTable = []
    updateTS = []
    for mboxd in badDaneList:
        try:
            cursor.execute('SELECT COUNT(id) FROM improperdane WHERE domain=%s', (mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not get count on domains in improperdane table.|Exiting now.")
            terminalError()
        for row in cursor:
            if int(row[0]) == 0:
                addToTable.append(mboxd)
            else:
                updateTS.append(mboxd)
    for mboxd in addToTable:
        try:
            cursor.execute('INSERT INTO improperdane(domain,danefail) VALUES(%s,1)', (mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not update improperdane database table.|Exiting now.")
            terminalError()
    for mboxd in updateTS:
        try:
            cursor.execute('UPDATE improperdane SET insdate = %s, danefail = 1 WHERE domain = %s', (today, mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not update improperdane database table.|Exiting now.")
            terminalError()
    dbase.commit()

# End query JSON / DANE Fail List

###
# DNSSEC / DANE validation functions
###

def verifyDelegationSignerChain(domain):
    """Verifies that a chain of DS records exist for a zone leading to the root zone. Returns True or False."""
    checkList = []
    nodeList = domain.split('.')
    checkDomain = '' # root
    for node in reversed(nodeList):
        checkDomain = node + "." + checkDomain
        try:
            myAnswers = myResolver.query(checkDomain, "SOA")
        except:
            foo = "bar"
        else:
            checkList.append(checkDomain)
    # Now verify everything in hierarchy with an SOA record also has a DS record
    for testDomain in checkList:
        try:
            myAnswers = myResolver.query(testDomain, "DS")
        except:
            return False
    #if we got this far, DS records exist for every domain in hierarchy with an
    # SOA as queried from our local DNSSEC enforcing resolver
    return True

def protectedMXdetect(mboxd):
    """Detects DNSSEC for a mailbox domain and updates the dnssec column in the mboxdomains table."""
    ErrorContext = "protectedMXdetect()"
    if verifyDelegationSignerChain(mboxd):
        try:
            cursor.execute('UPDATE mboxdomains SET dnssec = 1 WHERE domain = %s AND failcount = 0',(mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not update dnssec column in mboxdomains table.|Exiting now.")
            terminalError()
    else:
        try:
            cursor.execute('UPDATE mboxdomains SET dnssec = 0,dane = 0 WHERE domain = %s',(mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not update dnssec,dane column in mboxdomains table.|Exiting now.")
            terminalError()
    dbase.commit()

def validateTLSA(rdata):
    """Partial validation that a TLSA record meets the DANE for SMTP standars."""
    recordData = rdata.split(' ')
    if int(recordData[0]) != 2:
        if int(recordData[0]) != 3:
            return False
    if int(recordData[1]) != 0:
        if int(recordData[1]) != 1:
            return False
    if int(recordData[2]) == 1:
        if len(recordData[3]) == 64:
            return True
        else:
            return False
    if int(recordData[2]) == 2:
        if len(recordData[3]) == 128:
            return True
        else:
            return False
    # Do not attempt to validate entire key entries
    if int(recordData[2]) == 0:
        return True
    return False

def daneProtectDetect(mboxd,rawmx):
    """Determines if more than 55% of defined MX hosts are for a mailbox domain have usable TLSA records."""
    ErrorContext = 'daneProtectDetect()'
    MXList = rawmx.split(':')
    hits = 0
    for mx in MXList:
        if verifyDelegationSignerChain(mx):
            tlsa = "_25._tcp." + mx
            try:
                myAnswers = myResolver.query(tlsa, "TLSA")
            except:
                foo = "bar"
            else:
                validEntryFormat = 0
                for rdata in myAnswers:
                    strng = str(rdata).strip()
                    if validateTLSA(strng):
                        validEntryFormat += 1
                if validEntryFormat > 0:
                    hits += 1
    percentage = 0
    if len(MXList) > 0:
        percentage = int(100 * float(hits)/float(len(MXList)))
    if percentage > 55:
        DaneValue = 1
    else:
        DaneValue = 0
    try:
        cursor.execute('UPDATE mboxdomains SET dane = %s WHERE domain = %s',(DaneValue, mboxd,))
    except:
        outputMessage("Error", ErrorContext, "Could not update dane column in mboxdomains table.|Exiting now.")
        terminalError()
    dbase.commit()

def detectDaneAliases(testList):
    """Determines whether or not a mailbox domain uses a known DANE MX provider."""
    aliasdb = []
    updatedb = []
    try:
        cursor.execute('SELECT aliaskey,policymx FROM daneprovider')
    except:
        outputMessage("Error", "detectDaneAliases()", "Could not select list of well know DANE MX Providers from daneprovider table.|Exiting now.")
        terminalError()
    for row in cursor:
        myDict = {}
        myDict['aliaskey'] = str(row[0]).strip()
        strng = str(row[1]).strip()
        plist = strng.split(':')
        myDict['policymx'] = plist
        aliasdb.append(myDict)
    for myList in testList:
        mboxDict = {}
        mboxDict['domain'] = myList['domain']
        mboxDict['danealias'] = None
        rawList = myList['rawmx'].split(':')
        for aliasDef in aliasdb:
            cleanmx = []
            for mx in rawList:
                for testpolicy in aliasDef['policymx']:
                    if testpolicy.startswith('.'):
                        if mx.endswith(testpolicy):
                            if mx not in cleanmx:
                                cleanmx.append(mx)
                    elif mx == testpolicy:
                        if mx not in cleanmx:
                            cleanmx.append(mx)
            strng = ':'.join(cleanmx)
            if strng == myList['rawmx']:
                mboxDict['danealias'] = aliasDef['aliaskey']
        updatedb.append(mboxDict)
    return updatedb

def updateDaneAliasDefinitions(updatedb):
    """Updates the mboxdomains table for mailbox domains that use a known DANE MX providers."""
    for myDict in updatedb:
        try:
            cursor.execute('UPDATE mboxdomains SET danealias = %s, mtasts=0 WHERE domain = %s',(myDict['danealias'], myDict['domain'],))
        except:
            outputMessage("Error", "updateDaneAliasDefinitions()", "Could not assign well-known DANE MX Provider to mailbox domain in mboxdomains table.|Exiting now.")
            terminalError()
    dbase.commit()

###
# Query for Domains to Process
##

# query the MX RRset for a domain, return a sorted : delimited string of MX hosts    
def parseMXRecord(mboxd):
    """Queries the MX RRset, or the existence of an A RRset if MX RRset not present, for a mailbox domain, returns as : delimited set of hostnames."""
    mx = [];
    try:
        myAnswers = myResolver.query(mboxd, "MX")
    except:
        # It is technically legal for a mailbox domain to not have an MX record
        # if the mailbox domain itself has an A record.
        try:
            myAnswers = myResolver.query(mboxd, "A")
        except:
            return '';
        return mboxd
    for rdata in myAnswers:
        strng = str(rdata).strip()
        dnsrecord = strng.split(" ",2)
        host = dnsrecord[1]
        if not host.endswith( '.' ):
            host = host + "." + mboxd
        else:
            host = host[0:-1]
        mx.append(host)
    smx = sorted(mx)
    return ':'.join(smx);

def updateRawMX(mboxd):
    """Updates the rawmx column in mailboxdomain table with : delimited list of MX hosts for the domain."""
    ErrorContext = 'updateRawMX()'
    mx = parseMXRecord(mboxd)
    if len(mx) == 0:
        try:
            cursor.execute('UPDATE mboxdomains SET validmx = 0, rawmx = %s, mxfail = %s, dnssec = 0, failcount = failcount+1 WHERE domain = %s', (None, today, mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not remove listing of valid MX records for " + mboxd + " in mboxdomains database.|Exiting now.")
            terminalError()
    else:
        try:
            cursor.execute('UPDATE mboxdomains SET validmx = 1, rawmx = %s, mxfail = "1971-01-01", failcount = 0, lastcheck = %s WHERE domain = %s', (mx, today, mboxd))
        except:
            outputMessage("Error", ErrorContext, "Could not update listing of valid MX records for " + mboxd + " in mboxdomains database.|Exiting now.")
            terminalError()
    dbase.commit()

def queryDomainsToProcess():
    """Grabs list of domains to process, limiting list to max of 1/6th of total plus a few."""
    ErrorContext = 'queryDomainsToProcess()'
    try:
        cursor.execute('SELECT COUNT(domain) FROM mboxdomains')
    except:
        outputMessage("Error", ErrorContext, "Can not get count of mailbox domains in database.|Is database running? Exiting now.")
        terminalError()
    for row in cursor:
        N = int(row[0])
    M = N / 6
    M += 5
    STRM = str(roundupTen(M))
    outputMessage("Notice", None, "Database knows about " + str(N) + " mailbox domains.|Processing up to " + STRM + " mailbox domain MX records this round.")
    checkmx = []
    try:
        cursor.execute('SELECT domain FROM mboxdomains WHERE validmx = 0 OR lastcheck < %s ORDER BY failcount,lastcheck,id LIMIT ' + STRM, (lastcheck,))
    except:
        outputMessage("Error", ErrorContext, "Initial query for domains to process failed.|Exiting now.")
        terminalError()
    for row in cursor:
        checkmx.append(str(row[0]))
    for mboxd in checkmx:
        updateRawMX(mboxd)
        protectedMXdetect(mboxd)
    outputMessage("Notice", None, "Detecting DANE protected mailbox domains.")
    daneCheckList = []
    try:
        cursor.execute('SELECT domain,rawmx FROM mboxdomains WHERE dnssec = 1 AND lastcheck = %s', (today,))
    except:
        outputMessage("Error", ErrorContext, "Query for DNSSEC protected domain list failed.|Exiting now.")
        terminalError()
    for row in cursor:
        tmpList = [str(row[0]), str(row[1])]
        daneCheckList.append(tmpList)
    for arg in daneCheckList:  
        daneProtectDetect(arg[0],arg[1])
    # domains to test rawmx against a DANE MX provider
    DANEaliasTestList = []
    try:
        cursor.execute('SELECT domain,rawmx FROM mboxdomains WHERE validmx = 1 AND dnssec = 0 AND lastcheck = %s', (today,))
    except:
        outputMessage("Error", ErrorContext, "Could not query records needed to identity mailbox domains with well-known DANE MX providers.|Exiting now.")
        terminalError()
    for row in cursor:
        checkForAlias = {}
        checkForAlias['domain'] = str(row[0]).strip()
        checkForAlias['rawmx'] = str(row[1]).strip()
        DANEaliasTestList.append(checkForAlias)
    updatedb = detectDaneAliases(DANEaliasTestList)
    updateDaneAliasDefinitions(updatedb)

###
# MTA-STS functions
###

def generateMTAcheckList():
    """Determines which mailbox domains not protected by DANE have a _mta-sts TXT record."""
    ErrorContext = 'generateMTAcheckList()'
    outputMessage("Notice", None, "Detecting MTA-STS protected mailbox domains.")
    mtaInitialDeleteList = []
    mtaInitialCheckList = []
    try:
        cursor.execute('SELECT domain FROM mboxdomains WHERE validmx = 1 AND dane = 0 AND lastcheck = %s', (today,))
    except:
        outputMessage("Error", "generateMTAcheckList()", "Could not retrieve list of domains to check for MTA-STS policies.|Exiting now.")
    for row in cursor:
        checkrecord = "_mta-sts." + str(row[0])
        try:
            myAnswers = myResolver.query(checkrecord, "TXT")
        except:
            mtaInitialDeleteList.append(str(row[0]))
        else:
            mtaInitialCheckList.append(str(row[0]))
    for mboxd in mtaInitialCheckList:
        try:
            cursor.execute('UPDATE mboxdomains SET mtasts = 1 WHERE domain = %s', (mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Unable to update mtasts column in mboxdomains table.|Exiting now.")
            terminalError()
        # this will fail when entry already exists which is what we want
        try:
            cursor.execute('INSERT INTO mtacache(domain) VALUES(%s)', (mboxd,))
        except:
            foo = 'bar'
    for mboxd in mtaInitialDeleteList:
        try:
            cursor.execute('UPDATE mboxdomains SET mtasts = 0 WHERE domain = %s', (mboxd,))
        except:
            outputMessage("Warning", "nodnsmta()", "Could not update mboxdomains table to reflect domains that should NOT be checked for MTA-STS policies.|Continuing anyway.")
    dbase.commit()

def deleteFromCache(myList):
    """Deletes a list of mailbox domains from the mtacache table."""
    ErrorContext = 'deleteFromCache()'
    for mboxd in myList:
        try:
            cursor.execute('DELETE FROM mtacache WHERE domain = %s', (mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not delete entry from mtacache table.|Exiting now.")
            terminalError()
        try:
            cursor.execute('UPDATE mboxdomains SET mtasts = 0 WHERE domain = %s', (mboxd,))
        except:
            outputMessage("Error", ErrorContext, "Could not update domain metadata in mboxdomains table.|Exiting now.")
            terminalError()
    dbase.commit()

def returnCacheVersion(strng):
    """Currently a dummy placeholder that always returns STSv1."""
    ver = strng.lower()
    # different returns in future if applicable
    # watch the RFC
    return 'STSv1'

def returnCacheMode(strng):
    """Returns the policy mode of enforce, testing, or none."""
    mode = strng.lower()
    if mode == 'enforce':
        return 'enforce'
    elif mode == 'none':
        return 'none'
    else:
        return 'testing'

def returnCacheLife(strng):
    """Returns the MTA cache life, with a minimum of 3 days and max of 6 months."""
    try:
        int(strng)
    except ValueError:
        strng = '259200'
    if int(strng) < 259200:
        strng = '259200'
    elif int(strng) > 15778800:
        strng = '15778800'
    return strng

def parsePolicyFile(mboxd, serial):
    """Retrieves MTA policy from servers and parses policy, updating mtacache table."""
    policyDict = {'domain': mboxd, 'version': 'STSv1', 'mode': 'none', 'cache': '259200'}
    policyMX = []
    policyserver = 'https://mta-sts.' + mboxd + '/.well-known/mta-sts.txt'
    try:
        response = secureGetResource(policyserver)
    except:
        return
    policyfile = response.splitlines( )
    for line in policyfile:
        lline = line.strip()
        lline = lline.replace(" .", " *.", 1)
        if lline.startswith( 'version: ' ):
            strng = lline[8:].strip()
            policyDict['version'] = returnCacheVersion(strng)
        elif lline.startswith( 'mode:' ):
            strng = lline[5:].strip()
            policyDict['mode'] = returnCacheMode(strng)
        elif lline.startswith( 'max_age:' ):
            strng = lline[8:].strip()
            policyDict['cache'] = int(returnCacheLife(strng))
        elif lline.startswith( 'mx:' ):
            policyMX.append(lline[3:].strip())
    strng = ':'.join(policyMX)
    if len(strng) == 0:
        return
    policyDict['mx'] = strng
    #newts = timestamp + int(policyDict['cache'])
    #newexpires = datetime.utcfromtimestamp(newts).strftime('%Y-%m-%d %H:%M:%S')
    newexpiees = calculateNewExpires(policyDict['cache'])
    try:
        cursor.execute('UPDATE mtacache SET cachelife = %s, policymx = %s, mode = %s, serial = %s, expires = %s WHERE domain = %s',(policyDict['cache'], policyDict['mx'], policyDict['mode'], serial, newexpires, policyDict['domain'],))
    except:
        outputMessage("Error", "parsePolicyFile()", "Could not update entries in the mtacache database table.|Exiting now.")
        terminalError()
    dbase.commit()

def checkSerialNumber(mtaDict):
    """Checks via serial number whether or not cached MTA policy needs to be re-queried from web server."""
    ErrorContext = 'checkSerialNumber()'
    updateCacheExpire = False
    try:
        cursor.execute('SELECT serial,cachelife FROM mtacache WHERE domain = %s', (mtaDict['domain'],))
    except:
        outputMessage("Error", "checkSerialNumber()", "Could not select MTA-STS serial number and cache life from mtacache.|Exiting now.")
        terminalError()
    for row in cursor:
        serial = str(row[0])
        if serial == mtaDict['serial']:
            #ts = (timestamp + int(row[1]) - 86400)
            myseconds = (int(row[1] - 86400))
            newexpire = calculateNewExpires(myseconds)
            updateCacheExpire = True
    if updateCacheExpire:
        try:
            cursor.execute('UPDATE mtacache SET expires = %s where domain = %s', (newexpire, mtaDict['domain'],))
        except:
            outputMessage("Warning", ErrorContext, "Could not update MTA-STS policy expiration.|Continuing anyway.")
        dbase.commit()
    else:
        parsePolicyFile(mtaDict['domain'], mtaDict['serial'])

def policyFromDNS(mboxd):
    """Queries and parses the _mta-sts.[mailboxdomain] TXT record."""
    mboxd = str(mboxd)
    #mtaDeleteList = []
    validRecord = True
    checkrecord = "_mta-sts." + mboxd
    try:
        myAnswers = myResolver.query(checkrecord, "TXT")
    except:
        validRecord = False
    if validRecord:
        for rdata in myAnswers:
            strng = str(rdata).strip()
            # removes quotes on the record
            strng = strng[1:-1].strip()
            if not strng.startswith( 'v=STSv1' ):
                validRecord = False
    if validRecord:
        dnsrecord = strng.split(";",2)
        version = dnsrecord[0].strip()[2:]
        serial = dnsrecord[1].strip()
        if not serial.startswith( 'id=' ):
            validRecord = False
    if validRecord:
        serial = serial[3:]
        checkSerialNumber({ 'domain': mboxd, 'version': version, 'serial': serial, })
    else:
        deleteFromCache([mboxd])

def fetchMTApolicies():
    """Determines what MTA-STS protected domains need policy to be queried and queries them."""
    ErrorContext = 'fetchMTApolicies()'
    # policies to fetch/refetch
    refreshPolicyList = []
    try:
        cursor.execute('SELECT domain FROM mtacache WHERE expires < %s', (today,))
    except:
        outputMessage("Error", ErrorContext, "Query for list of domains with expired MTA-STS policy cached failed.|Exiting now.")
        terminalError()
    for row in cursor:
        refreshPolicyList.append(str(row[0]))
    for mboxd in refreshPolicyList:
        policyFromDNS(mboxd)
    deleteList = []
    try:
        cursor.execute('SELECT domain FROM mtacache WHERE policymx IS NULL')
    except:
        outputMessage("Error", ErrorContext, "Could not select MTA-STS cache domains with null MX policy.|Exiting now.")
        terminalError()
    for row in cursor:
        mbox = str(row[0]).strip()
        deleteList.append(mbox)
    try:
        cursor.execute('SELECT domain FROM mtacache WHERE mode="none"')
    except:
        outputMessage("Error", ErrorContext, "Could not select MTA-STS cache domains with policy mode of none.|Exiting now.")
        terminalError()
    for row in cursor:
        mbox = str(row[0]).strip()
        if row not in deleteList:
            deleteList.append(mbox)
    deleteFromCache(deleteList)

def updatePolicyDBMX(mtamx):
    """Updates mtacache table with MX hosts that validate against the MTA-STS policy."""
    cleanmx = mtamx['cleanmx'].strip()
    if len(cleanmx) == 0:
        cleanmx = None
    try:
        cursor.execute('UPDATE mtacache SET cleanmx = %s WHERE domain = %s',(cleanmx, mtamx['mboxd']))
    except:
        outputMessage("Error", "updatePolicyDBMX()", "Could not update sanitized MX list in mtacache database.|Exiting now.")
        terminalError()
    dbase.commit()
   
def seekPolicy(pDict):
    """Justifies MX RRset against a MTA-STS policy."""
    mtamx = {}
    mxList = pDict['rawmx'].split(':')
    pList = pDict['policy'].split(':')
    justifiedPolicy = []
    for mx in mxList:
        mx = mx.lower().strip()
        for p in pList:
            p = p.lower().strip()
            if p == mx:
                justifiedPolicy.append(mx)
            elif p.startswith("*"):
                p = p[1:]
                if mx.endswith(p):
                    justifiedPolicy.append(mx)
    cleanMX = ':'.join(justifiedPolicy)
    mtamx['mboxd'] = pDict['domain']
    mtamx['cleanmx'] = cleanMX
    updatePolicyDBMX(mtamx)

def sanitizeMXagainstMTASTS():
    """Queries database for MX RRset and MTA-STS MX policy to create a sanitized list of MX servers."""
    ErrorContext = 'sanitizeMXagainstMTASTS()'
    # make the clean MX that adheres to policy
    justifyMX = []
    try:
        cursor.execute('SELECT mboxdomains.domain, mboxdomains.rawmx, mtacache.policymx FROM mboxdomains,mtacache WHERE mboxdomains.domain = mtacache.domain')
    except:
        outputMessage("Error", ErrorContext, "Could not query records needs to sanitize MX records against MtA-STS policies.|Exiting now.")
        terminalError()
    for row in cursor:
        tempDict = {}
        tempDict['domain'] = str(row[0])
        tempDict['rawmx'] = str(row[1])
        tempDict['policy'] = str(row[2])
        justifyMX.append(tempDict)
    for pDict in justifyMX:
        seekPolicy(pDict)
    deleteList = []
    try:
        cursor.execute('SELECT domain FROM mtacache WHERE cleanmx IS NULL')
    except:
        outputMessage("Error", ErrorContext, "Could not selecy MTA-STS cache domains without any sanitized MX records.|Exiting now.")
        terminalError()
    for row in cursor:
        mboxd = str(row[0]).strip()
        deleteList.append(mboxd)
    deleteFromCache(deleteList)

def detectSecureAliases(testList):
    """Detects whether or not a mailbox domain uses a PKIX MX provider we know about."""
    aliasdb = []
    updatedb = []
    try:
        cursor.execute('SELECT aliaskey,policymx FROM pkixprovider')
    except:
        outputMessage("Error", "detectSecureAliases()", "Could not query PKIX validating MX Provider list.|Exiting now.")
        terminalError()
    for row in cursor:
        myDict = {}
        myDict['aliaskey'] = str(row[0]).strip()
        strng = str(row[1]).strip()
        plist = strng.split(':')
        myDict['policymx'] = plist
        aliasdb.append(myDict)
    for myList in testList:
        mboxDict = {}
        mboxDict['domain'] = myList['domain']
        mboxDict['pkixalias'] = None
        rawList = myList['rawmx'].split(':')
        for aliasDef in aliasdb:
            cleanmx = []
            for mx in rawList:
                for testpolicy in aliasDef['policymx']:
                    if testpolicy.startswith('.'):
                        if mx.endswith(testpolicy):
                            if mx not in cleanmx:
                                cleanmx.append(mx)
                    elif mx == testpolicy:
                        if mx not in cleanmx:
                            cleanmx.append(mx)
            strng = ':'.join(cleanmx)
            if strng == myList['rawmx']:
                mboxDict['pkixalias'] = aliasDef['aliaskey']
        updatedb.append(mboxDict)
    return updatedb

def updateSecureAliasDefinitions(updatedb):
    """Updates mboxdomains to reflect that a mailbox domain uses a known PKIX MX provider."""
    for myDict in updatedb:
        try:
            cursor.execute('UPDATE mboxdomains SET pkixalias = %s WHERE domain = %s',(myDict['pkixalias'], myDict['domain'],))
        except:
            outputMessage("Error", "updateSecureAliasDefinitions()", "Could not update mboxdomains table to reflect mailbox domains identified using PKIX validating MX Providers.|Exiting now.")
            terminalError()
    dbase.commit()

def identifyWellKnownPKI():
    """Checks mailbox domains using PKIX validating MX providers."""
    PKIaliasTestList = []
    outputMessage("Notice", None, "Detecting mailbox domains using TLS 1.2 PKIX Validating Well-Known MX Providers")
    try:
        cursor.execute('SELECT domain,rawmx FROM mboxdomains WHERE validmx = 1 AND dane = 0 AND mtasts = 0 AND danealias IS NULL AND lastcheck = %s', (today,))
    except:
        outputMessage("Error", "identifyWellKnownPKI()", "Could not query records needed to identity mailbox domains with well-known PKIX validating MX providers.|Exiting now.")
        terminalError()
    for row in cursor:
        checkForAlias = {}
        checkForAlias['domain'] = str(row[0]).strip()
        checkForAlias['rawmx'] = str(row[1]).strip()
        PKIaliasTestList.append(checkForAlias)
    updatedb = detectSecureAliases(PKIaliasTestList)
    updateSecureAliasDefinitions(updatedb)

###
# STARTTLS Everywhere
###

def santizeSTLSmx(myList):
    ErrorContext = 'santizeSTLSmx()'
    for myDict in myList:
        cleanmxList = []
        mxList = myDict['rawmx'].split(':')
        policymxstr = ''
        try:
            cursor.execute('SELECT policymx FROM stlscache WHERE domain = %s AND policymx IS NOT NULL', (myDict['domain'],))
        except:
            outputMessage("Error", ErrorContext, "Could not query STARTTLS policies from STARTTLS policy cache table.|Exiting now.")
            terminalError()
        for row in cursor:
            policymxstr = str(row[0]).strip()
        policyList = policymxstr.split(':')
        for mx in mxList:
            for policy in policyList:
                if policy.startswith('.'):
                    if mx.endswith(policy):
                        if mx not in cleanmxList:
                            cleanmxList.append(mx)
                elif mx == policy:
                    if mx not in cleanmxList:
                        cleanmxList.append(mx)
        cleanmx = ':'.join(cleanmxList)
        if len(cleanmx) > 0:
            try:
                cursor.execute('UPDATE stlscache SET cleanmx = %s WHERE domain = %s', (cleanmx, myDict['domain'],))
            except:
                outputMessage("Error", ErrorContext, "Could not updated sanitized MX list in STARTTLS Everywhere policy cache.")
                terminalError()
        else:
            try:
                cursor.execute('UPDATE stlscache SET cleanmx = %s WHERE domain = %s', (None, myDict['domain'],))
            except:
                outputMessage("Error", ErrorContext, "Could not updated sanitized MX list in STARTTLS Everywhere policy cache.")
                terminalError()
    dbase.commit()

# STARTTLS Everywhere
def applyStartTLSEverywhere():
    outputMessage("Notice", None, "Detecting mailbox domains protected by STARTTLS Everywhere.")
    checkForSTLSPolicy = []
    try:
        cursor.execute('SELECT domain,rawmx FROM mboxdomains WHERE validmx = 1 AND dane = 0 AND mtasts = 0 AND starttls = 1 AND lastcheck = %s AND pkixalias IS NULL', (today,))
    except:
        outputMessage("Error", "applyStartTLSEverywhere()", "Could not query records needed to sanitize MX records against STARTTLS Everywhere policies.|Exiting now.")
        terminalError()
    for row in cursor:
        checkDict = {}
        checkDict['domain'] = str(row[0]).strip()
        checkDict['rawmx'] = str(row[1]).strip()
        checkForSTLSPolicy.append(checkDict)
    santizeSTLSmx(checkForSTLSPolicy)

###
# clean up
###

def triggerMailLog():
    """Returns True if conditions met to e-mail log to admin, otherwise returnsFalse"""
    if not sendMessageLog:
        return False
    if timestamp is None:
        # should never happen
        return False
    # only send on Sunday
    if datetime.utcfromtimestamp(timestamp).weekday() != 6:
        # Unlike standard UNIX cron, 6 is Sunday in Python (6 is Saturday in UNIX cron)
        return False
    try:
        checkAlreadySent = r.get("mtastsbd-logs-retrieved")
    except:
        # should never happen
        return False
    if  checkAlreadySent is not None:
        # Already sent it this week
        return False
    return True

def emailLog():
    logcount = 0
    #sevenAgo = timestamp - (604800)
    #twentyEightAgo = timestamp - (2419200)
    #lastsunday = datetime.utcfromtimestamp(sevenAgo).strftime('%Y-%m-%d') + ' 00:00:00'
    lastSunsay = startOfLastSunday()
    #nuke = datetime.utcfromtimestamp(twentyEightAgo).strftime('%Y-%m-%d') + ' 00:00:00'
    nuke = startOfFourWreeksAgoSunday()
    try:
        cursor.execute('SELECT count(id) FROM logmessages WHERE tstamp >= %s', (lastsunday,))
    except:
        return
    for row in cursor:
        logcount = int(row[0])
    if logcount == 0:
        return
    # now get the logs
    if emailLogIgnoreNotice:
        try:
            cursor.execute('SELECT tstamp,type,mode,context,message FROM logmessages WHERE type != "Notice" AND tstamp >= %s', (lastsunday,))
        except:
            return
    else:
        try:
            cursor.execute('SELECT tstamp,type,mode,context,message FROM logmessages WHERE tstamp >= %s', (lastsunday,))
        except:
            return
    # format message
    message = 'All log timestamps are in UTC' + "\n\n"
    for row in cursor:
        tstamp = str(row[0]).strip()
        ltype = str(row[1]).strip()
        mode = str(row[2]).strip()
        context = str(row[3]).strip()
        lmess = str(row[4]).strip()
        message = message + tstamp + " ||| " + ltype + " ||| " + mode + " ||| " + context + "\n    " + lmess + "\n--\n"
    #print(message)
    import getpass
    import smtplib
    from email.mime.text import MIMEText
    username = getpass.getuser()
    if emailLogFromAddress is None:
        emailLogFromAddress = username + '@localhost'
    if emailLogToAddress is None:
        emailLogToAddress = username + '@localhost'
    msg = MIMEText(message)
    msg['Subject'] = 'mtastsdb logged messages'
    msg['From'] = emailLogFromAddress
    msg['To'] = emailLogToAddress
    s = smtplib.SMTP('localhost')
    try:
        s.sendmail(emailLogFromAddress, [emailLogToAddress], msg.as_string())
    except:
        outputMessage("Warning", "emailLog()", "Failed to send log messages")
        return
    s.quit
    try:
        cursor.execute('DELETE FROM logmessages WHERE tstamp < %s', (nuke,))
    except:
        return
    dbase.commit()
    r.set("mtastsbd-logs-retrieved", "foo")
    # lock for 24 hours
    r.expire("mtastsbd-logs-retrieved", 86401)
    outputMessage("Notice", None, "Check your mail for logged messages.")

def janitorService():
    ErrorContext = 'janitorService()'
    outputMessage("Notice", None, "Cleaning up.")
    deleteList = []
    checkList = []
    #fourdaysago = timestamp - 345600
    #fifteendaysago = timestamp - 1296000
    #sixmonthsago = timestamp - 15778800
    #twelvemonthsago = timestamp - 31557600
    fourdago = datetimeDaysAgo(4)
    fifteendago = datetimeDaysAgo(15)
    sixmoago = datetimeDaysAgo(182.625)
    twelvemoago = datetimeDaysAgo(365.25)
    # deprecated tls and blacklist management
    try:
        cursor.execute('DELETE FROM deprecatedtls WHERE insdate < %s', (twelvemoago,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries from deprecatedtls table.")
    try:
        cursor.execute('DELETE FROM policyblacklist WHERE insdate < %s', (sixmoago,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries from policyblacklist table.")
    try:
        cursor.execute('DELETE FROM improperdane WHERE insdate < %s', (fifteendago,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries from improperdane table.")
    try:
        cursor.execute('DELETE FROM improperdane WHERE danefail = 1 AND insdate < %s', (fourdago,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries since removed from the DANE Fail List from improperdane table.")
    try:
        cursor.execute('DELETE FROM lackstls WHERE insdate < %s', (twelvemoago,))
    except:
        outputMessage("Warning", ErrorContext, "Could not delete entries from the lackstls table.")
    dbase.commit()
    # end deprecated tls and blacklist management
    try:
        cursor.execute('SELECT domain FROM mboxdomains WHERE failcount > 36')
    except:
        outputMessage("Error", ErrorContext, "Could not select domains will excessive failed name resolution count.|Exiting now.")
        terminalError()
    for row in cursor:
        strng = str(row[0]).strip()
        if strng not in deleteList:
            deleteList.append(strng)
    try:
        cursor.execute('SELECT domain FROM policyblacklist')
    except:
        outputMessage("Error", ErrorContext, "Could not select domains from policyblacklist table.|Exiting now.")
        terminalError()
    for row in cursor:
        strng = str(row[0]).strip()
        if strng not in deleteList:
            deleteList.append(strng)
    # should trigger about twice a week
    if randint(0,100) == 67:
        try:
            cursor.execute('SELECT domain FROM mboxdomains WHERE dane = 0 AND mtasts = 0 AND pkixalias IS NULL AND danealias IS NULL AND lasthit < %s',(sixmoago,))
        except:
            outputMessage("Error", ErrorContext, "Could not select domains without policies that have not been touched in six months.|Exiting now")
            terminalError()
        for row in cursor:
            strng = str(row[0]).strip()
            if strng not in deleteList:
                deleteList.append(strng)
    for domain in deleteList:
        try:
            cursor.execute('DELETE FROM mboxdomains WHERE domain = %s', (domain,))
        except:
            outputMessage("Warning", ErrorContext, "Could not delete domains from mboxdomains table.")
        try:
            cursor.execute('DELETE FROM mtacache WHERE domain = %s', (domain,))
        except:
            outputMessage("Warning", ErrorContext, "Could not delete domains from mtacache table.")
        try:
            cursor.execute('DELETE FROM stlscache WHERE domain = %s', (domain,))
        except:
            outputMessage("Warning", ErrorContext, "Could not delete domains from stlscache table.")
    # will only happen if can not get fresh data from STARTTLS Everywhere
    expiredAliasList = []
    try:
        cursor.execute('SELECT aliaskey FROM pkixprovider WHERE expires < %s', (today,))
    except:
        outputMessage("Error", ErrorContext, "Could not select exired aliaskey from pkixprovider table.|Exiting now")
        terminalError()
    for row in cursor:
        strng = str(row[0]).strip()
        expiredAliasList.append(strng)
    for alias in expiredAliasList:
        try:
            cursor.execute('UPDATE mboxdomains SET pkixalias = %s WHERE pkixalias = %s', (None, alias,))
        except:
            outputMessage("Warning", ErrorContext, "Could not update pkixalias column in mboxdomains table.|Continuing anyway.")
        try:
            cursor.execute('DELETE FROM pkixprovider WHERE aliaskey = %s', (alias,))
        except:
            outputMessage("Warning", ErrorContext, "Could not delete aliaskey from pkixprovider table.|Continuing anyway.")
    expiredCacheList = []
    try:
        cursor.execute('SELECT domain FROM stlscache WHERE expires < %s', (today,))
    except:
        outputMessage("Error", ErrorContext, "Could not select expired domains from stlscache table.|Exiting now.")
        terminalError()
    for row in cursor:
        strng = str(row[0]).strip()
        expiredCacheList.append(strng)
    for domain in expiredCacheList:
        try:
            cursor.execute('UPDATE mboxdomains SET starttls = 0 WHERE domain = %s', (domain,))
        except:
            outputMessage("Warning", ErrorContext, "Could not update starttls column in mboxdomains table.|Continuing anyway.")
        try:
            cursor.execute('DELETE FROM stlscache WHERE domain = %s', (domain,))
        except:
            outputMessage("Warning", ErrorContext, "Could not delete domain from startcache table.|Continuing anyway.")
    # END will only happen if can not get fresh data from STARTTLS Everywhere
    dbase.commit()
    if triggerMailLog():
        emailLog()
    # And we are finished
    cursor.close()
    dbase.close()
    unsetLockFile()
    outputMessage("Notice", None, "Database updated. Goodbye.")
    sys.exit(0)
        
# end of function definitions except main

def main(argv):
    global vmode
    global dbuser
    global dbpass
    global redispass
    global cabundle
    global LOCALDNS
    global myResolver
    #global timestamp
    #global twelveago
    global today
    global lastcheck
    staleLockRemove = False
    try:
        opts, args = getopt.getopt(argv,shortswitches,longswitches)
    except getopt.GetoptError:
        sys.exit(ARGVusage)
    for opt, arg in opts:
        if opt in ("-s", "--silent"):
            vmode = 'silent'
        elif opt in ("-h", "--help"):
            print(ARGVusage)
            sys.exit(0)
        elif opt in ("-u", "--dbuser"):
            dbuser = arg
        elif opt in ("-p", "--dbpass"):
            dbpass = arg
        elif opt in ("-r", "--redis-pass"):
            redispass = arg
        elif opt in ("--pem"):
            cabundle = arg
        elif opt in ("--delete-lock"):
            staleLockRemove = True

    if redispass is None:
        outputMessage("Error", None, "No password specified for Redis database.")
        sys.exit(1)
    redisConnect()
    
    if staleLockRemove:
        unsetLockFile()

    if not setLockFile():
        outputMessage('Notice', None, 'The script appears to already be running. Exiting.')
        sys.exit(1)
    
    if dbuser is None:
        outputMessage("Error", None, "You did not supply a MariaDB username.")
        terminalError()
    elif dbpass is None:
        outputMessage("Error", None, "You did not supply a MariaDB password.")
        terminalError()
    mariadbConnect()
    checkDatabaseVersion()

    testInternetConnectivity()
    LOCALDNS = getLocalDNS()
    try:
        value = r.get('mtacachedb-ResolverTest')
    except:
        exit("I can not connect to Redis database. Is it running? Is the password correct?")
    if value is None:
        testLocalResolverForDNSSEC(LOCALDNS)
    # now we can use the friendlier Resolver object
    myResolver = dns.resolver.Resolver()
    myResolver.nameservers = [LOCALDNS]
    # prevent failures because of results not cached on
    # networks with poor connectivity
    myResolver.timeout = 15

    # detect the CA Root Trust Anchor bundle
    detectTrustAnchorBundle()

    #timestamp = int(time.time())
    #twelveago = int(timestamp - (12 * 3600))

    #today = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
    today = datetimeDaysAgo(0)
    #lastcheck = datetime.utcfromtimestamp(twelveago).strftime('%Y-%m-%d %H:%M:%S')
    lastcheck = datetimeDaysAgo(0.5)

    if detectNeedJSON():
        outputMessage("Notice", None, "Attempting to query needed JSON data.")
        staggarJSONFetch()
        querySTLSeverywherePolicyJSON()
        queryMXProviderPolicyJSON()
        getDaneFailList()

    queryDomainsToProcess()
    generateMTAcheckList()    
    fetchMTApolicies()
    sanitizeMXagainstMTASTS()
    identifyWellKnownPKI()
    applyStartTLSEverywhere()
    janitorService()

if __name__ == "__main__":
    main(sys.argv[1:])
