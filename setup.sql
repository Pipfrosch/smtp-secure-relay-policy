USE mtastsdb;

-- For a description of these tables and the columns, see Documentation/DatabaseTables.md

DROP TABLE IF EXISTS metadata;

CREATE TABLE metadata (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
codename  VARCHAR(32)         NOT NULL UNIQUE,
majorv    TINYINT UNSIGNED    NOT NULL,
minorv    TINYINT UNSIGNED    NOT NULL,
patchv    TINYINT UNSIGNED    NOT NULL,
compatv   CHAR(8)             NOT NULL,
variant   TINYINT UNSIGNED    NOT NULL DEFAULT 0,
vpatch    TINYINT UNSIGNED    NOT NULL DEFAULT 0,
active    TINYINT UNSIGNED    NOT NULL DEFAULT 0,
loaddate  TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

INSERT INTO metadata(codename,majorv,minorv,patchv,compatv,active) VALUES('Genesis',0,1,0,'00000100',1);

DROP TABLE IF EXISTS mboxdomains;

CREATE TABLE mboxdomains (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
validmx   TINYINT             NOT NULL DEFAULT 0,
dnssec    TINYINT             NOT NULL DEFAULT 0,
dane      TINYINT             NOT NULL DEFAULT 0,
mtasts    TINYINT             NOT NULL DEFAULT 0,
starttls  TINYINT             NOT NULL DEFAULT 0,
pkixalias VARCHAR(64),
danealias VARCHAR(64),
rawmx     TEXT,
lastcheck DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
mxfail    DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
lasthit   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
failcount TINYINT             NOT NULL DEFAULT 0
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS mtacache;

CREATE TABLE mtacache (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
version   VARCHAR(12)         NOT NULL DEFAULT 'STSv1',
policymx  TEXT,
cleanmx   TEXT,
serial    VARCHAR(64)         NOT NULL DEFAULT '0',
cachelife MEDIUMINT UNSIGNED  NOT NULL DEFAULT 604800,
mode      VARCHAR(12)         NOT NULL DEFAULT 'none',
expires   DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00'
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS stlscache;

CREATE TABLE stlscache (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
policymx  TEXT,
cleanmx   TEXT,
mode      VARCHAR(12)         NOT NULL DEFAULT 'none',
expires   DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
lastcheck DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00'
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS pkixprovider;

CREATE TABLE pkixprovider (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
aliaskey  VARCHAR(64)         NOT NULL UNIQUE,
policymx  TEXT,
lastcheck DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
expires   DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS daneprovider;

CREATE TABLE daneprovider (
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
aliaskey  VARCHAR(64)         NOT NULL UNIQUE,
policymx  TEXT,
lastcheck DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
expires   DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00',
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS deprecatedtls;

CREATE TABLE deprecatedtls(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS policyblacklist;

CREATE TABLE policyblacklist(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS improperdane;

CREATE TABLE improperdane(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
danefail  TINYINT             NOT NULL DEFAULT 0,
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS lackstls;

CREATE TABLE lackstls(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain    VARCHAR(255)        NOT NULL UNIQUE,
insdate   TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS logmessages;

CREATE TABLE logmessages(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
tstamp    DATETIME            NOT NULL,
type      VARCHAR(12)         NOT NULL DEFAULT 'Notice',
mode      VARCHAR(12)         NOT NULL DEFAULT 'silent',
context   varchar(32)         NOT NULL,
message   TEXT                NOT NULL DEFAULT 'Empty Message'
) DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS rootanchor;

CREATE TABLE rootanchor(
id        MEDIUMINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
common    VARCHAR(64)         NOT NULL UNIQUE,
ski       CHAR(40)            NOT NULL UNIQUE,
required  TINYINT             NOT NULL DEFAULT 0,
expires   DATETIME            NOT NULL,
insdate   DATETIME            NOT NULL DEFAULT '1971-01-01 00:00:00'
) DEFAULT CHARACTER SET utf8;

-- These three are needed for initial fetching of JSON files and DANE Fail list. Entries are
-- maintained by the https://dnssec.icu/mtastsdb/mx-providers.json file.
INSERT INTO rootanchor(common,ski,required,expires) VALUES ('DST Root CA X3', 'c4a7b1a47b2c71fadbe14b9075ffc41560858910', 1, '2021-09-30 14:01:15');
INSERT INTO rootanchor(common,ski,required,expires) VALUES ('Certum Trusted Network CA', '0876cdcb07ff24f6c5cdedbb90bce284374675f7', 1, '2029-12-31 12:07:37');
INSERT INTO rootanchor(common,ski,required,expires) VALUES ('DigiCert High Assurance EV Root CA', 'b13ec36903f8bf4701d498261a0802ef63642bc3', 1, '2031-11-10 00:00:00');


-- These are domains that as of 2019-02-24 are not in STARTTLS Everywhere. Think of them as
-- a starter pack that supplements what is in STARTTLS Everywhere. Not all of them result
-- result in a secure policy, many do not, but they are still commonly used mailbox domains
-- so they are well-worth monitoring to detect when they can generate a secure policy.
--
-- Several of them do result in a secure policy now.

INSERT INTO mboxdomains(domain) VALUES('aim.com');
INSERT INTO mboxdomains(domain) VALUES('alice.it');
INSERT INTO mboxdomains(domain) VALUES('aliceadsl.fr');
INSERT INTO mboxdomains(domain) VALUES('amazon.com');
INSERT INTO mboxdomains(domain) VALUES('arizona.edu');
INSERT INTO mboxdomains(domain) VALUES('asp4all.nl');
INSERT INTO mboxdomains(domain) VALUES('asu.edu');
INSERT INTO mboxdomains(domain) VALUES('arcor.de');
INSERT INTO mboxdomains(domain) VALUES('att.net');
INSERT INTO mboxdomains(domain) VALUES('avansplus.de');
INSERT INTO mboxdomains(domain) VALUES('bellsouth.net');
INSERT INTO mboxdomains(domain) VALUES('berkeley.edu');
INSERT INTO mboxdomains(domain) VALUES('bhosted.nl');
INSERT INTO mboxdomains(domain) VALUES('bigpond.com');
INSERT INTO mboxdomains(domain) VALUES('bigpond.net.au');
INSERT INTO mboxdomains(domain) VALUES('bitpath.de');
INSERT INTO mboxdomains(domain) VALUES('bjrn.io');
INSERT INTO mboxdomains(domain) VALUES('bluewin.ch');
INSERT INTO mboxdomains(domain) VALUES('blueyonder.co.uk');
INSERT INTO mboxdomains(domain) VALUES('bol.com.br');
INSERT INTO mboxdomains(domain) VALUES('bund.de');
INSERT INTO mboxdomains(domain) VALUES('centurytel.net');
INSERT INTO mboxdomains(domain) VALUES('centos.org');
INSERT INTO mboxdomains(domain) VALUES('charter.net');
INSERT INTO mboxdomains(domain) VALUES('chello.nl');
INSERT INTO mboxdomains(domain) VALUES('club-internet.fr');
INSERT INTO mboxdomains(domain) VALUES('colorado.edu');
INSERT INTO mboxdomains(domain) VALUES('cox.net');
INSERT INTO mboxdomains(domain) VALUES('dd24.net');
INSERT INTO mboxdomains(domain) VALUES('debian.org');
INSERT INTO mboxdomains(domain) VALUES('denic.de');
INSERT INTO mboxdomains(domain) VALUES('deviant.email');
INSERT INTO mboxdomains(domain) VALUES('digitalocean.com');
INSERT INTO mboxdomains(domain) VALUES('domeneshop.no');
INSERT INTO mboxdomains(domain) VALUES('earthlink.net');
INSERT INTO mboxdomains(domain) VALUES('enchantrixempire.com');
INSERT INTO mboxdomains(domain) VALUES('fau.de');
INSERT INTO mboxdomains(domain) VALUES('fbi.gov');
INSERT INTO mboxdomains(domain) VALUES('free.fr');
INSERT INTO mboxdomains(domain) VALUES('freebsd.org');
INSERT INTO mboxdomains(domain) VALUES('freenet.de');
INSERT INTO mboxdomains(domain) VALUES('frontiernet.net');
INSERT INTO mboxdomains(domain) VALUES('gmx.at');
INSERT INTO mboxdomains(domain) VALUES('gmx.com');
INSERT INTO mboxdomains(domain) VALUES('gmx.de');
INSERT INTO mboxdomains(domain) VALUES('gmx.net');
INSERT INTO mboxdomains(domain) VALUES('googlemail.com');
INSERT INTO mboxdomains(domain) VALUES('goppold.net');
INSERT INTO mboxdomains(domain) VALUES('hetnet.nl');
INSERT INTO mboxdomains(domain) VALUES('home.nl');
INSERT INTO mboxdomains(domain) VALUES('honigdachse.de');
INSERT INTO mboxdomains(domain) VALUES('hotmail.co.uk');
INSERT INTO mboxdomains(domain) VALUES('hotmail.de');
INSERT INTO mboxdomains(domain) VALUES('hotmail.es');
INSERT INTO mboxdomains(domain) VALUES('hotmail.fr');
INSERT INTO mboxdomains(domain) VALUES('hotmail.it');
INSERT INTO mboxdomains(domain) VALUES('houghton.edu');
INSERT INTO mboxdomains(domain) VALUES('hr-manager.net');
INSERT INTO mboxdomains(domain) VALUES('ietf.org');
INSERT INTO mboxdomains(domain) VALUES('ig.com.br');
INSERT INTO mboxdomains(domain) VALUES('isoc.org');
INSERT INTO mboxdomains(domain) VALUES('juno.com');
INSERT INTO mboxdomains(domain) VALUES('kabelmail.de');
INSERT INTO mboxdomains(domain) VALUES('laposte.net');
INSERT INTO mboxdomains(domain) VALUES('latinist.net');
INSERT INTO mboxdomains(domain) VALUES('laxu.de');
INSERT INTO mboxdomains(domain) VALUES('ldwg.us');
INSERT INTO mboxdomains(domain) VALUES('ledovskis.lv');
INSERT INTO mboxdomains(domain) VALUES('lemon-kiwi.co');
INSERT INTO mboxdomains(domain) VALUES('levinus.de');
INSERT INTO mboxdomains(domain) VALUES('libero.it');
INSERT INTO mboxdomains(domain) VALUES('linode.com');
INSERT INTO mboxdomains(domain) VALUES('live.ca');
INSERT INTO mboxdomains(domain) VALUES('live.co.uk');
INSERT INTO mboxdomains(domain) VALUES('live.com.au');
INSERT INTO mboxdomains(domain) VALUES('live.fr');
INSERT INTO mboxdomains(domain) VALUES('live.it');
INSERT INTO mboxdomains(domain) VALUES('live.nl');
INSERT INTO mboxdomains(domain) VALUES('lrz.de');
INSERT INTO mboxdomains(domain) VALUES('mac.com');
INSERT INTO mboxdomains(domain) VALUES('mail.ru');
INSERT INTO mboxdomains(domain) VALUES('namecheap.com');
INSERT INTO mboxdomains(domain) VALUES('neuf.fr');
INSERT INTO mboxdomains(domain) VALUES('nic.cz');
INSERT INTO mboxdomains(domain) VALUES('nlnetlabs.nl');
INSERT INTO mboxdomains(domain) VALUES('nsa.gov');
INSERT INTO mboxdomains(domain) VALUES('ntlworld.com');
INSERT INTO mboxdomains(domain) VALUES('nwytg.net');
INSERT INTO mboxdomains(domain) VALUES('octopuce.fr');
INSERT INTO mboxdomains(domain) VALUES('open.ch');
INSERT INTO mboxdomains(domain) VALUES('openssl.org');
INSERT INTO mboxdomains(domain) VALUES('optonline.net');
INSERT INTO mboxdomains(domain) VALUES('optusnet.com.au');
INSERT INTO mboxdomains(domain) VALUES('orange.fr');
INSERT INTO mboxdomains(domain) VALUES('oregonstate.edu');
INSERT INTO mboxdomains(domain) VALUES('otvi.nl');
INSERT INTO mboxdomains(domain) VALUES('overheid.nl');
INSERT INTO mboxdomains(domain) VALUES('paciellogroup.com');
INSERT INTO mboxdomains(domain) VALUES('planet.nl');
INSERT INTO mboxdomains(domain) VALUES('posteo.de');
INSERT INTO mboxdomains(domain) VALUES('prolocation.net');
INSERT INTO mboxdomains(domain) VALUES('prolocation.nl');
INSERT INTO mboxdomains(domain) VALUES('rambler.ru');
INSERT INTO mboxdomains(domain) VALUES('rediffmail.com');
INSERT INTO mboxdomains(domain) VALUES('redhat.com');
INSERT INTO mboxdomains(domain) VALUES('ruhr-uni-bochum.de');
INSERT INTO mboxdomains(domain) VALUES('sbcglobal.net');
INSERT INTO mboxdomains(domain) VALUES('sfr.fr');
INSERT INTO mboxdomains(domain) VALUES('shaw.ca');
INSERT INTO mboxdomains(domain) VALUES('sky.com');
INSERT INTO mboxdomains(domain) VALUES('skynet.be');
INSERT INTO mboxdomains(domain) VALUES('spu.edu');
INSERT INTO mboxdomains(domain) VALUES('stanford.edu');
INSERT INTO mboxdomains(domain) VALUES('sympatico.ca');
INSERT INTO mboxdomains(domain) VALUES('t-online.de');
INSERT INTO mboxdomains(domain) VALUES('tease.email');
INSERT INTO mboxdomains(domain) VALUES('telenet.be');
INSERT INTO mboxdomains(domain) VALUES('terra.com.br');
INSERT INTO mboxdomains(domain) VALUES('tin.it');
INSERT INTO mboxdomains(domain) VALUES('tiscali.co.uk');
INSERT INTO mboxdomains(domain) VALUES('tiscali.it');
INSERT INTO mboxdomains(domain) VALUES('torproject.org');
INSERT INTO mboxdomains(domain) VALUES('tum.de');
INSERT INTO mboxdomains(domain) VALUES('ucla.edu');
INSERT INTO mboxdomains(domain) VALUES('uni-erlangen.de');
INSERT INTO mboxdomains(domain) VALUES('unitybox.de');
INSERT INTO mboxdomains(domain) VALUES('uol.com.br');
INSERT INTO mboxdomains(domain) VALUES('uoregon.edu');
INSERT INTO mboxdomains(domain) VALUES('usc.edu');
INSERT INTO mboxdomains(domain) VALUES('uta.edu');
INSERT INTO mboxdomains(domain) VALUES('uvt.nl');
INSERT INTO mboxdomains(domain) VALUES('uw.edu');
INSERT INTO mboxdomains(domain) VALUES('verizon.net');
INSERT INTO mboxdomains(domain) VALUES('virgilio.it');
INSERT INTO mboxdomains(domain) VALUES('w3.org');
INSERT INTO mboxdomains(domain) VALUES('wanadoo.fr');
INSERT INTO mboxdomains(domain) VALUES('washington.edu');
INSERT INTO mboxdomains(domain) VALUES('web.de');
INSERT INTO mboxdomains(domain) VALUES('webcruitermail.no');
INSERT INTO mboxdomains(domain) VALUES('windstream.net');
INSERT INTO mboxdomains(domain) VALUES('wsu.edu');
INSERT INTO mboxdomains(domain) VALUES('xfinity.com');
INSERT INTO mboxdomains(domain) VALUES('xs4all.net');
INSERT INTO mboxdomains(domain) VALUES('xs4all.nl');
INSERT INTO mboxdomains(domain) VALUES('yahoo.ca');
INSERT INTO mboxdomains(domain) VALUES('yahoo.co.id');
INSERT INTO mboxdomains(domain) VALUES('yahoo.co.in');
INSERT INTO mboxdomains(domain) VALUES('yahoo.co.jp');
INSERT INTO mboxdomains(domain) VALUES('yahoo.co.uk');
INSERT INTO mboxdomains(domain) VALUES('yahoo.com.ar');
INSERT INTO mboxdomains(domain) VALUES('yahoo.com.au');
INSERT INTO mboxdomains(domain) VALUES('yahoo.com.br');
INSERT INTO mboxdomains(domain) VALUES('yahoo.com.mx');
INSERT INTO mboxdomains(domain) VALUES('yahoo.com.sg');
INSERT INTO mboxdomains(domain) VALUES('yahoo.de');
INSERT INTO mboxdomains(domain) VALUES('yahoo.es');
INSERT INTO mboxdomains(domain) VALUES('yahoo.fr');
INSERT INTO mboxdomains(domain) VALUES('yahoo.in');
INSERT INTO mboxdomains(domain) VALUES('yahoo.it');
INSERT INTO mboxdomains(domain) VALUES('zoho.com');
INSERT INTO mboxdomains(domain) VALUES('zonnet.nl');
