<?php
/*
  (c) 2019 Michael A. Peters except where otherwise noted.
  MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE
*/

// set this to the full path of wherever you put the settings.inc.php file
//  if it is put in different directory than this script.
$settingsFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.inc.php';

// Nothing below needs modification unless you don't like my logic.

if(! file_exists($settingsFile)) {
  header("HTTP/1.1 500 Internal Server Error");
  print ('Could not locate settings file.' . "\n");
  exit(1);
}
require($settingsFile);

if(! isset($pdo)) {
  header("HTTP/1.1 500 Internal Server Error");
  exit(1);
}
if(! checkDatabaseTableVersion($pdo, 'importEmail.php', '00000100')) {
  header("HTTP/1.1 500 Internal Server Error");
  exit(1);
}
if(isset($_POST['domains'])) {
  $json = $_POST['domains'];
  $test = json_decode($json);
  if(is_null($test)) {
    header("HTTP/1.0 400 Bad Request");
    exit;
  }
} else {
  header("HTTP/1.0 418 I'm A Teapot");
  exit;
}

function validateDomainName($input) {
  $input = trim($input);  
  if(function_exists("idn_to_ascii")) {
    $input = idn_to_ascii($input);
  }
  $input = strtolower($input);
  if($input === 'localhost') {
    return '';
  }
  if($input === 'localhost.localdomain') {
    return '';
  }
  if(filter_var($input, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) !== false) {
    return $input;
  }
  return '';
}

$inputArray = json_decode($json);
$validArray = [];

foreach($inputArray as $addy) {
  $addy = validateDomainName($addy);
  if(strlen($addy) > 0) {
    $validArray[] = $addy;
  }
}

$sql = "INSERT INTO mboxdomains(domain) VALUES(?)";
$q = $pdo->prepare($sql);
foreach($validArray as $domain) {
  $a = array($domain);
  $q->execute($a);
}

$sql = "UPDATE mboxdomains SET lasthit=? WHERE domain=?";
$q = $pdo->prepare($sql);
$now = date('Y-m-d H:i:s');
foreach($validArray as $domain) {
  $a = array($domain, $now);
  $q->execute($a);
}

$message = 'Inserted / touched ' . count($validArray) . ' domains submitted by IP address ' . $_SERVER['REMOTE_ADDR'];
logMessage($pdo, 'Notice', 'importEmail.php', $message);

header('HTTP/1.1 200 OK');
exit;
?>