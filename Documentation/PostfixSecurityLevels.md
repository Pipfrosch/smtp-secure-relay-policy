Postfix Security Levels
=======================

Postfix is the SMTP server software I am most familiar with. However I hope
that this project can be used to generate policy files for other SMTP servers
used as SMTP Relays, including Exim (high priority for me) and Sendmail.

Postfix allows the system administrator to specify a 'security level' for
outgoing messages in the `smtp_tls_security_level` directive within the
`main.cf` file.

This security level generally needs to be set rather liberally, but Postfix
allows for a policy map file to override the setting for specific domains where
a tighter (or even looser) security level is warranted.

These are the keywords (via `man 5 postconf` but summarized by me) that
directive takes:

`none`
:    No TLS is used. This directive should be avoided.

`may`
:    Opportunistic TLS. If the MX server advertises that it supports TLS, the
     client will attempt to negotiate a secure connection. The X.509
     certificate for the MX server will *not* be validated. The connection will
     continue in the clear if either the MX Server does not indicate that it
     supports TLS or if the SMTP Relay and the MX Server can not negotiate a
     secure connection. This is generally the setting that is used by default
     when Postfix is packaged by modern Linux distributions.

`encrypt`
:    Mandatory TLS. However, the X.509 certificates for the server will not be
     validated. With the exception of SMTP servers for specific industries
     where privacy is paramount, this setting should only be used in a policy
     map file to override a more liberal setting for specific domains where you
     are *sure* that TLS is supported.

`dane`
:    Opportunistic DANE TLS. If the SMTP Relay is using a DNSSEC enforcing
     local resolver, then Postfix will query for TLSA records associated with
     the MX server. If the MX server has a DNSSEC validating TLSA record, then
     Postfix will __require__ a secure connection to that server, with a X.509
     certificate that validates against the TLSA record, or the connection will
     be refused. If the MX server does *not* have a DNSSEC validating TLSA
     record, then this setting behaves the same as `may` and Opportunistic TLS
     is used. This is the setting I personally generally recommend in the
     `main.cf` file however you must have a local recursive resolver (such as
     Unbound) running in DNSSEC enforce mode, and Postfix needs to use that as
     the recursive resolver for DNS queries.

`dane-only`
:    Mandatory DANE TLS. With this setting, Postfix will only connect to an
     MX server that has a validating TLSA record, and then the connection will
     be dropped if it can not be upgraded to TLS with the server using a X.509
     certificate that validates against the TLSA record. This setting should
     only be used in a policy map file to override a more liberal setting for
     specific domains where you are *sure* the server implements DANE for SMTP.

`fingerprint`
:    Mandatory TLS with X.509 certificate validated against a fingerprint that
     is hard-coded in the configuration file. This setting can only be used in
     the context of specific MX servers where you __know__ what the expected
     fingerprint of the certificate or public key is suppose to be. In practice
     it usually only used for very high-security applications usually within
     the same organization (such as the U.S. Government, hence why Hillary
     really should not have used her own server for government business. Is the
     horse really dead? ;)

`verify`
:    Mandatory TLS verification. The hostnames from the `MX RRset` are
     implicitly trusted. As long as the X.509 certificate validates with a
     hostname that matches the hostname presented in the MX record, it is
     considered to be 'secure enough' to continue with the TLS connection
     upgrade. The connection is dropped if either X.509 certificate does not
     verify or the connection can not be upgraded to TLS for another reason.
     This setting should only be used in a policy map file to override a more
     liberal setting for specific mailbox domains where you are *sure* that a
     proper X.509 certificate will be used.

`secure`
:    Secure Channel TLS. This is like the `verify` setting, except that the
     hostnames provided by the `MX RRset` are not trusted, they __must__
     match a pattern given by an accompanied `match` directive. This setting
     should only be used in a policy map file to override a more liberal
     setting for specific mailbox domains where you are *sure* that a proper
     X.509 certificate will be used *and* you know what the MX server hostnames
     are suppose to be.

Of those modes, for an SMTP relay the following options are appropriate for
the more liberal setting within the Postfix `main.cf` file:

* `may`
* `dane`
* `encrypt`

If you can, for the general setting, make sure that you have a DNSSEC enforcing
recursive resolver running and use `dane` but if you can not for whatever
reason do that, then at least use `may`. The use of `encrypt` for your general
liberal setting should only be done for specific use cases, such as an SMTP
Relay for a Doctors Office where privacy is essential to the recipients, as it
will result in some (though small number) messages not being delivered.

Then create a policy map with stricter policies for mailbox domains where you
know stronger policy is either supported or warranted.

The purpose of this project is to help make that easy via discovery of what
mailbox domains publish about their own TLS capabilities.

As the policy map created includes `dane-only` policies, the SMTP Relay really
must have Unbound or a similar DNSSEC enforcing recursive resolver running on
the local server, so your liberal general Postfix security level should be set
to either `dane` or if necessary for your needs, `encrypt`.

Some enterprise distributions (including RHEL/CentOS 7) ship with seriously
outdated versions of Postfix that have no or poor DANE support. Please use
Postfix 3.1 or newer, I myself currently use the Postfix 3.3 release series.
Postfix 3.4 was *just* released at the end of February 2019, and it features
TLS session resumption, a very beneficial feature for busy servers. I will
likely move to that series in June or July.

For more information on Postfix configuration, see the `PostfixRelayHowto.md`
file. The security levels were described here purely to familiarize the reader
with the concept of connection policies.