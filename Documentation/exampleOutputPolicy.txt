# DANE only
aegee.org dane-only protocols=TLSv1.2 ciphers=high
aipi.de dane-only protocols=TLSv1.2 ciphers=high
alainwolf.ch dane-only protocols=TLSv1.2 ciphers=high
alainwolf.net dane-only protocols=TLSv1.2 ciphers=high
anongoth.pl dane-only protocols=TLSv1.2 ciphers=high
artemyev.com dane-only protocols=TLSv1.2 ciphers=high
artemyev.net dane-only protocols=TLSv1.2 ciphers=high
artemyev.org dane-only protocols=TLSv1.2 ciphers=high
backschues.com dane-only protocols=TLSv1.2 ciphers=high
backschues.de dane-only protocols=TLSv1.2 ciphers=high
backschues.net dane-only protocols=TLSv1.2 ciphers=high
bhosted.nl dane-only protocols=TLSv1.2 ciphers=high
billaud.eu.org dane-only protocols=TLSv1.2 ciphers=high
bugabinga.net dane-only protocols=TLSv1.2 ciphers=high
bund.de dane-only protocols=TLSv1.2 ciphers=high
burzmali.com dane-only protocols=TLSv1.2 ciphers=high
caletka.cz dane-only protocols=TLSv1.2 ciphers=high
cdom.de dane-only protocols=TLSv1.2 ciphers=high
cert.br dane-only protocols=TLSv1.2 ciphers=high
colincogle.name dane-only protocols=TLSv1.2 ciphers=high
comcast.net dane-only protocols=TLSv1.2 ciphers=high
dayman.net dane-only protocols=TLSv1.2 ciphers=high
dd24.net dane-only protocols=TLSv1.2 ciphers=high
ddimension.net dane-only protocols=TLSv1.2 ciphers=high
debian.org dane-only protocols=TLSv1.2 ciphers=high
denic.de dane-only protocols=TLSv1.2 ciphers=high
deviant.email dane-only protocols=TLSv1.2 ciphers=high
dietrich.cx dane-only protocols=TLSv1.2 ciphers=high
dismail.de dane-only protocols=TLSv1.2 ciphers=high
domblogger.net dane-only protocols=TLSv1.2 ciphers=high
domeneshop.no dane-only protocols=TLSv1.2 ciphers=high
druwe.net dane-only protocols=TLSv1.2 ciphers=high
durel.org dane-only protocols=TLSv1.2 ciphers=high
eckhofer.com dane-only protocols=TLSv1.2 ciphers=high
edam-volendam.nl dane-only protocols=TLSv1.2 ciphers=high
familieverberne.nl dane-only protocols=TLSv1.2 ciphers=high
fau.de dane-only protocols=TLSv1.2 ciphers=high
federation-interstellar.org dane-only protocols=TLSv1.2 ciphers=high
freenet.de dane-only protocols=TLSv1.2 ciphers=high
gehrke.in dane-only protocols=TLSv1.2 ciphers=high
gkd-el.de dane-only protocols=TLSv1.2 ciphers=high
gmx.at dane-only protocols=TLSv1.2 ciphers=high
gmx.com dane-only protocols=TLSv1.2 ciphers=high
gmx.de dane-only protocols=TLSv1.2 ciphers=high
gmx.net dane-only protocols=TLSv1.2 ciphers=high
grepular.com dane-only protocols=TLSv1.2 ciphers=high
hr-manager.net dane-only protocols=TLSv1.2 ciphers=high
hypermeganet.uk dane-only protocols=TLSv1.2 ciphers=high
ietf.org dane-only protocols=TLSv1.2 ciphers=high
incenp.org dane-only protocols=TLSv1.2 ciphers=high
inethost.eu dane-only protocols=TLSv1.2 ciphers=high
it-zahner.de dane-only protocols=TLSv1.2 ciphers=high
it4sure.nl dane-only protocols=TLSv1.2 ciphers=high
itzer.de dane-only protocols=TLSv1.2 ciphers=high
jonaswitmer.ch dane-only protocols=TLSv1.2 ciphers=high
kabelmail.de dane-only protocols=TLSv1.2 ciphers=high
kc1hbk.com dane-only protocols=TLSv1.2 ciphers=high
klingele.me dane-only protocols=TLSv1.2 ciphers=high
klinikum-oldenburg.de dane-only protocols=TLSv1.2 ciphers=high
knop.eu dane-only protocols=TLSv1.2 ciphers=high
kryptonresearch.com dane-only protocols=TLSv1.2 ciphers=high
kubica.ch dane-only protocols=TLSv1.2 ciphers=high
levinus.de dane-only protocols=TLSv1.2 ciphers=high
librelamp.com dane-only protocols=TLSv1.2 ciphers=high
lists.aegee.org dane-only protocols=TLSv1.2 ciphers=high
lrz.de dane-only protocols=TLSv1.2 ciphers=high
m9r1s.com dane-only protocols=TLSv1.2 ciphers=high
mach-politik.ch dane-only protocols=TLSv1.2 ciphers=high
maelstrom.ninja dane-only protocols=TLSv1.2 ciphers=high
mail.aegee.org dane-only protocols=TLSv1.2 ciphers=high
mail.com dane-only protocols=TLSv1.2 ciphers=high
mail.de dane-only protocols=TLSv1.2 ciphers=high
mailbox.org dane-only protocols=TLSv1.2 ciphers=high
mnium.de dane-only protocols=TLSv1.2 ciphers=high
mycloudmail.ooo dane-only protocols=TLSv1.2 ciphers=high
nic.br dane-only protocols=TLSv1.2 ciphers=high
nic.cz dane-only protocols=TLSv1.2 ciphers=high
nlnetlabs.nl dane-only protocols=TLSv1.2 ciphers=high
octopuce.fr dane-only protocols=TLSv1.2 ciphers=high
open.ch dane-only protocols=TLSv1.2 ciphers=high
openssl.org dane-only protocols=TLSv1.2 ciphers=high
otvi.nl dane-only protocols=TLSv1.2 ciphers=high
overheid.nl dane-only protocols=TLSv1.2 ciphers=high
pa5am.nl dane-only protocols=TLSv1.2 ciphers=high
posteo.de dane-only protocols=TLSv1.2 ciphers=high
prolocation.net dane-only protocols=TLSv1.2 ciphers=high
prolocation.nl dane-only protocols=TLSv1.2 ciphers=high
pt-server.de dane-only protocols=TLSv1.2 ciphers=high
rbbaader.de dane-only protocols=TLSv1.2 ciphers=high
registro.br dane-only protocols=TLSv1.2 ciphers=high
reisebuero-baader.de dane-only protocols=TLSv1.2 ciphers=high
rhymeswithmogul.com dane-only protocols=TLSv1.2 ciphers=high
ruhr-uni-bochum.de dane-only protocols=TLSv1.2 ciphers=high
s-hertogenbosch.nl dane-only protocols=TLSv1.2 ciphers=high
sbs.co.nz dane-only protocols=TLSv1.2 ciphers=high
sdeziel.info dane-only protocols=TLSv1.2 ciphers=high
secure.mailbox.org dane-only protocols=TLSv1.2 ciphers=high
setemares.org dane-only protocols=TLSv1.2 ciphers=high
sovereign-order-of-saint-john.org dane-only protocols=TLSv1.2 ciphers=high
spodhuis.org dane-only protocols=TLSv1.2 ciphers=high
systemli.org dane-only protocols=TLSv1.2 ciphers=high
torproject.org dane-only protocols=TLSv1.2 ciphers=high
treussart.com dane-only protocols=TLSv1.2 ciphers=high
tum.de dane-only protocols=TLSv1.2 ciphers=high
tutanota.com dane-only protocols=TLSv1.2 ciphers=high
uni-erlangen.de dane-only protocols=TLSv1.2 ciphers=high
unitybox.de dane-only protocols=TLSv1.2 ciphers=high
uvt.nl dane-only protocols=TLSv1.2 ciphers=high
vixrapedia.org dane-only protocols=TLSv1.2 ciphers=high
vm-0.com dane-only protocols=TLSv1.2 ciphers=high
vulnscan.org dane-only protocols=TLSv1.2 ciphers=high
web.de dane-only protocols=TLSv1.2 ciphers=high
webcruitermail.no dane-only protocols=TLSv1.2 ciphers=high
xfinity.com dane-only protocols=TLSv1.2 ciphers=high
xs4all.net dane-only protocols=TLSv1.2 ciphers=high
xs4all.nl dane-only protocols=TLSv1.2 ciphers=high
# well-known DANE MX providers but MX RRset Not Signed
avansplus.de dane-only protocols=TLSv1.2 ciphers=high
# MTA-STS enforce mode
bolte.org secure protocols=TLSv1.2 ciphers=high match=mail.bolte.org
c3w.at secure protocols=TLSv1.2 ciphers=high match=dergeraet.c3w.at
ck-energy.de secure protocols=TLSv1.2 ciphers=high match=alpha.mail.ck-energy.info:beta.mail.ck-energy.info
compucation.de secure protocols=TLSv1.2 ciphers=high match=mail.compucation.de
decimatechnologies.eu secure protocols=TLSv1.2 ciphers=high match=in1-smtp.messagingengine.com:in2-smtp.messagingengine.com
divegearexpress.com secure protocols=TLSv1.2 ciphers=high match=in1-smtp.messagingengine.com:in1-smtp.messagingengine.com:in2-smtp.messagingengine.com:in2-smtp.messagingengine.com:smtp.divegearexpress.com:smtp.divegearexpress.com
emilstahl.dk secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
ggp2.com secure protocols=TLSv1.2 ciphers=high match=c.fsckd.com:d.fsckd.com
goppold.net secure protocols=TLSv1.2 ciphers=high match=mail.goppold.net:mail2.goppold.net
hengroenet.de secure protocols=TLSv1.2 ciphers=high match=mail.hengroenet.de
js-webcoding.de secure protocols=TLSv1.2 ciphers=high match=mail.js-webcoding.de
ko-sys.com secure protocols=TLSv1.2 ciphers=high match=smtp.ko-sys.com
ledovskis.lv secure protocols=TLSv1.2 ciphers=high match=ariake.ledovskis.lv
maclemon.at secure protocols=TLSv1.2 ciphers=high match=dungeon.maclemon.at
mercedcapital.com secure protocols=TLSv1.2 ciphers=high match=smtp1-mke.securence.com:smtp1-msp.securence.com
randolf.at secure protocols=TLSv1.2 ciphers=high match=mail.randolf.at
semox.de secure protocols=TLSv1.2 ciphers=high match=www.semox.de
snowpaws.de secure protocols=TLSv1.2 ciphers=high match=mail.snowpaws.de
tbz-pariv.de secure protocols=TLSv1.2 ciphers=high match=mail.tbz-pariv.de:mail2.tbz-pariv.de
technikumnukleoniczne.pl secure protocols=TLSv1.2 ciphers=high match=technikumnukleoniczne-pl.mail.protection.outlook.com
# MTA-STS testing mode
artikel10.org encrypt protocols=TLSv1.2 ciphers=high
giebel.it encrypt protocols=TLSv1.2 ciphers=high
gmail.com encrypt protocols=TLSv1.2 ciphers=high
googlemail.com encrypt protocols=TLSv1.2 ciphers=high
modum.by encrypt protocols=TLSv1.2 ciphers=high
saccani.net encrypt protocols=TLSv1.2 ciphers=high
salford.gov.uk encrypt protocols=TLSv1.2 ciphers=high
thelinuxtree.net encrypt protocols=TLSv1.2 ciphers=high
tifaware.com encrypt protocols=TLSv1.2 ciphers=high
tspu.edu.ru encrypt protocols=TLSv1.2 ciphers=high
tspu.ru encrypt protocols=TLSv1.2 ciphers=high
wpd.de encrypt protocols=TLSv1.2 ciphers=high
yahoo.com encrypt protocols=TLSv1.2 ciphers=high
ybti.net encrypt protocols=TLSv1.2 ciphers=high
# STARTTLS Everywhere well-known providers
berkeley.edu secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
enchantrixempire.com secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:aspmx.l.google.com
lemon-kiwi.co secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
dombox.org secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
hmdb.org secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
netera.com secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
salesforce.com secure protocols=TLSv1.2 ciphers=high match=alt1.aspmx.l.google.com:alt2.aspmx.l.google.com:alt3.aspmx.l.google.com:alt4.aspmx.l.google.com:aspmx.l.google.com
mac.com secure protocols=TLSv1.2 ciphers=high match=mx1.mail.icloud.com:mx2.mail.icloud.com:mx3.mail.icloud.com:mx4.mail.icloud.com:mx5.mail.icloud.com:mx6.mail.icloud.com
icloud.com secure protocols=TLSv1.2 ciphers=high match=mx1.mail.icloud.com:mx2.mail.icloud.com:mx3.mail.icloud.com:mx4.mail.icloud.com:mx5.mail.icloud.com:mx6.mail.icloud.com
me.com secure protocols=TLSv1.2 ciphers=high match=mx1.mail.icloud.com:mx2.mail.icloud.com:mx3.mail.icloud.com:mx4.mail.icloud.com:mx5.mail.icloud.com:mx6.mail.icloud.com
hotmail.co.uk secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
hotmail.de secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
hotmail.es secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
hotmail.fr secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
hotmail.it secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
houghton.edu secure protocols=TLSv1.2 ciphers=high match=houghton-edu.mail.protection.outlook.com
isoc.org secure protocols=TLSv1.2 ciphers=high match=isoc-org.mail.protection.outlook.com
live.ca secure protocols=TLSv1.2 ciphers=high match=nam.olc.protection.outlook.com
live.co.uk secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
live.com.au secure protocols=TLSv1.2 ciphers=high match=apc.olc.protection.outlook.com
live.fr secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
live.it secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
live.nl secure protocols=TLSv1.2 ciphers=high match=eur.olc.protection.outlook.com
spu.edu secure protocols=TLSv1.2 ciphers=high match=spu-edu.mail.protection.outlook.com
uta.edu secure protocols=TLSv1.2 ciphers=high match=uta-edu.mail.protection.outlook.com
cpexre.com secure protocols=TLSv1.2 ciphers=high match=cpexre-com.mail.protection.outlook.com
hotmail.com secure protocols=TLSv1.2 ciphers=high match=hotmail-com.olc.protection.outlook.com
live.com secure protocols=TLSv1.2 ciphers=high match=live-com.olc.protection.outlook.com
msn.com secure protocols=TLSv1.2 ciphers=high match=msn-com.olc.protection.outlook.com
netcompany.com secure protocols=TLSv1.2 ciphers=high match=netcompany-com.mail.protection.outlook.com
saskpension.com secure protocols=TLSv1.2 ciphers=high match=saskpension-com.mail.protection.outlook.com
thenetw.org secure protocols=TLSv1.2 ciphers=high match=thenetw-org.mail.protection.outlook.com
vectro.co secure protocols=TLSv1.2 ciphers=high match=vectro-co.mail.protection.outlook.com
sky.com secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
yahoo.ca secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoo.co.uk secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
yahoo.com.ar secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoo.com.au secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoo.com.br secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoo.com.mx secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoo.de secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
yahoo.es secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
yahoo.fr secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
yahoo.it secure protocols=TLSv1.2 ciphers=high match=mx-eu.mail.am0.yahoodns.net
rocketmail.com secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
yahoogroups.com secure protocols=TLSv1.2 ciphers=high match=fo-mx-groups.mail.am0.yahoodns.net
ymail.com secure protocols=TLSv1.2 ciphers=high match=mta5.am0.yahoodns.net:mta6.am0.yahoodns.net:mta7.am0.yahoodns.net
# STARTTLS Everywhere testing mode
bitpath.de encrypt protocols=TLSv1.2 ciphers=high
honigdachse.de encrypt protocols=TLSv1.2 ciphers=high
latinist.net encrypt protocols=TLSv1.2 ciphers=high
laxu.de encrypt protocols=TLSv1.2 ciphers=high
ldwg.us encrypt protocols=TLSv1.2 ciphers=high
0xheap.org encrypt protocols=TLSv1.2 ciphers=high
4zal.net encrypt protocols=TLSv1.2 ciphers=high
accessnetwork.ro encrypt protocols=TLSv1.2 ciphers=high
acloud.one encrypt protocols=TLSv1.2 ciphers=high
airpost.pw encrypt protocols=TLSv1.2 ciphers=high
allnetwork.de encrypt protocols=TLSv1.2 ciphers=high
anthonyvadala.me encrypt protocols=TLSv1.2 ciphers=high
apbox.de encrypt protocols=TLSv1.2 ciphers=high
arkadiyt.com encrypt protocols=TLSv1.2 ciphers=high
avdagic.net encrypt protocols=TLSv1.2 ciphers=high
aztecface.email encrypt protocols=TLSv1.2 ciphers=high
babioch.de encrypt protocols=TLSv1.2 ciphers=high
bearsunderland.com encrypt protocols=TLSv1.2 ciphers=high
benjamin.computer encrypt protocols=TLSv1.2 ciphers=high
bfg-10k.ru encrypt protocols=TLSv1.2 ciphers=high
bingbong.epac.to encrypt protocols=TLSv1.2 ciphers=high
boenning.me encrypt protocols=TLSv1.2 ciphers=high
boerner.click encrypt protocols=TLSv1.2 ciphers=high
bolshaw.me.uk encrypt protocols=TLSv1.2 ciphers=high
bornwebserver.nl encrypt protocols=TLSv1.2 ciphers=high
bpce.fr encrypt protocols=TLSv1.2 ciphers=high
castlehoward.co.uk encrypt protocols=TLSv1.2 ciphers=high
centerclick.org encrypt protocols=TLSv1.2 ciphers=high
cfenns.de encrypt protocols=TLSv1.2 ciphers=high
chamberlain.net.au encrypt protocols=TLSv1.2 ciphers=high
chas.se encrypt protocols=TLSv1.2 ciphers=high
chatras.me encrypt protocols=TLSv1.2 ciphers=high
clearbus.fr encrypt protocols=TLSv1.2 ciphers=high
conchaudron.net encrypt protocols=TLSv1.2 ciphers=high
connordav.is encrypt protocols=TLSv1.2 ciphers=high
corecha.in encrypt protocols=TLSv1.2 ciphers=high
cosysco.com encrypt protocols=TLSv1.2 ciphers=high
crasline.de encrypt protocols=TLSv1.2 ciphers=high
crustytoothpaste.net encrypt protocols=TLSv1.2 ciphers=high
cryptomilk.org encrypt protocols=TLSv1.2 ciphers=high
dahoam.de encrypt protocols=TLSv1.2 ciphers=high
dancingsnails.com encrypt protocols=TLSv1.2 ciphers=high
davej.org encrypt protocols=TLSv1.2 ciphers=high
delfic.org encrypt protocols=TLSv1.2 ciphers=high
desgrange.net encrypt protocols=TLSv1.2 ciphers=high
desu.ne.jp encrypt protocols=TLSv1.2 ciphers=high
dhautefeuille.eu encrypt protocols=TLSv1.2 ciphers=high
donnerdoof.de encrypt protocols=TLSv1.2 ciphers=high
dotmol.nl encrypt protocols=TLSv1.2 ciphers=high
duesterhus.eu encrypt protocols=TLSv1.2 ciphers=high
eff.org encrypt protocols=TLSv1.2 ciphers=high
eltrich.name encrypt protocols=TLSv1.2 ciphers=high
email.ctrl.blog encrypt protocols=TLSv1.2 ciphers=high
ergobyte.gr encrypt protocols=TLSv1.2 ciphers=high
eruditium.org encrypt protocols=TLSv1.2 ciphers=high
espci.fr encrypt protocols=TLSv1.2 ciphers=high
eurocontrol.int encrypt protocols=TLSv1.2 ciphers=high
f00f.org encrypt protocols=TLSv1.2 ciphers=high
fabje.nl encrypt protocols=TLSv1.2 ciphers=high
facebook.com encrypt protocols=TLSv1.2 ciphers=high
familieblume.nl encrypt protocols=TLSv1.2 ciphers=high
fau.org encrypt protocols=TLSv1.2 ciphers=high
fayeline.nl encrypt protocols=TLSv1.2 ciphers=high
fbreest.de encrypt protocols=TLSv1.2 ciphers=high
fireserve.com encrypt protocols=TLSv1.2 ciphers=high
fireserve.net encrypt protocols=TLSv1.2 ciphers=high
fivl.it encrypt protocols=TLSv1.2 ciphers=high
fladnag.net encrypt protocols=TLSv1.2 ciphers=high
flarn.com encrypt protocols=TLSv1.2 ciphers=high
french-interface.com encrypt protocols=TLSv1.2 ciphers=high
friendlyfire.ca encrypt protocols=TLSv1.2 ciphers=high
fruweb.de encrypt protocols=TLSv1.2 ciphers=high
gaertner-im-internet.de encrypt protocols=TLSv1.2 ciphers=high
getappetite.tv encrypt protocols=TLSv1.2 ciphers=high
grupoicot.es encrypt protocols=TLSv1.2 ciphers=high
guyromm.com encrypt protocols=TLSv1.2 ciphers=high
gwalarn.eu encrypt protocols=TLSv1.2 ciphers=high
halla.lv encrypt protocols=TLSv1.2 ciphers=high
hany.sk encrypt protocols=TLSv1.2 ciphers=high
harrysmallbones.co.uk encrypt protocols=TLSv1.2 ciphers=high
hesketh.net.au encrypt protocols=TLSv1.2 ciphers=high
hewgill.net encrypt protocols=TLSv1.2 ciphers=high
iatmgu.com encrypt protocols=TLSv1.2 ciphers=high
iddya.com encrypt protocols=TLSv1.2 ciphers=high
iq.pl encrypt protocols=TLSv1.2 ciphers=high
jcea.es encrypt protocols=TLSv1.2 ciphers=high
jndata.dk encrypt protocols=TLSv1.2 ciphers=high
joelmediatv.de encrypt protocols=TLSv1.2 ciphers=high
jtg.business encrypt protocols=TLSv1.2 ciphers=high
jvb.ca encrypt protocols=TLSv1.2 ciphers=high
kimihia.org.nz encrypt protocols=TLSv1.2 ciphers=high
kjkobes.com encrypt protocols=TLSv1.2 ciphers=high
klausen.dk encrypt protocols=TLSv1.2 ciphers=high
kmm.im encrypt protocols=TLSv1.2 ciphers=high
knthost.com encrypt protocols=TLSv1.2 ciphers=high
krapace.fr encrypt protocols=TLSv1.2 ciphers=high
kwain.net encrypt protocols=TLSv1.2 ciphers=high
lancashirebeekeepers.org.uk encrypt protocols=TLSv1.2 ciphers=high
lifechurch.at encrypt protocols=TLSv1.2 ciphers=high
linecorp.com encrypt protocols=TLSv1.2 ciphers=high
linuxfirewall.no-ip.org encrypt protocols=TLSv1.2 ciphers=high
lmcm.io encrypt protocols=TLSv1.2 ciphers=high
luup.info encrypt protocols=TLSv1.2 ciphers=high
macaw.me encrypt protocols=TLSv1.2 ciphers=high
maddes.net encrypt protocols=TLSv1.2 ciphers=high
mahlendur.de encrypt protocols=TLSv1.2 ciphers=high
mattleidholm.com encrypt protocols=TLSv1.2 ciphers=high
megane.it encrypt protocols=TLSv1.2 ciphers=high
menthiere.fr encrypt protocols=TLSv1.2 ciphers=high
meproduction.org encrypt protocols=TLSv1.2 ciphers=high
mewp.pl encrypt protocols=TLSv1.2 ciphers=high
michaelpjohnson.com encrypt protocols=TLSv1.2 ciphers=high
miggy.org encrypt protocols=TLSv1.2 ciphers=high
mingjie.info encrypt protocols=TLSv1.2 ciphers=high
misguide.de encrypt protocols=TLSv1.2 ciphers=high
mnfgroup.limited encrypt protocols=TLSv1.2 ciphers=high
monopod.net encrypt protocols=TLSv1.2 ciphers=high
mpcompany.ru encrypt protocols=TLSv1.2 ciphers=high
myriapolis.net encrypt protocols=TLSv1.2 ciphers=high
naundorf.it encrypt protocols=TLSv1.2 ciphers=high
naver.com encrypt protocols=TLSv1.2 ciphers=high
navercorp.com encrypt protocols=TLSv1.2 ciphers=high
neffets.de encrypt protocols=TLSv1.2 ciphers=high
netgalaxy.ca encrypt protocols=TLSv1.2 ciphers=high
ninetailed.ninja encrypt protocols=TLSv1.2 ciphers=high
ninov.de encrypt protocols=TLSv1.2 ciphers=high
node-nine.com encrypt protocols=TLSv1.2 ciphers=high
onex.de encrypt protocols=TLSv1.2 ciphers=high
ozark.be encrypt protocols=TLSv1.2 ciphers=high
petersons.me encrypt protocols=TLSv1.2 ciphers=high
pf.id.au encrypt protocols=TLSv1.2 ciphers=high
pharmbiotest.com.ua encrypt protocols=TLSv1.2 ciphers=high
php.watch encrypt protocols=TLSv1.2 ciphers=high
pienpa.nl encrypt protocols=TLSv1.2 ciphers=high
piratenpartei.at encrypt protocols=TLSv1.2 ciphers=high
piratenpartei.de encrypt protocols=TLSv1.2 ciphers=high
ppt.com encrypt protocols=TLSv1.2 ciphers=high
privacyweek.at encrypt protocols=TLSv1.2 ciphers=high
projectografico.com encrypt protocols=TLSv1.2 ciphers=high
protonmail.com encrypt protocols=TLSv1.2 ciphers=high
qq.com encrypt protocols=TLSv1.2 ciphers=high
railduction.eu encrypt protocols=TLSv1.2 ciphers=high
rbr.info encrypt protocols=TLSv1.2 ciphers=high
robwillis.org encrypt protocols=TLSv1.2 ciphers=high
rogerkunz.ch encrypt protocols=TLSv1.2 ciphers=high
rupostel.com encrypt protocols=TLSv1.2 ciphers=high
s2net.ch encrypt protocols=TLSv1.2 ciphers=high
sandbornvoglfarms.com encrypt protocols=TLSv1.2 ciphers=high
sanker.by encrypt protocols=TLSv1.2 ciphers=high
sauerburger.io encrypt protocols=TLSv1.2 ciphers=high
scarf.org.uk encrypt protocols=TLSv1.2 ciphers=high
schlarb.eu encrypt protocols=TLSv1.2 ciphers=high
schleifenbaum.email encrypt protocols=TLSv1.2 ciphers=high
schleifenbaum.org encrypt protocols=TLSv1.2 ciphers=high
scotthsmith.com encrypt protocols=TLSv1.2 ciphers=high
seffner-schlesier.de encrypt protocols=TLSv1.2 ciphers=high
seite3.net encrypt protocols=TLSv1.2 ciphers=high
sequitur.tech encrypt protocols=TLSv1.2 ciphers=high
sertelon.fr encrypt protocols=TLSv1.2 ciphers=high
shivering-isles.com encrypt protocols=TLSv1.2 ciphers=high
sixpak.org encrypt protocols=TLSv1.2 ciphers=high
skynet-devices.com encrypt protocols=TLSv1.2 ciphers=high
sodium.gr encrypt protocols=TLSv1.2 ciphers=high
spendy.org encrypt protocols=TLSv1.2 ciphers=high
sunsetdynamics.com encrypt protocols=TLSv1.2 ciphers=high
sven.de encrypt protocols=TLSv1.2 ciphers=high
synergynh.com encrypt protocols=TLSv1.2 ciphers=high
systemausfall.org encrypt protocols=TLSv1.2 ciphers=high
t-senger.de encrypt protocols=TLSv1.2 ciphers=high
taintedbit.com encrypt protocols=TLSv1.2 ciphers=high
tala.dk encrypt protocols=TLSv1.2 ciphers=high
teichsurfer.de encrypt protocols=TLSv1.2 ciphers=high
tepas.de encrypt protocols=TLSv1.2 ciphers=high
the.sexy encrypt protocols=TLSv1.2 ciphers=high
thewiesners.org encrypt protocols=TLSv1.2 ciphers=high
thinking-bit.com encrypt protocols=TLSv1.2 ciphers=high
thinktank.co.nz encrypt protocols=TLSv1.2 ciphers=high
thisishugo.com encrypt protocols=TLSv1.2 ciphers=high
tilde.team encrypt protocols=TLSv1.2 ciphers=high
touraineverte.com encrypt protocols=TLSv1.2 ciphers=high
tresdouteux.com encrypt protocols=TLSv1.2 ciphers=high
tynan.com encrypt protocols=TLSv1.2 ciphers=high
valorizmusic.com encrypt protocols=TLSv1.2 ciphers=high
volitans-software.com encrypt protocols=TLSv1.2 ciphers=high
vosky.fr encrypt protocols=TLSv1.2 ciphers=high
w4eg.de encrypt protocols=TLSv1.2 ciphers=high
wakesecure.com encrypt protocols=TLSv1.2 ciphers=high
waldkindergarten-ausstattung.de encrypt protocols=TLSv1.2 ciphers=high
webgma.co.il encrypt protocols=TLSv1.2 ciphers=high
wiktel.com encrypt protocols=TLSv1.2 ciphers=high
wpd-group.com encrypt protocols=TLSv1.2 ciphers=high
wpd.fr encrypt protocols=TLSv1.2 ciphers=high
xel.nl encrypt protocols=TLSv1.2 ciphers=high
xylon.de encrypt protocols=TLSv1.2 ciphers=high
yandex.ru encrypt protocols=TLSv1.2 ciphers=high
yoursafe.se encrypt protocols=TLSv1.2 ciphers=high
zip-support.de encrypt protocols=TLSv1.2 ciphers=high
zl1ab.nz encrypt protocols=TLSv1.2 ciphers=high
# NON-monitored domains with known bad DANE records
1cocomo.com encrypt protocols=TLSv1.2 ciphers=high
4sales.ca encrypt protocols=TLSv1.2 ciphers=high
abcdoprava.cz encrypt protocols=TLSv1.2 ciphers=high
aprendere.se encrypt protocols=TLSv1.2 ciphers=high
aprendereskolor.se encrypt protocols=TLSv1.2 ciphers=high
art.org.tw encrypt protocols=TLSv1.2 ciphers=high
aspdammskolan.se encrypt protocols=TLSv1.2 ciphers=high
avanco.fr encrypt protocols=TLSv1.2 ciphers=high
bacrau.ro encrypt protocols=TLSv1.2 ciphers=high
balwisseling.nl encrypt protocols=TLSv1.2 ciphers=high
biafrarepublic.me encrypt protocols=TLSv1.2 ciphers=high
blauwblaatje.nl encrypt protocols=TLSv1.2 ciphers=high
bodixite.com encrypt protocols=TLSv1.2 ciphers=high
budhi.net encrypt protocols=TLSv1.2 ciphers=high
carlsbergluckyday.dk encrypt protocols=TLSv1.2 ciphers=high
casanielsen.dk encrypt protocols=TLSv1.2 ciphers=high
cbk-praxis-kompetenz.de encrypt protocols=TLSv1.2 ciphers=high
cloud-zero.cz encrypt protocols=TLSv1.2 ciphers=high
collegefrancais.ca encrypt protocols=TLSv1.2 ciphers=high
cra.org.tw encrypt protocols=TLSv1.2 ciphers=high
cutspin.com encrypt protocols=TLSv1.2 ciphers=high
danmahoney.com encrypt protocols=TLSv1.2 ciphers=high
dehtak.cz encrypt protocols=TLSv1.2 ciphers=high
diplomatic.email encrypt protocols=TLSv1.2 ciphers=high
dnsmadefree.com encrypt protocols=TLSv1.2 ciphers=high
dukkebutikken.dk encrypt protocols=TLSv1.2 ciphers=high
dutchdistortion.com encrypt protocols=TLSv1.2 ciphers=high
dutchdistortion.nl encrypt protocols=TLSv1.2 ciphers=high
dykhoeve.nl encrypt protocols=TLSv1.2 ciphers=high
e-admin.de encrypt protocols=TLSv1.2 ciphers=high
eisenfaust-clan.de encrypt protocols=TLSv1.2 ciphers=high
elafskolan.se encrypt protocols=TLSv1.2 ciphers=high
emulation.nl encrypt protocols=TLSv1.2 ciphers=high
engl-mail.de encrypt protocols=TLSv1.2 ciphers=high
engl-server.de encrypt protocols=TLSv1.2 ciphers=high
esterkreuger.nl encrypt protocols=TLSv1.2 ciphers=high
exxponent.nl encrypt protocols=TLSv1.2 ciphers=high
familie-sander.rocks encrypt protocols=TLSv1.2 ciphers=high
fewopenzberg.de encrypt protocols=TLSv1.2 ciphers=high
filsinger.fr encrypt protocols=TLSv1.2 ciphers=high
flame.org encrypt protocols=TLSv1.2 ciphers=high
flying-gizmo.dk encrypt protocols=TLSv1.2 ciphers=high
fonsecu.de encrypt protocols=TLSv1.2 ciphers=high
forlaget-amadeo.dk encrypt protocols=TLSv1.2 ciphers=high
fortknox.cz encrypt protocols=TLSv1.2 ciphers=high
foxglo.be encrypt protocols=TLSv1.2 ciphers=high
foxglobe.net encrypt protocols=TLSv1.2 ciphers=high
fscker.nl encrypt protocols=TLSv1.2 ciphers=high
fuglsang-akupunktur.dk encrypt protocols=TLSv1.2 ciphers=high
fujilink.ca encrypt protocols=TLSv1.2 ciphers=high
gc-system.cz encrypt protocols=TLSv1.2 ciphers=high
gi-design.dk encrypt protocols=TLSv1.2 ciphers=high
gietl.com encrypt protocols=TLSv1.2 ciphers=high
glazen-sieraden.nl encrypt protocols=TLSv1.2 ciphers=high
gushi.org encrypt protocols=TLSv1.2 ciphers=high
h-dent.hu encrypt protocols=TLSv1.2 ciphers=high
ham.org.tw encrypt protocols=TLSv1.2 ciphers=high
handikram.dk encrypt protocols=TLSv1.2 ciphers=high
hartbecke.net encrypt protocols=TLSv1.2 ciphers=high
hasno.info encrypt protocols=TLSv1.2 ciphers=high
i-gluc.hu encrypt protocols=TLSv1.2 ciphers=high
igalileo.cz encrypt protocols=TLSv1.2 ciphers=high
injectoare-turbocompresoare.ro encrypt protocols=TLSv1.2 ciphers=high
innoteil.com encrypt protocols=TLSv1.2 ciphers=high
ipsgroupbv.com encrypt protocols=TLSv1.2 ciphers=high
ipsholland.nl encrypt protocols=TLSv1.2 ciphers=high
ispecialista.eu encrypt protocols=TLSv1.2 ciphers=high
itconnect.ro encrypt protocols=TLSv1.2 ciphers=high
itspecialista.eu encrypt protocols=TLSv1.2 ciphers=high
jak-na-les.cz encrypt protocols=TLSv1.2 ciphers=high
jaknales.cz encrypt protocols=TLSv1.2 ciphers=high
jamkicks.fi encrypt protocols=TLSv1.2 ciphers=high
kaisers-backstube.com encrypt protocols=TLSv1.2 ciphers=high
kajanfriskola.se encrypt protocols=TLSv1.2 ciphers=high
kommle.de encrypt protocols=TLSv1.2 ciphers=high
kremer-sas.com encrypt protocols=TLSv1.2 ciphers=high
kremer-sas.fr encrypt protocols=TLSv1.2 ciphers=high
kutgeluid.nl encrypt protocols=TLSv1.2 ciphers=high
machkovi.cz encrypt protocols=TLSv1.2 ciphers=high
magaluf.dk encrypt protocols=TLSv1.2 ciphers=high
magitaskolan.se encrypt protocols=TLSv1.2 ciphers=high
maverickvalves-hq.com encrypt protocols=TLSv1.2 ciphers=high
maverickvalves.com encrypt protocols=TLSv1.2 ciphers=high
maverickvalves.eu encrypt protocols=TLSv1.2 ciphers=high
min-skola.se encrypt protocols=TLSv1.2 ciphers=high
minerva-investment.nl encrypt protocols=TLSv1.2 ciphers=high
monitoruldesuceava.ro encrypt protocols=TLSv1.2 ciphers=high
monitorulsv.ro encrypt protocols=TLSv1.2 ciphers=high
motorsportgymnasiet.se encrypt protocols=TLSv1.2 ciphers=high
musiikkikoulut.fi encrypt protocols=TLSv1.2 ciphers=high
nukanhetnog.net encrypt protocols=TLSv1.2 ciphers=high
odengymnasiet.se encrypt protocols=TLSv1.2 ciphers=high
odenplansgymnasiet.se encrypt protocols=TLSv1.2 ciphers=high
pasion.ro encrypt protocols=TLSv1.2 ciphers=high
pocitace-pribram.cz encrypt protocols=TLSv1.2 ciphers=high
pocitacepraha.cz encrypt protocols=TLSv1.2 ciphers=high
pocitacepribram.cz encrypt protocols=TLSv1.2 ciphers=high
ppfurniture.dk encrypt protocols=TLSv1.2 ciphers=high
ppmoebler.dk encrypt protocols=TLSv1.2 ciphers=high
qbitnet.com encrypt protocols=TLSv1.2 ciphers=high
quagga.rocks encrypt protocols=TLSv1.2 ciphers=high
qualydis.fr encrypt protocols=TLSv1.2 ciphers=high
randomfacepalm.nl encrypt protocols=TLSv1.2 ciphers=high
riggers-and-roustabouts.com encrypt protocols=TLSv1.2 ciphers=high
sauditelecom.com.sa encrypt protocols=TLSv1.2 ciphers=high
secmatic.com encrypt protocols=TLSv1.2 ciphers=high
secmatic.net encrypt protocols=TLSv1.2 ciphers=high
secufon.com encrypt protocols=TLSv1.2 ciphers=high
secufon.de encrypt protocols=TLSv1.2 ciphers=high
secufon.net encrypt protocols=TLSv1.2 ciphers=high
shoottothrill.dk encrypt protocols=TLSv1.2 ciphers=high
siaddk.dk encrypt protocols=TLSv1.2 ciphers=high
sigtest.com encrypt protocols=TLSv1.2 ciphers=high
skarings.se encrypt protocols=TLSv1.2 ciphers=high
skolgrunden.se encrypt protocols=TLSv1.2 ciphers=high
skovsgaardmoelleogbagerimuseum.dk encrypt protocols=TLSv1.2 ciphers=high
smia-automotive.com encrypt protocols=TLSv1.2 ciphers=high
sosmodracisme.dk encrypt protocols=TLSv1.2 ciphers=high
soundplannord.dk encrypt protocols=TLSv1.2 ciphers=high
spamaccount.nl encrypt protocols=TLSv1.2 ciphers=high
spotlightpatriot.com encrypt protocols=TLSv1.2 ciphers=high
startech-rd.io encrypt protocols=TLSv1.2 ciphers=high
stepz-dansestudie.dk encrypt protocols=TLSv1.2 ciphers=high
stt.dk encrypt protocols=TLSv1.2 ciphers=high
stuartmcintyre.dk encrypt protocols=TLSv1.2 ciphers=high
studio-kiara.cz encrypt protocols=TLSv1.2 ciphers=high
supermerkatie.nl encrypt protocols=TLSv1.2 ciphers=high
svovlbrinte.dk encrypt protocols=TLSv1.2 ciphers=high
swatsmg.de encrypt protocols=TLSv1.2 ciphers=high
syrettegroup.com encrypt protocols=TLSv1.2 ciphers=high
syrettegroup.hu encrypt protocols=TLSv1.2 ciphers=high
tehsuck.de encrypt protocols=TLSv1.2 ciphers=high
terra-nautica.dk encrypt protocols=TLSv1.2 ciphers=high
terra-navtica.dk encrypt protocols=TLSv1.2 ciphers=high
terranautica.dk encrypt protocols=TLSv1.2 ciphers=high
testgeomed.ro encrypt protocols=TLSv1.2 ciphers=high
tethys-systems.ch encrypt protocols=TLSv1.2 ciphers=high
tethys-systems.com encrypt protocols=TLSv1.2 ciphers=high
thysygeplejevikarbureau.dk encrypt protocols=TLSv1.2 ciphers=high
untethered.org encrypt protocols=TLSv1.2 ciphers=high
uteseny.eu encrypt protocols=TLSv1.2 ciphers=high
varkullen.se encrypt protocols=TLSv1.2 ciphers=high
vetteworst.nl encrypt protocols=TLSv1.2 ciphers=high
victorycity.com.hk encrypt protocols=TLSv1.2 ciphers=high
vinkeln.nu encrypt protocols=TLSv1.2 ciphers=high
virtunix.nl encrypt protocols=TLSv1.2 ciphers=high
vivje.nl encrypt protocols=TLSv1.2 ciphers=high
vtg.nu encrypt protocols=TLSv1.2 ciphers=high
xcdn.org encrypt protocols=TLSv1.2 ciphers=high
xmw.de encrypt protocols=TLSv1.2 ciphers=high
xn--buplsydst-r8a.dk encrypt protocols=TLSv1.2 ciphers=high
xn--drej-i-tr-o3a.dk encrypt protocols=TLSv1.2 ciphers=high
xn--romse-yua.dk encrypt protocols=TLSv1.2 ciphers=high
xn--toksvrdhallen-7fb.dk encrypt protocols=TLSv1.2 ciphers=high
youhacked.us encrypt protocols=TLSv1.2 ciphers=high
z0z0.me encrypt protocols=TLSv1.2 ciphers=high
