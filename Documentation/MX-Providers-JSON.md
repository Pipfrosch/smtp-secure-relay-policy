MX Providers JSON
=================

A JSON file maintained by me is frequently retrieved by the `checkDomains.py`
script. The location of the file:

https://dnssec.icu/mtastsdb/mx-providers.json

It is at that location rather than in the Gitlab repo for the project for two
reasons:

* Gitlab does not provide an `AAAA` record for the `gitlab.com` domain. For
  those on IPv6 only networks, they *may* still be able to retrieve content
  from gitlab if they have a NAT64 gateway set up, but it is better to host the
  file on a dual-stack host.
* If and when I make development branches, I do not have to keep the somewhat
  frequently modified file in sync between branches.

* [Purpose](#purpose)
* [DANE Providers](#dane-providers)
* [PKIX Providers](#pkix-providers)
* [Subject Key Identifier List](#subject-key-identifier-list)
* -- [Important Note](#important-note)

Purpose
-------

Authenticated (secure) SMTP over TLS requires that the server X509 certificate
is either authenticated using the facilities of
[DANE for SMTP](https://tools.ietf.org/html/rfc7672) using the
[DNSSEC](https://www.internetsociety.org/deploy360/dnssec/) Chain of Trust or
using the facilities of [PKIX](https://datatracker.ietf.org/wg/pkix/documents/)
using a Chain of Trust that leads back to the Certificate Authority ‘Trust
Anchors’ specifically trusted by the application that validates the X509
certificate provided by SMTP server.

Deployment of DANE for SMTP and/or use of a X509 certificate that validates
using the PKIX facilities are not enough, the MTA client (SMTP Relay) that
connects to the server has to know ahead of time that the MX server has in fact
deployed one of those mechanisms, or the MTA client will use
[Opportunistic TLS](https://en.wikipedia.org/wiki/Opportunistic_TLS) which
__sometimes__ results in an *encrypted* connection but it can not be considered
a *secure* connection because authentication does not take place, allowing for
[MITM Attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) even
when encryption is used.

Many modern MTA clients have the capability to discover when the remote MX
server offers DANE validation, but that capability is not always available. For
example, Postfix requires a default `smtp_tls_security_level` of `dane` and
that is what *most* MTA client administrators should use, but there are valid
reasons why an administrator may wish to use `encrypt` instead, so that clear
text transmission is not allowed by default. An MTA client for a medical office
or a financial institution, for example, may prefer to not allow outgoing mail
to MX servers that do not support modern TLS.

[MTA-STS](https://tools.ietf.org/html/rfc8461) provides a mechanism by which a
destination domain can advertise that the MX servers that receive mail should
be authenticated via PKIX however many MTA clients do not yet support MTA-STS
and many destination domains do not publish a MTA-STS policy even though they
could.

To help discover when authenticated SMTP over TLS should be used under
conditions where the MTA client may not be able to make that discovery itself,
I am providing a JSON file that provides *some* commercial MX service providers
that either support DANE validation for their MX servers or provide proper CA
signed X509 certificates that validate using PKIX with the ‘Trust Anchors’ that
are included in the so-called ‘Mozilla Bundle’. These are the Trust Anchors
that most UNIX-like operating systems either directly use, or base their
customized bundle off of. Most of the Trust Anchors in this bundle are also
trusted by other operating systems and browsers.

With the definitions in the JSON file, the `checkDomains.py` script can
discover which destination domains use these MX service providers so that
a policy can be defined for those domains that requires Authenticated SMTP over
TLS, even when the destination domain has not taken the effort to publish a
security policy, making it more difficult for an attacker to subvert the
security capabilities that are available.

In order to keep the lists easier to maintain, only commercial MX service
providers are included.

Additionally, to help ensure the `checkDomains.py` script is able to identify
published policies from destination domains that do publish a MTA-STS policy,
a list of Trust Anchor ‘Subject Key Identifier’ numbers is included so that the
`checkDomains.py` script can detect when the Certificate Authority bundle is
not sufficient to validate the X509 certificates used on the HTTPS web servers
used to publish MTA-STS policies.

The JSON file is retrieved about every 28 hours and is validated against a PGP
signature.

The proper fingerprint for the signing key hard-coded into the
`checkDomains.py` script.

It should be noted the `checkDomains.py` script benefits from the JSON file but
has been intentionally designed to properly operate in the event it is not able
to retrieve the file.


DANE Providers
--------------

In addition to providing proper DANE configuration for *every* MX host, to make
this list an MX service provider must:

* Support TLS 1.2 with 2048-bit minimum for RSA certificates or P-256 minimum
  for ECDSA certificates
* Support 128-bit minimum Forward Secrecy cipher suites with AEAD
(ChaCha20-Poly1305 and/or AES-GCM)
* Use a minimum of 2048-bit DH parameters for DHE key exchange


PKIX Providers
--------------

Every MX server must meet the following requirements:

* Certificates signed by CA with Trust Anchor that is trusted by the Mozilla
  root certificate bundle and uses Certificate Transparency
* Support TLS 1.2 with 2048-bit minimum for RSA certificates or P-256 minimum
  for ECDSA certificates
* Support 128-bit minimum Forward Secrecy cipher suites with AEAD
(ChaCha20-Poly1305 and/or AES-GCM)
* Use a minimum of 2048-bit DH parameters for DHE key exchange

Please note that currently this list duplicates the `policy-aliases` list that
is provided by the STARTTLS Everywhere JSON file. It is *planned* to expand
beyond them in the near future.

There are three reasons I am not just using their list:

* The STARTTLS Everywhere JSON file only maintains MX service providers that
  are commonly used by destination domains that have policies listed with the
  STARTTLS Everywhere project. My purpose is to discover secure policies for
  destination domains regardless of whether or not they are listed with that
  project.
* Most of the DANE MX providers meet the requirements. Unfortunately some of
  them have periodic issues with their DANE records, often resulting from
  updating their certificate without updating their `TLSA` record to match.
  When this happens, I want to be able to list them as PKIX validating MX
  providers so that MTA clients can fairly quickly and automatically switch to
  a secure policy that requires PKI validation, allowing communication with
  that provider to continue functioning in a validated secure fashion.
* The STARTTLS Everywhere project currently gives these providers a mode of
  `testing`. By inspecting them myself, I can confidently use a policy mode of
  `enforce` instead.


Subject Key Identifier List
---------------------------

Note this list neither installs or includes actual root certificates, it only
includes the `Subject Key Identifier` codes that can be used to test the
suitability of the CA bundle on your system.

In order for the `checkDomains.py` script to discover MTA-STS policies, it must
be able to retrieve the policy from the web server that contains the policy,
which means the CA bundle `checkDomains.py` is configured to use needs to be
kept up to date.

This list will contain an updated list of `Subject Key Identifier` codes that
can be used to warn the system administrator when their Certificate Authority
bundle is starting to get stale.

Three Trust Anchors are currently flagged as required:

* DST Root CA X3
* Certum Trusted Network CA
* DigiCert High Assurance EV Root CA

The first is a root cert from Let’s Encrypt and is used by this project to
distribute the JSON policy described here.

The second is the root cert needed to retrieve the JSON file distributed by the
STARTTLS Everywhere project.

The third is used by Github where the DANE Failed list is maintained and
retrieved from.

If the CA bundle does not include the corresponding root Trust Anchors for the
X509 certificates used by those resources, the `checkDomains.py` script will
exit.

Not yet implemented but in addition to `Subject Key Identifiers` for those
Trust Anchors (which are subject to change if the mentioned services change
what Trust Anchor their certificate leads to) up to one dozen additional Trust
Anchors will be in the list, based upon how commonly they are seen on web
servers that serve MTA-STS policies. These will not be *required* by the
`checkDomains.py` script however if they are not found within the CA root
bundle, a warning message will be logged.

The point of the warning message is not to scold the admin, the admin may have
a legitimate reason why they do not trust a certain Certificate Authority and
may have chosen to manually remove if from their bundle.

However what does sometimes happen, system administrators either neglect to
automate updates to their certificate bundle or the automation system fails,
resulting in a stale bundle that over time becomes less and less effective at
retrieving MTA-STS policies.

### Important Note

The backend for determining which twelve Certificate Authorities to are most
commonly deployed in the context of MTA-STS has not yet been developed.
