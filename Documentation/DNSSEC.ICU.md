DNSSEC.ICU
==========

DNSSEC.ICU is a zone I registered for the purpose of testing the capabilities
of the local DNS resolver, important as the security of the policy records will
depend upon proper DNSSEC validation.

The zone delegated several child zones that will have various types of
intentional errors that only a properly validating resolver will catch. This is
still being worked on.

Some of child zones will have several different types of `TLSA` records too so
that I can learn how to code checks for various different types of errors that
occur in the record itself, but the records will not correspond to actual X.509
certificates. The `TLSA` records will be for sub-domains within the zones and
will not even have corresponding `A`/`AAAA` records.

All zones will use `ECDSAP384SHA384` (Algorithm 14) with `DS` records in both
`SHA-256` (Hash Algo 2) and `SHA-384` (Hash Algo 4). For the sake of simplicity
the respective `KSK` and `ZSK` used by each of the child zones will not be
rotated, however the zone content will be regenerated daily with serial number
properly incremented as they would be in a production environment.

No TLSA records yet exist for testing in any of the zones. That will come once
I am certain the zones that need scripting tricks after signing to accomplish
is working properly with automation (the expired sig zone and the invalid sig
zone).


unsigned.dnssec.icu (should result in `NOERROR` but responses are not secured)
-----------------------------------------------------------------------------------

The zone `unsigned.dnssec.icu` will not be signed at all. This zone is
currently usable for testing. See
[http://dnsviz.net/d/unsigned.dnssec.icu/dnssec/](http://dnsviz.net/d/unsigned.dnssec.icu/dnssec/)


proper.dnssec.icu (should result in `NOERROR` and responses are properly secured)
--------------------------------------------------------------------------------------

The zone `proper.dnssec.icu` will be properly signed. This zone is currently
usable for testing. See
[http://dnsviz.net/d/proper.dnssec.icu/dnssec/](http://dnsviz.net/d/proper.dnssec.icu/dnssec/)


lacking-ds.dnssec.icu (should result in `NOERROR` but responses are not secured)
-------------------------------------------------------------------------------------

The zone `lacking-ds.dnssec.icu` will be properly signed but lack a `DS` record
in the parent zone. It should be treated as an unsigned zone even though
`RRSIG` exist because there is no Chain of Trust. This zone is currently usable
for testing. See
[http://dnsviz.net/d/lacking-ds.dnssec.icu/dnssec/](http://dnsviz.net/d/lacking-ds.dnssec.icu/dnssec/)


invalid-ds.dnssec.icu (should result in `SERVFAIL` and responses seen as bogus)
-------------------------------------------------------------------------------

The zone `invalid-ds.dnssec.icu` will be properly signed but the `KSK` will not
correspond with the `DS` record in the parent zone. It should result in
`SERVEFAIL` response when querying records from a DNSSEC validating resolver.
This zone is currently usable for testing. See
[http://dnsviz.net/d/invalid-ds.dnssec.icu/dnssec/](http://dnsviz.net/d/invalid-ds.dnssec.icu/dnssec/)


valid-dnskey-with-bad-sigs.dnssec.icu (should result in `SERVFAIL` and responses seen as bogus)
-----------------------------------------------------------------------------------------------

The zone `valid-dnskey-with-bad-sigs.dnssec.icu` will have a proper validating
`DNSKEY RRset` but the other records will be signed with a *different* `ZSK`
that is not in the `DNSKEY` RRset. It should result in `SERVEFAIL` for every
record *except* the `DNSKEY` records when querying records from a DNSSEC
validating resolver. This zone is currently usable for testing. See
[http://dnsviz.net/d/valid-dnskey-with-bad-sigs.dnssec.icu/dnssec/](http://dnsviz.net/d/valid-dnskey-with-bad-sigs.dnssec.icu/dnssec/)


expired-sigs.dnssec.icu (should result in `SERVFAIL` and responses seen as bogus)
---------------------------------------------------------------------------------

The zone `expired-sigs.dnssec.icu` will be properly signed but the signatures
will have all expired. It should result in `SERVEFAIL` for every record request
from a DNSSEC validating resolver. This zone is currently usable for testing.
See
[http://dnsviz.net/d/expired-sigs.dnssec.icu/dnssec/](http://dnsviz.net/d/expired-sigs.dnssec.icu/dnssec/)

