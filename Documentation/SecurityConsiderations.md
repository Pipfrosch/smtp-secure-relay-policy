Security Considerations
=======================

Much of what is here is already covered elsewhere. I just though that when it
comes to potential security considerations, it is prudent to have a single file
that aggregates them.

* [MariaDB Database](#mariadb-database)
* [Redis Database](#redis-database)
* [Web Server](#web-server)
* -- [`rsync` over `ssh`](https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/blob/master/Documentation/SecurityConsiderations.md#rsync-over-ssh)
* [`99-cron-retrieveNewPostfixPolicy.sh` Script](#99-cron-retrievenewpostfixpolicysh-script)
* [`settings.inc.php` Script](#settingsincphp-script)
* -- [Command Line PHP Scenario](#command-line-php-scenario)
* -- [Web Server Scenario](#web-server-scenario)
* [`checkDomains.py` Script](#checkdomainspy-script)
* [`extractFromPostfix.py` Script](#extractfrompostfixpy-script)
* [Remote Fetched Configuration Resources](#remote-fetched-configuration-resources)
* -- [DANE Fail List](#dane-fail-list)


MariaDB Database
----------------

If an attacker is able to modify the database, the generated policies are
vulnerable.

Keep the database on the same host that runs the `checkDomains.py` script and
make sure you have a firewall that blocks access to TCP Port 3306 so that
remote connections to the database simply are not possible.

Make sure files that contain the password to the MariaDB database are not world
readable.


Redis Database
--------------

When I first started using Redis, I was quite shocked to see many documents
online suggesting that a password is not necessary with Redis. In fairness
there are many that do not make that absurd claim, but there are many that do.

Set a fracking password on your Redis database. And if you find resources that
try to argue it is not really necessary, do not trust anything that resource
claims, they are not resources that care about actual application security.


Web Server
----------

If your SMTP Relay is on a different host than the MariaDB database, the
the instructions here involve running a web server both for the distribution of
policy files generated from the database and as a means by which the SMTP Relay
can submit new mailbox domains to be monitored.

The webserver MUST use proper TLS to prevent the generated policy files from
being modified in transit, as well as to prevent an attacker from knowing what
policies you are applying to what mailbox domains.

Additionally, the web servers should restrict who can connect to it my IP
address. An attacker could flood the `importEmail.php` script with bogus
domains to monitor, intefering with the ability of the `checkDomains.py`
script to properly process legitimate domains.

### `rsync` over `ssh`

An alternative to running a web server, the host with the database could use
the command-line php binary to generate the policy file and place it in a
directory where SMTP Relays can fetch it via `rsync` over `ssh`.

Likewise, rather than using POST to submit new domains to monitor, the SMTP
Relays could generate a JSON encoded list and write to a directory that then
gets pushed to the host with the database via `rsync` over `ssh`.

I will document how to do this before the 1.0 release. However, running a web
server is safe if you restrict who can connect by IP address.


`99-cron-retrieveNewPostfixPolicy.sh` Script
-----------------------------------------

Per the instructions, this script is installed within either `/etc/cron.daily`
or `/etc/cron.hourly` where it is executed with the permissions of the `root`
user.

It needs to run as `root` so it can both write to the `/etc/postfix` directory
and so it can read the rotated `/var/maillog-YYYYMMDD` file.

If the SMTP Relay is on a different host than the database is running on a
different host, then external connections are made. You may have a policy
against scripts that run as `root` making external connections.

You can run it as a non-root user, you just have to make sure the non-root user
you run it as has write permission to both the
`/etc/postfix/smtp_tls_policymap` __and__ `/etc/postfix/smtp_tls_policymap.db`
files, and make sure that non-root user has read permission to the rotated
log files.

How to do that depends upon your setup, there may be some SELinux context stuff
to take into consideration, but *generally* this would work.

Note that I have not actually tried this, this really is for the super
paranoid.

    chown root:postmaster /etc/postfix
    chmod 775 /etc/postfix
    chown root:postmaster /etc/postfix/smtp_tls_policymap
    chown root:postmaster /etc/postfix/smtp_tls_policymap.db
    chmod 664 /etc/postfix/smtp_tls_policymap
    chmod 664 /etc/postfix/smtp_tls_policymap.db
    usermod -a -G postmaster someusername

You then have to edit the script so it does not exit if run as a non-root user.
If `someuser` has a `UID` of, say, `1337` then change line 8

    if [[ $EUID -ne 0 ]]; then

to

    if [[ $EUID -ne 1337 ]]; then

With respect to read permissions on the rotated log files, there I currently at
a loss but I will look into it. I suspect on `systemd` systems it can be
configured in the `systemd` related scripts.

The `99-cron-retrieveNewPostfixPolicy.sh` script would then need to be placed
in `/home/someusername/bin/` (or `/Users/someusername/bin/` or whatever) and
called from that user’s crontab entry rather than from `/etc/cron.daily` or
`/etc/cron.daily`.

### `99-cron-retrieveNewPostfixPolicy.sh` Read Permission

Note that the cron script often has a your Redis password in it and may also
have a MariaDB password in it (for the `extractFromPostfix.py` script) so make
sure it can only be read by the user that executes it (`root` unless you
changed what user it runs as)


`settings.inc.php` Script
-------------------------

This file is only needed on the host running the MariaDB database. This file
will contain the MariaDB password. There are two different scenarios.

### Command Line PHP Scenario

If you are running the MariaDB database on the SMTP Relay, then the PHP scripts
will be installed in `/usr/local/libexec` and the `settings.inc.php` script
should only be readable by the user the `99-cron-retrieveNewPostfixPolicy.sh`
script runs as (`root` unless you changed that).

### Web Server Scenario

If you are running the MariaDB database on a different host than one or more
SMTP Relays that will be using the policies, then at least if following the
current instructions, there is a wen server that facilitates the distribution
of the policy as well as providing a simple API for the SMTP Relays to submit
new mailbox domains to monitor.

In those scenarios, the `settings.inc.php` file needs to be readable by the
system user the web server runs as (often the `apache` or `www` user).

Also, even though the web server should be secured to only allow specific IP
addresses to connect to it, it is a bad practice to have a script that contains
a password in the web root.

Each PHP script that uses the `settings.inc.php` script has the following line:

    $settingsFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.inc.php';

That line tells PHP that the settings file is in the same directory as the PHP
script.

This is paranoid but it is best practices. In the case of a web server, you
should put the `settings.inc.php` file *outside* the web root and then change
that line in the scripts that call it to define the full path to where you have
it, e.g. if your web root that contains the `importEmail.php` and `policy.php`
script is `/srv/mtastsdb/www` then you might create the directory
`/srv/mtastsdb/settings` for the settings file. In both those scripts then
change the definition of `$settingsFile` to reflect that:

    $settingsFile = '/srv/mtastsdb/settings/settings.inc.php';


`checkDomains.py` Script
------------------------

This script does not need to be run by a privileged user and therefore it
should not be run by a privileged user.

I recommend setting the Redis and MariaDB authentication in the script file
itself.

You can pass that information to the script as arguments, however if you choose
to do so, do not do it from the crontab entry itself but use a wrapper script.

The problem is that bugs have existed in various cron daemon implementations
before that allow one user to see another user’s crontab entry, so it is
generally considered a very bad practice to have passwords in a crontab entry.

Either create a wrapper script with the arguments and call the wrapper script
from the crontab, or just set the MariaDB and Redis authentication information
in the script itself.


`extractFromPostfix.py` Script
------------------------------

This script has to run as a user with permission to read permissions to read
the log files generated by Postfix. In the `Documentation/PostfixRelayHowto.md`
file, the instructions result in this script being run as the `root` user.

This script does need the Redis authentication information to operate, and if
running on the same host as the MariaDB database where it directly inserts new
domains to monitor, it will require the MariaDB authentication information.

It is recommended to add that information with switches passed to the script
from the `99-cron-retrieveNewPostfixPolicy.sh` script but you can also set that
information directly in the `extractFromPostfix.py` if you prefer. If you do so
make sure the script is only readable by the system user that executes it.


Remote Fetched Configuration Resources
--------------------------------------

The `checkDomains.py` script retrieves several remote sources that are used to
create policies.

Most DANE policies are retrieved from information queried from a DNSSEC
enforcing local recursive resolver.

The `checkDomains.py` script has specific checks in place to ensure these DNS
queries do come from a local resolver that enforces DNSSEC validation. How
secure the DNSSEC resolution actually is depends upon on your local recursive
resolver, it is possible for them to be configured to trust DNSSEC trust
anchors external to the ICANN DNS root trust anchor. Do not do that. Now that
all top level domains are signed with keys that validate using a Chain of Trust
that leads back to the trust anchor in the ICANN root, there is absolutely no
need to trust any DNSSEC anchors outside of the ICANN root and it is dangerous
to do so. Any zone that uses a different Chain of Trust should be treated as an
unsigned zone. I am not aware of any zones that still use an alternate chain of
trust as there no longer is a need.

MTA-STS policies are retrieves for HTTPS servers. The retrieval of those
policies does require use a X509 certificate that has a Chain of Trust leading
to a PKIX Trust Anchor your system explicitly trusts, as described in the
`Documentation/PKIX-ChainOfTrust.md` file.

Two remote JSON files have heavy impact on policy generation. In addition to
using HTTPS with validation of PKIX Chain of Trust, both of these JSON files
are validated against PGP signatures with the fingerprint for the needed public
keys hard-coded into the `checkDomains.py` script.

### DANE Fail List

The DANE Fail List is retrieved from github. PKIX validation is used to make
sure it is not modified in transit, but there is no PGP signature to make sure
the file has not been maliciously modified at the source.

A modification to the DANE fail list could result in disabling DANE security
policies.

If the lack of PGP validation makes you feel uncomfortable, edit the
`checkDomains.py` script and comment out the line that reads

    getDaneFailList()

It is near the very bottom of the script. You can still manually add
problematic domains to the `improperdane` database table to add domains that
should not use DANE validation even though they have DNSSEC validating `TLSA`
records, but that table will no longer have entries automatically added.

    











