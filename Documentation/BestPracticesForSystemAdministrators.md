Best Practices For System Administrators
========================================

This is an *opinion* piece.

If you have a domain that receives e-mail, there are some very important steps
that you should take to make delivery of e-mail messages to your users as safe
as possible.

Even if you outsource your MX services as many do, please read this so that any
issues can be brought up with your service provider for them to fix.

* [Analyze Your Domain At Hardenize](#analyze-your-domain-at-hardenize)
* -- [Supported Protocols](#supported-protocols)
* -- [Cipher Suite Preference](#cipher-suite-preference)
* -- [Strong Key Exchange](#strong-key-exchange)
* -- [Forward Secrecy](#forward-secrecy)
* -- [X.509 Certificate](#x509-certificate)
* [TLS-RPT (SMTP TLS Reporting)](#tls-rpt-smtp-tls-reporting)
* [DANE](#dane)
* [MTA-STS](#mta-sts)
* [STARTTLS Everywhere](#starttls-everywhere)
* [Final Thoughts](#final-thoughts)


Analyze Your Domain At Hardenize
--------------------------------

An excellent service for determining the current security of your mail services
is provided for free by [Hardenize](https://www.hardenize.com/). Simply enter
your mailbox domain (the part after the `@` in e-mail addresses) and let it do
its thing. You may need an account (free, at least it was when I signed up) but
I think they let you do some scans without one.

Hardenize will give a good summary, including for your mail service, of what
you are doing well and what can be improved.

### Supported Protocols

Your server __must__ support TLS 1.2. Your server __must not__ support SSL 2/3
as those protocols are completely broken.

Hardenize will give you an analysis note if you still support TLS 1.0 and TLS
1.1 but it is usually safe to continue supporting those versions of TLS, and
some SMTP Relays that connect to your server will not be capable of TLS 1.2 and
will use clear channel instead of TLS if you do not still support TLS 1.0 and
TLS 1.1.

Unfortunately at this time, MX servers are still __required__ to accept clear
channel connection to be standards compliant, so you can not simply refuse
clear channel connections. Well, you can, but then you are not complying with
the standard.

### Cipher Suite Preference

Your server should be configured to enforce the cipher suite order it prefers,
and cipher suites that provide Forward Secrecy should be listed first. Ciphers
that use AEAD should be the very first (ChaCha20-poly1305 and AES-GCM).

### Strong Key Exchange

Your server should be configured to use DH Parameters of at least 2048-bit when
DHE key exchange is used. Many in the industry actually now recommend at least
3072-bit DH parameters, but 2048-bit is not likely to be broken anytime soon.

### Forward Secrecy

With Forward Secrecy, an ephemeral secret is negotiated between the server and
client. The X.509 (TLS/SSL) certificate is only used for authentication. This
is the most secure form of TLS communication, it means if an attacker manages
to steal your private key, it is useless to decrypt previously logged encrypted
sessions the attacker sniffed over the network. And yes, attackers do that.

### X.509 Certificate

You should use either a 3072-bit or 4096-bit RSA certificate. Technically a
2048-bit RSA certificate is still safe but it does not provide the 128-bits
of effective security that most security experts now recommend.

__DO NOT USE A SELF-SIGNED CERTIFICATE__-- these use to be the standard way
that SMTP servers were secured, but they will not work with MTA-STS and are
easy for an attacker to MITM.

If you are 100% sure your SMTP server supports OCSP stapling, it is okay to
get a certificate that *requires* OCSP stapling to be valid, however some
very high quality SMTP servers do not support OCSP stapling, those certificates
will not work with those servers. Postfix is an example of a high quality
SMTP server that does __NOT__ support OCSP stapling.

Make sure the certificate uses a SHA-2 family signature hash (such as SHA-256),
SHA-1 is broken. At this point, Certificate Authorities will no longer sign a
CSR request with SHA-1.

### TLS-RPT (SMTP TLS Reporting)

This is a DNS record (defined in
[RFC 8460](https://tools.ietf.org/html/rfc8460)) that defines a standard
mechanism by which a connecting SMTP Relay can report a TLS error when
attempting to connect to your MX servers.

If you outsource your MX services, your provider very well may have an e-mail
address or report URI already set up for this, but the DNS record has to be
in YOUR zone file (the zone file with your MX records that point to your
provider’s MX hosts).

### DANE

The zone for the MX hosts (which may be different than the zone for your
mailbox domain) really should be DNSSEC signed and should have valid TLSA
records for all the MX hosts.

How to do this is beyond the scope of this document, but I will say that if
deploying DNSSEC on a new zone, use either P-256 (signature algorithm 13)
or P-384 (signature algorithm 14). RSA algorithms result in very bloated
signature sizes, and EdDSA is not well enough supported yet.

Before deploying DANE yourself, please read
[Common DANE Mistakes](https://dane.sys4.de/common_mistakes).

If you outsource your MX services, I would like to recommend
[Prolocation](https://prolocation.net/). They provide very high quality secure
services, and they provide DANE validation for their MX servers.

They are located outside of the United States and I do not know if they service
customers in the United States, but if you are in the United States you can ask
them. Honestly their service looks to be better than services provided by any
of the American MX providers I am currently aware of.

In addition to DANE, one of the things they do right that many providers do
not, they provide MX servers that are under two different top level domains
(both `.net` and `.nl`). This provides for very high availability in the event
of a major network problem that causes one of those top level domains to no
longer resolve for a period. Secure high-availability seems to be a specialty
of theirs.

In addition to DANE support for your MX services, you also should DNSSEC sign
the zone that has your MX records if at all possible. This reduces the attack
surface when an attacker sends an SMTP Relay fraudulent DNS results causing
those servers to send mail intended for your servers to the attacker.

### MTA-STS

You should set up an MTA-STS policy for your mailbox domain. Specific
instructions are beyond the scope of this document, but it is not very
difficult to do.

Some MX hosting providers provide a generic policy you can point to by creating
the appropriate `CNAME` record. Please do not do that, `CNAME` records have
always been a very bad solution. In my *opinion* that clearly many disagree
with.

One of the issues with using a `CNAME` record, you are required to change the
serial number in the DNS `TXT` record component of MTA-STS but when you use a
`CNAME` to point to a policy provided by your provider instead of hosting it
yourself, you have no way of easily knowing when the serial number needs to be
updated.

There also is a security concern with using a `CNAME` record, an attacker can
alter where the record points causing a fraudulent policy to be read.

Remember to update your policy whenever you change your MX records, and also
remember to update the serial number in your `TXT` record that alerts others
that you *have* a MTA-STS policy.

### STARTTLS Everywhere

You should also submit your mailbox domain to the
[STARTTLS Everywhere](https://starttls-everywhere.org/) project. Many SMTP
Relays get their security policies from that project rather than from MTA-STS.

### Final Thoughts

DANE is the most effective way to allow SMTP Relays to securely connect to
your MX servers. Other than keeping your TLSA records up to date, it is all
automatic for a lot of SMTP Relays.

For many SMTP Relays, however, they either are not capable of DANE validation
or they choose not to.

Having both an MTA-STS and STARTTLS Everywhere policy increases the odds that
those SMTP Relays will be able to detect that your mailbox domain has a secure
policy they should enforce.
