Setting up Redis
================

Redis is a simple `key => value` store that caches the store in system memory
but also caches it to file so it survives a restart of the Redis server or even
a reboot of the operating system. It is very fast.

Both of the current Python scripts make use of a Redis database and I suspect
most if not all future Python scripts used in this project will also make use
of a Redis database, it is an excellent way to cache information that may be of
value during a subsequent execution of the script without needing to write the
information to a file and without needing the complexity of a structured query
language database.

Redis Configuration
-------------------

After installing Redis, first you will need to edit the configuration file to
set a password.

Many online guides to Redis give very bad advice, insisting that a password
really is not needed for Redis. That is very bad advice. There may be some
use cases where that is the case, but this is not one of them. Set a password
on the Redis database.

On Red Hat / Fedora systems the configuration file is located at
`/etc/redis.conf`. Change the line

    #requirepass foobared

to something like

    requirepass mySecretPass


Configure the server to start on system boot and start it. On Linux systems
that use `systemd` this is probably accomplished with the following commands:

    systemctl enable redis.service
    systemctl start redis.service


Redis Python Module
-------------------

You also will need to install the Redis python module.

On RHEL / CentOS 7 systems, the package is called `python2-redis`. For Fedora
systems (and I suspect RHEL/CentOS 8) that use Python 3 as the default Python
interpreter, I *believe* the package is called `python3-redis`.

Anyway, it needs to be installed so Python can communicate with the Redis data
store.

