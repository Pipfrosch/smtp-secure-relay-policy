MariaDB Database Setup and Automated Updating Howto
===================================================

This project uses a MariaDB SQL database server to keep track of mailbox
domains it should monitor, and security policies that are discovered regarding
those mailbox domains. The project is tested with MariaDB 10.2.x but I
*believe* it also will work with the MariaDB 5.5.60 that ships with RHEL /
CentOS 7. However one of the tables does use two `TIMESTAMP` columns with a
default of `CURRENT_TIMESTAMP`. That *may* cause problems with MariaDB 5.5.x,
there was some issue with older versions of MariaDB and multiple `TIMESTAMP`
columns in the same table but I *think* that issue was only if they both
wanted to automatically update on any other change to the row, I just do not
remember, been a LONG time since I actually used MariaDB 5.5.x.

This project uses a local DNSSEC enforcing recursive resolver to discover
what mailbox domains have a DNSSEC signed `MX RRset` with at least one MX host
that has a DNSSEC signed `TLSA RRset` so those domains can have a policy that
specifies DANE validation must be used.

This project uses a Python script to do the dirty work of discovering policies
about the mailbox domains in the database. The Python script is written to work
with both Python 2.7.5 and Python 3.4.

This project uses PHP to both provide an API by which new domains can be added
for monitoring and by which a Postfix policy map file can be generated. The PHP
scripts are tested in PHP 7.3.x but I believe they will also work with the PHP
5.4.16 that ships with RHEL / CentOS 7.6.

If you are running the MariaDB database on the same server as the SMTP Relay
and you do not need to make the generated policy available to other SMTP Relays
then it is probably fairly trivial to do the PHP stuff in Python instead, but
if you want the generated policy available to SMTP Relays on hosts, then PHP
really is the easiest way to do what needs to be done.

* [Step One: Install and Configure a Local DNSSEC Enforcing Recursive Resolver](#step-one-install-and-configure-a-local-dnssec-enforcing-recursive-resolver)
* [Step Two: Install MariaDB and Load the Database](#step-two-install-mariadb-and-load-the-database)
* -- [Secure MariaDB](#secure-mariadb)
* -- [UTC Timezone](#utc-timezone)
* -- [Create The Database and User](#create-the-database-and-user)
* -- [Load the `setup.sql` File](#load-the-setupsql-file)
* [Step Three: Install the Redis Database and Python Module](#step-three-install-the-redis-database-and-python-module)
* [Step Four: Modify and Install the `checkDomains.py` Python Script](#step-four-modify-and-install-the-checkdomainspy-python-script)
* -- [Database Authentication](#database-authentication)
* -- [Redis Authentication](#redis-authentication)
* -- [PKI Certificate Bundle](#pki-certificate-bundle)
* -- [Log Messages E-Mail Settings](#log-messages-e-mail-settings)
* -- [GnuPG Settings](#gnupg-settings)
* [Step Five: Install and Automate The Script](#step-five-install-and-automate-the-script)
* [Step Six: Optional Web Server Setup](#step-six-optional-web-server-setup)
* -- [Secure Virtual Host](#secure-virtual-host)
* -- [PHP with PDO and MySQL](#php-with-pdo-and-mysql)
* -- -- [`settings.inc.php` PHP Script](#settingsincphp-php-script)
* -- -- [`settings.inc.php` Security](#settingsincphp-security)
* -- -- [`importEmail.php` PHP Script](#importemailphp-php-script)
* -- -- [`policymap.php` PHP Script](#policymapphp-php-script)
* -- -- [MTA-STS Testing Mode](#mta-sts-testing-mode)
* -- -- [STARTTLS Everywhere Testing Mode](#starttls-everywhere-testing-mode)
* -- -- [DANE Fail List](#dane-fail-list)
* -- -- [Bad DANE Policy Mode](#bad-dane-policy-mode)


Step One: Install and Configure a Local DNSSEC Enforcing Recursive Resolver
---------------------------------------------------------------------------

A local Recursive Resolver that enforces DNSSEC is __critical__ to the
discovery of secure policies.

I personally recommend [Unbound](https://nlnetlabs.nl/projects/unbound/about/),
it is very high quality and well maintained and is already packaged for the
majority of UNIX-like operating systems in the vendor package repository.

Instructions for setting up Unbound are in the `UnboundHowto.md` file.

You can use a different DNSSEC enforcing recursive resolver, there is not a
technical reason why it must be Unbound, that is just the one I prefer.


Step Two: Install MariaDB and Load the Database
-----------------------------------------------

MariaDB is packaged for most UNIX-like operating systems in the vendor package
system.

Many vendors split the server and client into two different packages, make sure
you have both installed. Make sure the server is configured to start at system
boot and start it. On Linux systems that use `systemd` this is likely
accomplished with the following commands (as `root` or via `sudo`):

    systemctl enable mariadb.service
    systemctl start mariadb.service

### Secure MariaDB

Specifically make sure you run the command `mysql_secure_installation` but it
is a good idea to read over the tips Digital Ocean has graciously made
available at
[How To Secure MySQL and MariaDB Database in a Linux VPS](https://www.digitalocean.com/community/tutorials/how-to-secure-mysql-and-mariadb-databases-in-a-linux-vps)

It is also a very good idea to have your firewall block TCP port 3306 unless
you *know* that you want other hosts to be able to connect directly to your
MariaDB database. On many Linux distributions that port is blocked by the
default firewall configuration, but you have to actually be running the default
firewall.

### UTC Timezone

This project will work if you do not follow the instructions in this section.
I still recommend doing this out of general principle.

It really is best to configure MariaDB to use UTC by default. Internally,
MariaDB uses seconds since epoch for its `TIMESTAMP` column but when queried,
it will convert it into `YYYY-MM-DD HH:MM:SS` usually using the timezone the
operating system is set to use, but there is no timezone indicator in the
result of the query.

The `DATETIME` column on the other hand does not use seconds from epoch. You
can literally query a value from a `TIMESTAMP` column and insert it the result
into a `DATETIME` column, change the default system timezone, restart the
database, and they will be different values because `DATETIME` does not have
any timezone information associated with it.

`TIMESTAMP` in only 32-bit even on 64-bit systems, so it has a
[Y2038](https://en.wikipedia.org/wiki/Year_2038_problem) problem which makes it
not appropriate for many types of uses, including things like certificate
expiration dates (there are already root Trust Anchors that expire after
`2038-01-29 03:14:07 UTC`) so it should only be used for fields where you want
MariaDB to automatically insert the current time on either `INSERT` or
`UPDATE`. For other dates that could be 20 years in the future, `DATETIME` is
the better design choice but it does not include a timezone identifier.

Keep things simple. Take into consideration that `TIMESTAMP` has a flaw in that
only stores values in non-negative 32-bit integer seconds from UNIX epoch. It
is therefore not suitable for anything prior to `1970-01-01 00:00:00 UTC` or
after `2038-01-29 03:14:07 UTC`. It should only be used when the intended use
is to record the immediate date and time. Any other date and time columns
__MUST__ use `DATETIME` or your code will fail if a date and time outside that
range is ever inserted.

Take into consideration that `DATETIME` columns are effectively a string
description of the date and time with a flaw that timezone information can not
be stored in the same column, so only use UTC when inserting a date and time
into a `DATETIME` column.

Set your database to use UTC by default, always set your scripts that connect
to your database to use UTC by default, do everything UTC by default *except*
when you need to *display* the date and time in another timezone for the
benefit of a user. Do the conversion to the proper timezone and include the
timezone identifier (or offset from UTC) with the date and time displayed.

To set MariaDB to use UTC by default, edit `/etc/my.cnf.d/mariadb-server.cnf`
(or equivalent if your system uses a different file for server configuration)
and find the place that reads:

    # this is read by the standalone daemon and embedded servers
    [server]

Add `default-time-zone=+00:00` to that section so it reads:

    # this is read by the standalone daemon and embedded servers
    [server]
    default-time-zone=+00:00

Restart the MariaDB daemon. On Linux systems with SystemD:

    systemctl restart mariadb.service

Your MariaDB database will now use UTC by default when displaying the output of
a query to a `TIMESTAMP` field.

### Create The Database and User

Log in to your now-secured MariaDB user as the administrative user:

    mysql -u adminuser -p

When successfully logged in:

    MariaDB [(none)]> CREATE DATABASE mboxdomains;
    Query OK, 1 row affected (0.00 sec)
    
    MariaDB [(none)]> GRANT ALL PRIVILEGES ON mboxdomains.* TO 'someuser'@'localhost' IDENTIFIED BY 'somepassword';
    Query OK, 0 rows affected (0.42 sec)
    
    MariaDB [(none)]> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.42 sec)
    
    MariaDB [(none)]> exit;
    Bye

Replace `someuser` with the username for the database you want to use. I find
it easiest to use the username of the operating system user account that will
be running the Python script that makes use of the database, but you can use a
different username if you prefer. Replace `somepassword` with a secure
password. You will need to know both the username and password for scripts that
talk to the database.

The `FLUSH PRIVILEGES` command does not seem to be necessary on modern versions
of MariaDB but it does not hurt, and I remember in the old days of MySQL 3.x
that forgetting to run that command caused frustrations as the user could not
authenticate until it had been run. I do not know what version of MariaDB it
became no longer necessary, or if there are still situations where it is, but
since it does not hurt I just always do it when I remember to.

### Load the `setup.sql` File

To load the `setup.sql` database file:

    mysql -u dbuser -p < setup.sql

If your operating system user account is the same as the user account you gave
access to the database for, you do not need the `-u dbuser` part of that
command.

It will ask you for the password, enter it correctly and the database file will
load.

The `setup.sql` file does add a bunch of domains that will be monitored from
the start, but even more will be added by the STARTTLS Everywhere project
JSON file and by your SMTP Relay. You can manually add additional domains but
there is little need to, those that your SMTP Relays communicate with will get
added automagically.


Step Three: Install the Redis Database and Python Module
--------------------------------------------------------

Instructions for configuring Redis are in the
`Documentation/RedisSetupHowto.md` file.


Step Four: Modify and Install the `checkDomains.py` Python Script
-----------------------------------------------------------------

The Python script will need a few modifications to work in your environment.
At a minimum, it will need to the correct username and password to use with the
MariaDB database and the password for the Redis database.

Note that the `checkDomains.py` Python script __does not need__ to be run as
the `root` user and therefore it __should not__ be run as the `root` user.


### Database Authentication

Near the top change the two lines that read:

    dbuser = None
    dbpass = None

to

    dbuser = 'someuser'
    dbpass = 'somepassword'

Change `dbuser` to your MariaDB database username and change `dbpass` to the
corresponding password. You can also define them as arguments when starting the
script using the `--dbuser=someuser [ -u someuser ]` and `--dbpass=somepassword
[ -p somepassword ]` switches however since the script will normally be called
from a user `crontab` entry and it not considered secure to put password into
a crontab entry, I recommend just defining them in the script itself.

### Redis Authentication

Also near the top of the script is where the Redis password is defined. Change
the line that reads

    redispass = None

to

    redispass = 'mySecretPass'

where `mySecretPass` is the password you set for the Redis database.

Again, you can also define it at run time with the `--redis-pass=mySecretPass
[ -r mySecretPass ]` switch but again, I recommend just defining it in the
script itself.

### PKI Certificate Bundle

The Python script uses `pycurl` for the secure retrieval of remote resources,
such as the STARTTLS Everywhere JSON policy database and the MTA-STS policy
files for mailbox domains that implement MTA-STS.

You will need to specify the proper path to a certificate bundle with root
Trust Anchors in PEM format for the commonly used Certificate Authorities.

For detailed information on Certificate Authority Trust Anchors, see the
`Documentation/PKIX-ChainOfTrust.md` documentation file.

Regardless of which CA bundle you choose to use, find the line that reads:

    cabundle = '/etc/pki/tls/certs/mozilla-cacert.pem'

and modify it to point to your chosen bundle. You can also define the bundle to
use runtime with the `--pem=/path/to/your/bundle.pem` switch if you prefer.

The script will try some common locations if it can not find the bundle
specified by that setting, but it is better to specify that setting to the
bundle you specifically prefer to use.

### Log Messages E-Mail Settings

On Sundays, the `checkDomains.py` will attempt to e-mail any log messages
dating back to the previous Sunday to a system administrator.

If you do not want this happen, change

    sendMessageLog = True

to

    sendMessageLog = False

By default, it will attempt to send it to the mail account on the `localhost`
associated with the account running the script. If you want to specify an
e-mail address to send it to, change

    emailLogToAddress = None

to

    emailLogToAddress = 'whatever@wherever.tld'

By default, it will use the user account that runs the script at the
`localhost` as the From address. If you would rather specify an e-mail address
to use, change

    emailLogFromAddress = None

to

    emailLogFromAddress = 'whoever@wherever.tld'

### GnuPG Settings

The `getSignedJSON(myDict)` function is only triggered if it has been at least
28 hours since it was last called. It used to securely retrieve the JSON
encoded databases, the PGP signatures for those databases, and verifies the
signature matches.

When it is called, by default it will end up creating a temporary directory to
use for the GnuPG home directory. I did not feel comfortable having the script
interact with an existing GnuPG keyring.

A more ideal solution is to create a permanent directory for this.

Create a directory called `/usr/local/misc/eff_gpg` and make sure the user the
script runs as, and only the user the script runs as, has write permission to
that directory. As the `root` user (or via `sudo`):

    mkdir -p /usr/local/misc/eff_gpg
    chown alice:alice /usr/local/misc/eff_gpg
    chmod 700 /usr/local/misc/eff_gpg

Obviously change `alice` to whatever username will be running the
`checkDomains.py` script.

If you create a different directory for the purpose, then edit the
`checkDomains.py` and change the line that reads:

    dirforgnupg = '/usr/local/misc/eff_gpg'

Change it to point to the directory you do want to use.

If your GnuPG binary is not installed at `/usr/bin/gpg2` then you also need to
change

    gnupgbin = '/usr/bin/gpg2'

to point to your GnuPG binary.


Step Five: Install and Automate The Script
------------------------------------------

The script contains database and password information and does not need to be
run as the root user, so I recommend installing it within the `~/bin` directory
of the user it will run as:

    [ ! -d ~/bin ] && mkdir ~/bin
    mv checkDomains.py ~/bin/
    chmod 750 ~/bin/checkDomains.py

Then, execute the script to make sure it works with your version of Python:

    /usr/bin/python ~/bin/checkDomains.py

It is *possible* there will be errors due to missing Python packages. Install
what is missing. I tried really hard to stick with Python modules that are
standard in just about every UNIX-like distribution so that they are likely to
be available from your vendor package distribution, `pip` should not be needed.

On a fairly fresh CentOS 7.6 system I did not have a lot of Python packages
installed on, I had to install the following packages:

1. `mysql-connector-python`
2. `python-dns`
3. `python2-gnupg`
4. `python2-cryptography`


Assuming the script successfully runs, automate it in your `crontab`. The
following `crontab` entry will work:

    00 * * * * /home/alice/bin/checkDomains.py -s

The `00` can be anything between `00` and `59`, whatever floats your boat. You
could look at your clock and just pick it so it triggers in five minutes. The
point is to run it once an hour, every hour. Which minute during the hour it
actually triggers does not really matter. The `-s` puts it in silent mode so
that there is only output in the event of an error (cron daemon will try to
e-mail you output, so you only want it if there is an error).

Make sure to change the `alice` above to your actual account name.

If you are not familiar with how to use the `crontab` for a non-root user, see
[How to Set up a Crontab File on Linux](https://www.wikihow.com/Set-up-a-Crontab-File-on-Linux)

Every time it runs, it counts how many mailbox domains it is to keeping track
of and will process up to one sixth of that number plus a few. However it only
processes mailbox domains it has not processed within the last twelve hours.

Nutshell, each domain has DANE policy sniffed twice a day, and if there is not
DANE support detected it will also sniff for MTA-STS policy. This frequency
helps it discover policy changes rather quickly.

About every twenty-eight hours it will trigger to attempt to retrieve a new
JSON policy databases. Every twenty-eight hours was chosen as the trigger time
to stagger the requests throughout the day if lots of people start using this
project.

When it does trigger to grab a new STARTTLS Everywhere policy, it will sleep
for a random number of minutes up to fifteen minutes before fetching the
policy to further stagger the requests to the servers hosting the JSON
policies.


Step Six: Optional Web Server Setup
-----------------------------------

If you are running the MariaDB database on the same server that will be using
the generated policy *and* you do not need to share the generated policy with
any other SMTP Relays then there is absolutely no need to mess with a web
server.

However if any other hosts need access to the generated policy, the easiest way
to accomplish this is with a web server.

The project currently has two PHP scripts that are of benefit to SMTP Relays on
other hosts that will be using the generated policy file.

One script provides a simple API that allows these SMTP Relays to notify the
database of mailbox domains it is in communication with so that secure policies
for those domains can potentially be discovered.

The other script generates a Postfix policy map based upon the data within the
database.

Hopefully in the future a third script will be available to generate policy
rules for Exim.

### Secure Virtual Host

When run in a web server, these PHP scripts should be run from a secure virtual
host. I recommend setting up a sub-domain and virtual host specifically for
them, and the virtual host should be configured to *only* listen on Port 443.

There is no point in Port 80 redirects to Port 443, these are not public
services, these services will exist only for scripts on mail servers under your
administration to communicate with.

Configure the document root for the virtual host to refuse all connections
except from the specific IP addresses assosiated with the SMTP Relays that will
be using the resource.

In Apache 2.4 you can do this with the `Require ip` directive:

    <Directory /path/to/some/directory/>
        Require ip 45.79.159.147 [2600:3c03::f03c:91ff:fe55:db96]
    </Directory>

If the PHP scripts are in `/path/to/some/directory/` then any requests to them
will be denied by the web server unless they come from one of the IP addresses
you have in the `Require ip` directive.

I do recommend both the IPv4 and IPv6 address for each SMTP Relay that you want
to use these resources, servers usually prefer to communicate over IPv6 when
they can but sometimes configurations cause them to use IPv4 even when IPv6 is
available.

In the event you have been living in a cave, you can get free TLS certificates
from the [Let’s Encrypt](https://letsencrypt.org/) Project, another fine
initiative of the [Electronic Frontier Foundation](https://www.eff.org/).

### PHP with PDO and MySQL

These scripts require PHP with the PDO and MySQL extensions. The scripts are
tested in PHP 7.3.x but *should* work in the crusty PHP 5.6.x that some
enterprise Linux distributions still ship.

Enterprise customers sure do pay a lot of money to have old software so that
the commercial software vendors they pay a lot of money to do not have to
update their code.

### `settings.inc.php` PHP Script
This script contains configurable settings for all the other PHP scripts.

Many system administrators (myself included) do not like to have PHP scripts
that contain authentication information inside the web root. If you will be
installing the `settings.inc.php` script in a different directory than the
other scripts, then when installing the other scripts, in the other scripts
change `line 9` from:

    $settingsFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.inc.php';

to

    $settingsFile = '/full/path/to/where/you/have/settings.inc.php';

In the `settings.inc.php` script, at a minimum you will need to update the
`$dbuser` and `$dbpass` variables to reflect your MariaDB authentication
information.

#### `settings.inc.php` Security

This file contains MariaDB authentication information. It is important to make
sure it can only be read by your admin user account that may need to modify the
setting and the web server.

If your admin user account is `alice` and the web server is running in the
group `apache` then:

    chown alice:apache /full/path/to/where/you/have/settings.inc.php
    chmod 640 /full/path/to/where/you/have/settings.inc.php

### `importEmail.php` PHP Script

This PHP script provides a simple API by which your SMTP Relays can submit the
mailbox domains they are in communication with so that they can be monitored
for security policy discovery.

Other than the database authentication, nothing in the `settings.inc.php` has
any bearing on how this script runs.

The script accepts data by POST with a `domains` field containing a JSON
encoded list of domain names. Each domain name received is validated to make
sure it is a valid domain name before it is added to the MariaDB database. It
does not verify that the domain name resolves but it shouldn’t, a temporary
name resolution would then prevent a valid domain name from being added and the
SMTP Relay may not attempt to add it again for months.

If the POST does not contain a `domains` field it responds with a `HTTP/1.0 418
I’m A Teapot` header. If the `domains` field is not valid JSON it responds
with a `HTTP/1.0 400 Bad Request` header. Upon success, it responds with a
`HTTP/1.1 200 OK` header. Pet Peeve, it should be `Okay` and not `OK` as `OK` is
not an acronym, but I did not write the HTTP response code standards.

### `policymap.php` PHP Script

This PHP script queries the MariaDB database and turns the results into a
Postfix policy map file.

In addition to the database authentication settings in the `settings.inc.php`
file, there are several other changes you *may* wish to make.

#### MTA-STS Testing Mode

According to the MTA-STS specification, when a mailbox domain has a published
MTA-STS policy but the mode is set to `testing`, SMTP Relays are suppose to
still make the connection even if the X.509 certificate does not validate. The
SMTP Relay *MAY* send a message to the SMTP TLS Error reporting mechanism, but
they are still suppose to connect.

Postfix can not be configured to send anyway if it attempts to authenticate a
certificate but the authentication fails. If Postfix is asked to validate a
certificate then the connection drops if the certificate does not validate.

To comply with the MTA-STS specification, for mailbox domains that publish a
MTA-STS policy in testing mode, a Postfix security mode of `encrypt` is used
rather than `secure`. The connection will still require TLS 1.2 or better, but
no attempt will be made to validate the certificate.

If you would prefer to require the MX host have a validating certificate even
when the published MTA-STS policy mode is `testing` then change

    $mtaTestingAsEnforce = false;

to

    $mtaTestingAsEnforce = true;

I do NOT recommend making that change. Mailbox domain system administrators
have direct control over the published MTA-STS mode, they can change it to
`enforce` when they are confident their policies regarding certificate renewal
are properly set up to avoid issues of problematic certificates.

#### STARTTLS Everywhere Testing Mode

The same applies with domains that have a published STARTTLS Everywhere policy.
When in `testing` mode, the connection is still *suppose* to take place even if
the certificate does not actually validate.

However at this time, *every single* domain listed in STARTTLS Everywhere has a
policy mode of `testing`.

The default behavior of this PHP script with STARTTLS Everywhere `testing` mode
matches that of MTA-STS, a policy of `encrypt` will be used. However I actually
do recommend changing it, at least until STARTTLS Everywhere starts assigning a
policy of `enforce` to some domains.

System administrators have no control over the mode assigned by STARTTLS
Everywhere, but if they are listed it is because either they are a *major*
provider of e-mail or because they *chose* to be listed.

Therefore I recommend changing the default so that certificate validation can
actually take place, I just did not feel comfortable setting that as the
default.

To require validation of X.509 certificates with mailbox domains that have a
STARTTLS Everywhere policy mode of `testing` change

    $starttlsTestingAsEnforce = false;

to

    $starttlsTestingAsEnforce = true;

#### DANE Fail List

If the `smtp_tls_security_level` in your Postfix `main.cf` file is set to
`dane` and Postfix tries to send a message to a server that has an improperly
configured `TLSA` record, it is quite probable that the message will bounce.

A list of problematic mailbox domains is maintained at
[The DANE Fail List](https://github.com/danefail/list/blob/master/dane_fail_list.dat)
and is incorporated into this project.

For mailbox domains specifically being monitored by this project that are
identified as DANE domains and are in the list, the generated policy always
compensates. However for mailbox domains that are not specifically being
monitored in that table, delivery failure will still occur when the default
Postfix policy is set to `dane`.

For that reason, these domains are by default added to the generated policy
file with an assigned security level of either `encrypt` or `may`.

If you are already using a default Postfix policy of either `may` or `encrypt`
then this is not necessary, and you can optionally change

    $addBadDane = true;

to

    $addBadDane = false;

This will result in a smaller policy map without reducing security. However if
you ever do set your default Postfix policy to `dane` then there may on
occasion be delivery problems if you change this setting to `false`.

#### Bad DANE Policy Mode

When a domain is listed in the `improperdane` table, the policy for that domain
by default is `encrypt`. Some system administrators may prefer a more liberal
policy of `may` in those cases. I recommend against this as a policy of `may`
can allow an attacker to prevent discovery of TLS capabilities and I find it
highly unlikely that MX servers that have mis-configured `TLSA` records would
not have TLS, but if you want to make that change, change

    $failedDanePolicyMode = 'encrypt';

to

    $failedDanePolicyMode = 'may';

and attempts to connect to those domains will use standard Opportunistic TLS
rather than the mandatory TLS the system administrator of those MX servers
tried to indicate they wanted when they added `TLSA` records to their DNS.