PKIX Chain of Trust
===================

With DANE, the Chain of Trust needed to validate X509 certificates is taken
care of by the local DNSSEC enforcing Recursive Resolver, we do not have to do
anything other than make sure it actually works.

With PKIX we do not have that lurury. To validate a certificate, an application
has to be explicitly told what Certificate Authority root Trust Anchors it
should trust. There is no standard list for this.

Most UNIX-like Operating Systems ship with a CA Trust Anchor bundle in PEM
format, typically based off of the bundle maintained by Mozilla but there are
sometimes differences in which root Trust Anchors are trusted.

It is my *opinion* that the safest thing to do is use the certificate bundle
that is provided by your operating system, if it provides one. That bundle will
*hopefully* be updated by your operating system vendor on a timely basis, so
that new Trust Anchors are added and old Trust Anchors are removed.

* [Mozilla PEM Bundle as Extracted by the curl project](#mozilla-pem-bundle-as-extracted-by-the-curl-project)
* [The `checkDomains.py` Script](#the-checkdomainspy-script)
* [The `extractFromPostfix.py` Script](#the-extractfrompostfixpy-script)
* [Postfix Configuration](#postfix-configuration)


Mozilla PEM Bundle as Extracted by the curl project
---------------------------------------------------

If your operating system does not include a PEM encoded certificate bundle in
a single file or if you do not trust that it is being updated in a timely
manner, a second option is to use the Mozilla bundle as provide by curl at
[CA certificates extracted from Mozilla](https://curl.haxx.se/docs/caextract.html)

To keep it updated in a timely manner, you can use the shell script I provide
in the `extras` directory called `cacert-cron.sh`

As the root user:

    install -m755 extras/cacert-cron.sh /etc/cron.daily/
    chown root:root /etc/cron.daily/cacert-cron.sh
    sh /etc/cron.daily/cacert-cron.sh

That will retrieve the Mozilla bundle and install it at
`/etc/pki/tls/certs/mozilla-cacert.pem` and it will check for newer versions on
a daily basis to keep it up to date.

That bundle does not have a PGP signature to validate it has not been meddled
with, there is no guarantee that curl will continue to provide it, and there is
no guarantee it will continue to be updated in a timely manner.

As it will probably not differ significantly from the PEM bundle provided by
your operating system vendor, it is safer to use the bundle provided by your
operating system vendor if it provides one.

I have *never* heard of any security issues with the Mozilla bundle as
extracted by the curl project but it still is probably safer to just use your
operating system provided bundle.


The `checkDomains.py` Script
----------------------------

This script by default looks for the bundle located at
`/etc/pki/tls/certs/mozilla-cacert.pem` but will also look for a certificate
bundle at other locations where it is sometimes installed.

When the script does find a bundle, it will then test the bundle to make sure
that it contains the Trust Anchors needed to download the JSON data files and
the DANE fail list. The script will also check to see if the bundle contains
Trust Anchors commonly uses in association with the MTA-STS policy servers and
will warn you if any are missing. The warning messages are logged in the
database so they are available when the script is run in silent mode.

If you are using the cron script to retrieve the curl extracted Mozilla bundle
then it will find it and things will ‘just work’.

If you are not using the cron script, there is a decent chance it will find a
suitable bundle and things will still ‘just work’. However it still would be
a good idea to specify the precise location of the bundle you do want to use by
editing the python script and defining it by changing

    # path to certificate authority bundle
    cabundle = '/etc/pki/tls/certs/mozilla-cacert.pem'

to point to the PEM encoded Trust Anchor bundle you do want to use.


The `extractFromPostfix.py` Script
----------------------------------

This script by default looks for the bundle located at
`/etc/pki/tls/certs/mozilla-cacert.pem` but will also look for a certificate
bundle at other locations where it is sometimes installed.

The script only makes a connection if the MariaDB database is running on a
different host, and then it only connects to one specific URL that you have
configured it to connect to, so it really only needs a bundle that has the root
Trust Anchor needed to validate the certificate you use for that server.

If you are using the cron script to retrieve the curl extracted Mozilla bundle
then it will find it and things will ‘just work’.

If you are not using the cron script, there is a decent chance it will find a
suitable bundle and things will still ‘just work’. However it still would be
a good idea to specify the precise location of the bundle you do want to use.

As this script is ordinarily called by the
`99-cron-retrieveNewPostfixPolicy.sh` shell script, the easiest way to
specify a specific bundle is to edit that shell script and change

    /usr/bin/python /usr/local/libexec/extractFromPostfix.py \
      --redis-pass=mySecretPass \
      --post-url=https://example.net/importEmail.php \
      --silent

to

    /usr/bin/python /usr/local/libexec/extractFromPostfix.py \
      --redis-pass=mySecretPass \
      --post-url=https://example.net/importEmail.php \
      --pem=/path/to/your/cabundle.pem \
      --silent

If you are using a self-signed certificate, I *think* you can just point the
`--pem=` switch to a local PEM encoded copy of the self-signed certificate but
I have not personally tested that. Technically speaking all Trust Anchors are
themselves self-signed, so I think that should work.


Postfix Configuration
---------------------

Please see Step Three in the `PostfixRelayHowto.md` file.
