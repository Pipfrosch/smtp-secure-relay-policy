Postfix Relay Howto
===================

This HOWTO assumes that you already have Postfix installed and working as an
SMTP Relay that sends message to MX servers around the Internet.

You should be using a modern version of Postfix, for example the version that
shipped swith RHEL / CentOS 7 is too old. I myself use Postix 3.3.x on CentOS 7
with no issues (mine is built against LibreSSL but OpenSSL works too).

As of February 27, 2019 Postfix 3.0 is End of Life and Postfix 3.1 is the
oldest legacy version still supported by upstream. Yes, Enterprise Linux
distributions will continue to support older versions and backport security
fixes into them, but if you are using a so-called 'legacy' version of Postfix,
upgrade to something supported by Postfix upstream. As a mail administrator,
that really is your job.

This HOWTO assumes you already have a system running the MariaDB database with
the `checkDomains.py` script running once an hour so it is discovering
published security policies for mailbox domains it knows about.

This HOWTO assumes you have some familiarity with the UNIX command line and
know how to use a text editor such as vim, emacs, or pico.

* [Step One: Install and Configure a Local DNSSEC Enforcing Recursive Resolver](#step-one-install-and-configure-a-local-dnssec-enforcing-recursive-resolver)
* [Step Two: Configure Postfix for DANE Enforcement](#step-two-configure-postfix-for-dane-enforcement)
* [Step Three: Configure Postfix to Validate CA Signed Certificates](#step-three-configure-postfix-to-validate-ca-signed-certificates)
* [Step Four: Configure Postfix to Use A Policy File](#step-four-configure-postfix-to-use-a-policy-file)
* [Step Five: Automate Retrieval of Policy File](#step-five-automate-retrieval-of-policy-file)
* [Step Six: Discovery of Mailbox Domains to Monitor: Redis Database](#step-six-discovery-of-mailbox-domains-to-monitor-redis-database)
* [Step Seven: Discovery of Mailbox Domains to Monitor: Python Script](#step-seven-discovery-of-mailbox-domains-to-monitor-python-script)
* -- [LOGFILE location](#logfile-location)
* -- [`--logfile=/path/to/file` Switch](#-logfilepathtofile-switch)
* -- [`useRotatedLog()` Function](#userotatedlog-function)
* -- [`--use-current-log` Switch](#-use-current-log-switch)
* -- [Redis Configuration](#redis-configuration)
* -- -- [Other Redis configuration options](#other-redis-configuration-options)
* -- -- [`--delete-cache` Switch](#-delete-cache-switch)
* -- [MariaDB Configuration](#mariadb-configuration)
* -- [Submit via Post Configuration](#submit-via-post-configuration)
* -- [X509 Trust Anchor](#x509-trust-anchor)
* -- [Install `extractFromPostfix.py` Script](#install-extractfrompostfixpy-script)


Step One: Install and Configure a Local DNSSEC Enforcing Recursive Resolver
---------------------------------------------------------------------------

A local Recursive Resolver that enforces DNSSEC is __critical__ to the secure
operation of an SMTP Relay.

I personally recommend [Unbound](https://nlnetlabs.nl/projects/unbound/about/),
it is very high quality and well maintained and is already packaged for the
majority of UNIX-like operating systems in the vendor package repository.

Instructions for setting up Unbound are in the `Documentation/UnboundHowto.md`
file.

You can use a different DNSSEC enforcing recursive resolver, there is not a
technical reason why it must be Unbound, that is just the one I prefer.


Step Two: Configure Postfix for DANE Enforcement
------------------------------------------------

Now you are ready to configure Postfix to enforce DANE policies when it can.

This is relatively simple. As the root user (or via `sudo`):

    postconf -e "smtp_dns_support_level = dnssec"
    postconf -e "smtp_tls_security_level = dane"
    postfix reload

Note that if you run a server where it is *never* okay to send a message to an
MX host without encryption, then for the second command above, you should
instead do:

    postconf -e "smtp_tls_security_level = encrypt"

However, be warned that will result in delivery bounces when the remote MX
server does not support TLS, so only use that default security level when you
WANT such deliveries to be bounced.

Also be warned that in `encrypt` mode, Postfix will use encryption but will
not use either PKIX or DANE validation, with `dane` mode it does not always
encrypt but if Postfix detects the destination supports DANE it will for that
destination as well as enforcing DANE.

However in `encrypt` mode, you can use this project to set a DANE enforcement
policy for destination MX servers with detected DANE support.


Step Three: Configure Postfix to Validate CA Signed Certificates
----------------------------------------------------------------

By default, Postfix does not bother to validate X509 certificates that MX
servers present when it establishes an encrypted connection.

This may be shocking, but many MX servers simply use self-signed certificates
or certificates issued for a different domain. Even Google mail services did
this until quite recently, most MX server admins simply did not bother with
properly signed certificates because Opportunistic TLS where delivery of the
message takes precendence over security encourages sloppy server
administration.

Anyway, one of two directives needs to point to a certificate bundle that
Postfix can use: `smtp_tls_CAfile` or `smtp_tls_CApath` - note that there is
no `d` after `smtp`, there are similar setting with a `d` for when you want
Postfix to require a *client* to connect with a certificate that validates.

If you have a certificate bundle where each Trust Anchor is in an individual
file then use `smtp_tls_CApath` to tell Postfix where that directory is. For
example:

    smtp_tls_CApath = /etc/postfix/certs

Note that to use that method, you have to create special hash links to the
individual files. For more information, see:

[man 5 postconf](http://www.postfix.org/postconf.5.html#smtp_tls_CApath)

If you have a certificate bundle with all the Trust Anchors in a single file,
then use something like this instead:

    smtp_tls_CAfile = /etc/pki/tls/certs/mozilla-cacert.pem

The correct path to use probably depends upon your vendor packaging.

Assuming you are using the Mozilla bundle described in the
`Documentation/PKIX-ChainOfTrust.md` documentation file:

    postconf -e "smtp_tls_CAfile = /etc/pki/tls/certs/mozilla-cacert.pem"
    postfix reload

Postfix will now be *capable* of verifying the X.509 certificates presented by
MX servers it talks to, although it will not try to do so except with domains
that it is specifically told to with a policy map file.


Step Four: Configure Postfix to Use A Policy File
-------------------------------------------------

As currently configured in the above three steps, Postfix will already do DANE
validation for mail servers that have a proper TLSA record, but domains that do
not implement DNSSEC signed TLSA records will still have no verification
whatsoever for the X.509 certificates they present, making them open to easy
MITM attacks even when an encrypted connection is established.

To fix this, for domains that have published proper security policies either by
the use of MTA-STS or via STARTTLS Everywhere need specific policies on a per
domain basis in a Postfix Policy Map file.

__Generating this map file with quality policy detection is the purpose of this
project.__

To configure postfix so that it will make use of the map file:

    touch /etc/postfix/smtp_tls_policymap
    postmap /etc/postfix/smtp_tls_policymap
    postconf -e "smtp_tls_policy_maps = hash:/etc/postfix/smtp_tls_policymap"
    postfix reload

The policy map is currently empty, but Postfix is now configured to use it. Of
course the directory `/etc/postfix` may not be correct for your system, it all
depends upon on where your Postfix is installed.


Step Five: Automate Retrieval of Policy File
--------------------------------------------

If you are running the MariaDB database on the same machine, then you do not
need to use a web server. Simply install the `policymap.php` script and the
`settings.inc.php` configuration script in `/usr/local/libexec/` and make sure
you have the `/usr/bin/php` command installed along with the PDO module and
the PHP MySQL module. On RHEL / CentOS 7 systems these are provided by the
`php-cli`, `php-pdo`, and `php-mysql` packages. PHP packages for PHP 7+ often
call the MySQL package `php-mysqlnd`.

Information on configuring the `settings.inc.php` file can be found in the
`DatabaseSetupHowto.md` file in the section titled
__Step Six: Optional Web Server Setup__.

As the `settings.inc.php` script contains database authentication information
it should only be readable by the root user when used as a shell script:

    chown root:root /usr/local/libexec/settings.inc.php
    chmod 600 /usr/local/libexec/settings.inc.php

If you are running the MariaDB database on a different machine, then you will
want to have the `policymap.php` file running in a web server on that machine.
In that case, you need to edit the `99-cron-retrieveNewPostfixPolicy.sh` and
change line 20:

    /usr/bin/curl -Os https://example.org/policymap.php

so that it points to the web server where you have the MariaDB database and the
`policymap.php` script.

In either case, whether you are running the MariaDB database on the same host
or a different host, place the `99-cron-retrieveNewPostfixPolicy.sh` script
in `/etc/cron.daily` and make sure it is executable:

    mv 99-cron-retrieveNewPostfixPolicy.sh /etc/cron.daily/
    chown root:root /etc/cron.daily/99-cron-retrieveNewPostfixPolicy.sh
    chmod 700 /etc/cron.daily/99-cron-retrieveNewPostfixPolicy.sh

The reason for the `700` permissions is because you may edit it to include
password information in a later step.

Make sure it works:

    sh /etc/cron.daily/99-cron-retrieveNewPostfixPolicy.sh
    cat /etc/postfix/smtp_tls_policymap

If it worked, that second command should fill your screen with domain policies
that are in the file.

If you want the policy map to be more responsive to detected policy changes
than once a day, you can put it in `/etc/cron.hourly` instead of in
`/etc/cron.daily`.

Postfix will now require tighter security when it relays messages to the
mailbox domains with policies defined in that file, and the policy file will be
updated on a daily basis.


Step Six: Discovery of Mailbox Domains to Monitor: Redis Database
-----------------------------------------------------------------

The Python script `extractFromPostfix.py` will parse the Postfix log file to
discover domains your server communicates with.

The Python script uses a Redis `key => value` store to keep track of mailbox
domains it has submitted for monitoring so that it does not resubmit the same
domains on a frequent basis.

Instructions for configuring Redis are in the
`Documentation/RedisSetupHowto.md` file.


Step Seven: Discovery of Mailbox Domains to Monitor: Python Script
------------------------------------------------------------------

The initial loading of the MariaDB database inserted some domains into the
database and the `checkDomains.py` script adds domains that are known to the
STARTTLS Everywhere project, but you want to monitor domains that your users
communicate with to detect their published security policies so that you can
apply them.

This is done with the script `extractFromPostfix.py` which looks at the
Postfix log file and extracts mailbox domains from it.

That python script may need some modification for your environment. Some thing
can also be configured at run time with switches.

### LOGFILE location

On Red Hat / Fedora systems, and presumably most UNIX-like systems, the Postfix
log file will be located at `/var/log/maillog`. If it is located at a different
location on your system, then you need to edit that definition in the script.

#### `--logfile=/path/to/file` Switch

To run the script against a *specific* log file once, you can use the
`--logfile=/path/to/file` switch. This switch is intended for initial setup
only, to manually call this script giving it the specific location of a
previously rotated log file so that it can extract mailbox domains contained
within. When this switch is used, it will not look for a rotated version of
the argument but will read the exact file given as an argument.

### `useRotatedLog()` Function

On Red Hat / Fedora systems, and presumably most UNIX-like systems, when the
log is rotated it is copied to `/var/log/maillog-YYYYMMDD`. If your system
handles the rotation of that log file differently, then you will need to edit
the `useRotatedLog()` function so that it adjusts the name of the rotated log
file according to how it is done on your system.

Note that the script actually looks for a log file with the previous day
timestamp, that is because this script is intended to be called once a day, so
looking for the previous day rotation rather than the current day avoids
needing to make sure the script runs after log rotations.

Also note that the log is often not rotated daily on low volume systems, that
is okay.

#### `--use-current-log` Switch

If you would rather have the script read from the current log rather than from
a rotated log, you can use the `--use-current-log` switch. The script will then
use the `LOGFILE` definition without an appended date stamp.

### Redis Configuration

At a minimum you will need to tell the script what password to use when it
connects to the Redis database. To set this in the script itself, change

    redispass = None

to

    redispass = 'mySecretPass'

You can also set the Redis password with with either the `--redis-pass=` long
switch or with the `-r` short switch (note the long switch requires an `=` but
the short switch does not)

Assuming you are using the `99-cron-retrieveNewPostfixPolicy.sh` script to
call the `extractFromPostfix.py`, defining the Redis password with a switch in
the cron script is probably the easiest thing to do.

#### Other Redis configuration options

If for some reason you need to change the `host` and/or `port` being used for
the Redis connection, then edit the `redisConnect()` function. Those connection
settings can not be set with a switch.

#### `--delete-cache` Switch

If for some reason you need to delete the Redis cache of mailbox domains that
have already been submitted to the database, call the `extractFromPostfix.py`
with the `--delete-cache` switch.

### MariaDB Configuration

If you happen to be running the MariaDB database on the same host as your
Postfix install, then you need to tell the `extractFromPostfix.py` script how
to authenticate to the database.

You can edit the script itself and change

    dbuser = None
    dbpass = None

to

    dbuser = 'YourMariaDBuser'
    dbpass = 'YourMariaDBpass'

where `YourMariaDBuser` is the username for the database connection and
`YourMariaDBpass` is the password.

Alternatively you can define the MariaDB username with the `--dbuser=` long
switch or the `-u` short switch, and you can define the corresponding MariaDB
password with `--dbpass=` long switch or the `-p` short switch.

Assuming you are using the `99-cron-retrieveNewPostfixPolicy.sh` script to
call the `extractFromPostfix.py`, defining the MariaDB authentication
information with switches in the cron script is probably the easiest thing to
do.

### Submit via Post Configuration

If the MariaDB database is on a different host, then you will need configure
the URL where you have the `importEmail.php` script that accepts the POST
submission.

You can edit the `extractFromPostfix.py` file and change

    postURL = None

to

    postURL = "https://host.yourdomain.tls/importEmail.php"

Alternative, you can set the URL with the `--post-url=` long switch. There is
no short switch for that option.

Assuming you are using the `99-cron-retrieveNewPostfixPolicy.sh` script to
call the `extractFromPostfix.py`, defining the URL to submit the POST request
to with the long switch in the cron script is probably the easiest thing to do.

#### X509 Trust Anchor

You will need to let the script know where to find a certificate bundle that
includes the Trust Anchor needed for the X509 certificate used by the web
server that serves the `importEmail.php` script.

Note that is the *only* URL the script connects to, so it does not need to
be a full certificate bundle, it is okay if it only contains a single PEM
for the root Trust Anchor needed. You can often get that from the Certificate
Authority that issued the certificate. Or just use a full bundle that has it.

To specify the certificate bundle, you can edit the `extractFromPostfix.py`
script and change line that reads:

    cabundle = '/etc/pki/tls/certs/mozilla-cacert.pem'

to instead point to the certificate bundle you will be using. Alternatively
you can use the `--pem=` long switch to the `extractFromPostfix.py` script to
specify what bundle it should use.

If you are using a self-signed certificate, I *suspect* you can just have a
copy of the self-signed certificate on the host running the
`extractFromPostfix.py` script (in PEM format) and point to that, but I have
not tried, I just use a Let’s Encrypt certificate.

Assuming you are using the `99-cron-retrieveNewPostfixPolicy.sh` script to
call the `extractFromPostfix.py`, and assuming you need to change the defined
certificate bundle, defining the certificate bundle to use with the long switch
in the cron script is probably the easiest thing to do.

### Install `extractFromPostfix.py` Script

Once you have made needed edits to the Python script, place it in the
`/usr/local/libexec` directory so that the bash cron script can find it. Since
it may contains passwords (for Redis and possibly for MariaDB) it should only
be readable by the `root` user:

    chown root:root extractFromPostfix.py
    chmod 700 extractFromPostfix.py
    [ ! -d /usr/local/libexec ] && mkdir -p /usr/local/libexec
    mv extractFromPostfix.py /usr/local/libexec/

Now when your Postfix log is rotated, the following day when the
`99-cron-retrieveNewPostfixPolicy.sh` script executes, it will also extract
email domains your users communicate with and submit them for policy detection.
If any of those domains have published policies that can be enforced, they will
be discovered and should appear in the policy map to be enforced the next day.
