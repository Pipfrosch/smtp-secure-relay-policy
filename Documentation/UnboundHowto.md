Unbound Howto
=============

The DNS system as originally designed is very insecure, it originally did not
provide any mechanism by which a system could validate that the responses it
received had not been modified somewhere in transit.

While the use of DNS over TLS (RFC 7858) does prevent modification between you
and the DNS resolver you use, it still does not guarantee the answer has not
been modified.

DNSSEC provides a mechanism by which the authoritative DNS server for a zone
can serve cryptographic signatures along with the responses so that the
validity of the responses can be validated by the system that made the request.

In the future, a combination of DNSSEC stub resolvers on the operating system
and DNS over TLS may alleviate the need for servers to run their own DNSSEC
enforcing recursive resolver, but in the present, *every single server on the
Internet should run its own DNSSEC enforcing recursive resolver.*

Presently only a small percentage of zones actually sign their DNS responses
but that number is growing, especially with mail servers where DANE for SMTP
greatly increases security against MITM attacks. DANE for SMTP *requires* that
the MX servers have `TLSA` records signed with DNSSEC and that the SMTP relay
connecting to them is able to validate those `TLSA` records against the DNSSEC
signature on the `TLSA RRset`.

A local Recursive Resolver that enforces DNSSEC is __critical__ to the secure
operation of a SMTP Relay.

I personally recommend [Unbound](https://nlnetlabs.nl/projects/unbound/about/),
it is very high quality and well maintained and is already packaged for the
majority of UNIX-like operating systems in the vendor package repository.

* [Unbound Configuration](#unbound-configuration)
* [`/etc/resolv.conf`](#etcresolvconf)
* [Testing DNSSEC Enforcement](#testing-dnssec-enforcement)


Unbound Configuration
---------------------

The default configuration for unbound is to enforce DNSSEC but it is possible
some distributiond change that default. In the configuration file (located at
`/etc/unbound/unbound.conf` on Red Hat / Fedora systems)

Make sure it has the following configuration parameters set:

    val-permissive-mode: no

For detailed instructions, see
[Howto enable DNSSEC](https://www.nlnetlabs.nl/documentation/unbound/howto-anchor/)
however *every* distribution package for Unbound that I have seen has it set
up properly to enforce DNSSEC from the start.

Make sure Unbound is configured to only listen on the localhost. This should be
the default (it will listen on `127.0.0.1` and `::1`) but make sure your vendor
did not change it. All of the lines in the configuration file that start with
the keyword `interface:` should be commented out, and you should have:

    interface-automatic: no

uncommented.

Configure the daemon to start at system boot, and start it. On Linux systems
that use `systemd` this is probably accomplished with the following commands:

    systemctl enable unbound.service
    systemctl start unbound.service

`/etc/resolv.conf`
------------------

Running the daemon is not enough, you have to tell the operating system to
use it. The contents of the `/etc/resolv.conf` file is traditionally where
UNIX systems configure what DNS server they use for name resolution, it
should contain something like the following:

    nameserver 127.0.0.1
    nameserver ::1

That instructs it to use the local nameserver you just installed rather than
using a remote nameserver that is susceptible to MITM attacks on your queries.

Unfortunately that file is often rewritten by the DHCP server whenever the
system boots and grabs its IP address.

On RHEL/CentOS 7 this can *usually* be fixed by editing the file

    /etc/sysconfig/network-scripts/ifcfg-eth0

Make sure it has the following:

    PEERDNS=no
    IPV6_PEERDNS=no
    DNS1=127.0.0.1
    DNS2=::1

Reboot the server to make sure the setting do what you are expecting (to make
sure the appropriate `/etc/resolv.conf` file is generated) and if that did not
do it, well, consult your hosting company, sometimes they have funky
configurations in their VM images. Also, I do not have experience with network
configuration in other Linux distributions, I pretty much stick with Red Hat
world.

Testing DNSSEC Enforcement
--------------------------

To test that Unbound is running in DNSSEC enforcing mode:

    dig SOA proper.dnssec.icu @127.0.0.1 |grep "HEADER"

You should get a result like this:

    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3012

Then test with a signed zone where validation should fail:

    dig SOA invalid-ds.dnssec.icu @127.0.0.1 |grep "HEADER"

You should get a result like this:

    ;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 57272

Congratulations, your server is now safer from forged DNS responses. At least
for zones that actually sign their results, but that number is growing. It
seems to be growing fastest in Europe, countries like America where a few
monopolies run almost all tech tend to be slower at adopting better security
practices as huge income inequality results, so the companies that are not the
big monopolies have less resources to implement best practices but have to
outsouece to the large monopolies, which then have no market pressure to
implement best practices themselves.

However adoption is growing. Not as fast as I would like, but it is growing.
