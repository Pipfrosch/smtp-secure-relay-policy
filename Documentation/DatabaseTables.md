Description of Database Tables
==============================

This file described the database table specification.

The Database Table structure for the SMTP Secure Relay Policy project is now
stable. It will not change before the Beta Release and it probably will not
change for some time.

If and when it does eventually change, an upgrade script will be provided that
can migrate the specification here to whatever changes are needed.

A description of the tables:

* [`metadata` Table](#metadata-table)
* [`mboxdomains` Table](#mboxdomains-table)
* [`mtacache` Table](#mtacache-table)
* [`stlscache` Table](#stlscache-table)
* [`pkixprovider` Table](#pkixprovider-table)
* [`daneprovider` Table](#daneprovider-table)
* [`deprecatedtls` Table](#deprecatedtls-table)
* [`policyblacklist` Table](#policyblacklist-table)
* [`improperdane` Table](#improperdane-table)
* [`lackstls` Table](#lackstls-table)
* [`logmessages` Table](#logmessages-table)
* [`rootanchor` Table](#rootanchor-table)
* -=-=-=-
* [INITIAL TABLE ROWS](#initial-table-rows)



`metadata` Table
----------------

This primary purpose of this table is to allow scripts that update or downdate
the database structure to know how to do the right things based on the existing
database structure.

__codename__ Column: This column contains a human readable codename for the
  release. It is not used for a functional purpose.

__majorv__ Column: This column contains the major version for the release. It
  does not change very often. The structure of the `metadata` table and of the
  `logmessages` table are guaranteed to be stable between versions that have
  the same value for the major version.
  
  The major version will be an integer value between 0 and 65535 inclusive, but
  likely will not change more than once a year.

__minorv__ Column: This column contains the minor version for the release. When
  it is an odd number, it should be considered a pre-release. It will always
  have a value between 0 and 255 inclusive *unless* the `majorv` is 0 in which
  case it will always have a value between 1 and 255 inclusive.

__compatv__ Column: This column contains a hex encoding of the minimum version
  of the release that Python and PHP scripts must be from to properly work with
  the database. It exists to allow scripts to easily determine if a compatible
  database version is being used. It only changes when changes to the database
  table structures are made that are not compatible with previous versions.
  
  For example, if a new table or column are added that do not prevent older
  scripts from working, then the value is not incremented. However scripts that
  do need the new tables or columns would have to also check the `majorv`, the
  `minorv`, and the `patchv` values.
  
  The first two bytes represent the major version, the third byte represents
  the minor version, and the fourth byte represents the patch version.
  
  It will also be used with database upgrade scripts when a new release changes
  the database version.

__variant__ Column: This is an open source project. Some people may want to
  create forks that add features, and those forks may require additional tables
  and/or additional table columns. The forks may be assigned a number for their
  project so that their scripts can verify the database being used is properly
  set up the queries it needs to make. This value will always be 0 for releases
  from this project.

__vpatch__ Column: This allows the previously mentioned forks, should they
  happen, to specify their own revision numbers within their fork. This value
  will always be 0 for releases from this project.

__active__ Column: This will be have a value of 0 or 1. I do not personally
  like the boolean data type, some libraries that cache queries use boolean
  types incorrectly and will return false when a value does not exist which
  causes issues if the value exists but is false. Yes those issues can be
  coded around but I prefer to just avoid them by using an integer value of
  0 for false and an integer value of 1 for true.
  
  Only one row may have an integer value of 1. When a row has a value of 0
  it is only still present in the database so that historical data exists for
  when the database was upgraded.
  
  Database table update scripts should first do a dump of the database to flat
  file, then assign a value of 0 to this column for all rows, then perform the
  update to the tables, and upon successful completion of the update then and
  only then assign a value of 1 to the current structure.

__loaddate__ Column: This column holds the date the current database structure
  was either loaded or upgraded from a previous version.


`mboxdomains` Table
-------------------

This table contains the list of mailbox domains that are being monitored for a
secure policy.

__domain__ Column: This column has the mailbox domains that are being monitored
  for a current security policy.

__validmx__ Column: This column will have a value of 1 if a valid `MX`, `A`, or
`AAAA` record has been detected for the domain. Otherwise it will be a 0.

__dnssec__ Column: This column will have a value of 1 if the mailbox domain is
detected to be in a DNSSEC signed zone.

__dane__ Column: This column will have a value of 1 if the mailbox domain is in
  a zone protected by DNSSEC *and* more than 55% of the MX hosts have a valid
  `TLSA` record in a zone that is signed by DNSSEC.

__mtasts__ Column: When the `dane` column is 0, this column will have a value
  of 1 if a valid MTA-STS policy of either `testing` or `enforce` has been
  detected. Otherwise it will have a value of 0. Mailbox domains that are 
  protected by DANE are not evaluated for MTA-STS policy.

__starttls__ Column: When a domain has been submitted to the STARTTLS
  Everywhere project with an actual policy listed by that project rather than
  a policy alias, this column will have a value of 1. Otherwise it will have a
  value of 0.

__pkixalias__ Column: When all the MX hosts for a mailbox domain match the
  pattern of a third party MX service provider known to use modern TLS with
  X509 certificates that validate through the PKIX Chain of Trust, the name of
  the provider will be present in this column.

__danealias__ Column: When all the MX hosts for a mailbox domain match the
  pattern of a third party MX service provider known to use modern TLS with
  X509 certificates that validate through the DANE Chain of Trust, the name of
  the provider will be present in this column.

__lastcheck__ Column: A UTC timestamp for the last time a security policy was
  sniffed for the domain. When it has never been sniffed or when a valid MX
  can not be detected, the timestamp `1971-01-01 00:00:00` is used.

__mxfail__ Column: A UTC timestamp for the last time an attempt to query an
  `MX` record was made and failed. This column is not currently used. The
  timestamp is inserted, but my code does not do anything useful with it.

__insdate__ Column: A UTC timestamp for the date the domain was added for
  monitoring.

__lasthit__ Column: A UTC timestamp for the last time the domain was submitted
  for monitoring. It is used by the janitor service. When a domain does not
  have a detected security policy and has not been submitted for monitoring in
  at least six months, it will be removed from monitoring.

__failcount__ Column: Starts at zero. Every time the domain fails to resolve or
  a valid `MX` record (or `A`/`AAAA` record) is not found, this column is
  incremented. On success, it resets to 0.
  
  The higher the number, the lower the priority in sniffing so that abandoned
  domains spammers use and drop do not clog up sniffing of domains actually in
  use. If the count reaches 36, it is removed from the database. That gives
  plenty of time for domains with temporary DNS problems to solve their
  problems before having their domain deleted while still removing the bogus
  domains fairly quickly.


`mtacache` Table
----------------

When an MTA-STS policy is discovered, this table caches the policy so that it
does not need to be retrieved again until the cache expires.

__domain__ Column: The mailbox domain name for the cached policy.

__version__ Column: The version of MTA-STS the policy is defined for. Right now
  the only version that exists is `STSv1`.

__policymx__ Column: A colon delimited list of MX hosts to match against.

__cleanmx__ Column: A colon delimited list of MX hosts from the `MX RRset`
  that match what is defined within the `policymx` column. This list is
  updated frequently even when the cache for the policy itself has not yet
  expired.

__serial__ Column: The serial number for the MTA-STS policy as defined in the
  DNS record.

__cachelife__ Column: The length of time in seconds the policy is to be
  considered valid before an update to the policy should be checked for.

__mode__ Column: The MTA-STS policy mode. It will be a value of `none`,
  `testing`, or `enforce`.

__expires__ Column: A UTC timestamp for when the current cached policy should
  be considered expired and a new policy checked for.


`stlscache` Table
-----------------

When a mailbox domain is listed with the STARTTLS project, this table holds the
necessary information to create a security policy in the event that a DANE or
MTA-STS policy is not sniffed.

__domain__ Column: The mailbox domain name for the cached policy.

__policymx__ Column: A colon delimited list of MX hosts to match against.

__cleanmx__ Column: A colon delimited list of MX hosts from the `MX RRset`
  that match what is defined within the `policymx` column. This list is
  updated frequently.

__mode__ Column: The MTA-STS policy mode. It will be a value of `none`,
  `testing`, or `enforce`.

__expires__ Column: A UTC timestamp for when the current cached policy should
  be considered expired. This value is taken from the STARTTLS Everywhere JSON
  data file, as the date that file is set to expire. The JSON file is retrieved
  about every twenty-six hours, under normal conditions a record will be
  updated before it ever expires.

__lastcheck__ Column: A UTC timestamp indicating when the JSON policy file for
  the STARTTLS Everywhere list was obtained. When a new policy list is
  obtained, any domain formerly cached that is not updated with the current
  timestamp is removed from this table.


`pkixprovider` Table
--------------------

Many mailbox domains outsource their mail services to third party providers.
For example, if you send an e-mail to `gradadm@berkeley.edu` the mailbox domain
is `berkeley.edu` but the mail is actually handled by Google services:

    dig MX +short berkeley.edu
    1 aspmx.l.google.com.
    5 alt2.aspmx.l.google.com.
    5 alt1.aspmx.l.google.com.
    10 alt3.aspmx.l.google.com.
    10 alt4.aspmx.l.google.com.

I think it is sad that one of the most influential Universities in the
development of the Internet does not handle its own mail services, but that may
just be me being nostalgic. Anyway...

When we know these third party services offer both modern TLS with quality
cipher suites and have X509 certificates signed by reputable well-known
Certificate Authorities with Trust Anchors that are commonly trusted, then we
can create secure policies for mailbox domains we can identity as using those
MX service providers. The `pkixprovider` table stores the information we need
to identify which mailbox domains use them.

__aliaskey__ Column: What we internally call the provider. This is what will be
  placed in the `pkixalias` column in the `mboxdomains` table for mailbox
  domains identified as using the provider.

__policymx__ Column: A colon delimited list of patterns to match against when
  determining of a mailbox domain uses the provider.

__lastcheck__ Column: A UTC timestamp indicating the last time we verified the
  provider meets the requirements for a secure policy.

__expires__ Column: A UTC timestamp after which point the row should be deleted
  if it has not been updated by then.

__insdate__ Column: A UTC timestamp for when the record was created or last
  updated.


`daneprovider` Table
--------------------

This is similar to the `pkixprovider` table except it is for MX service
providers that provide DANE validation for the X509 certificates they use.

The columns are identical to what is in the `pkixprovider` table. I will not
repeat them here.


`deprecatedtls` Table
---------------------

When information from the above tables is used to generate secure policies, the
assumption is made that the MX hosts support TLS 1.2 with quality cipher
suites. Indeed that is required by the MTA-STS specification.

Unfortunately that is not always the case. The `deprecatedtls` table allows you
to specify mailbox domains for which more lenient requires with respect to the
TLS version and cipher suite must be specified.

Domains are *never* automatically added to this table. When you encounter mail
that can not be delivered to a domain because it has a secure policy that can
be sniffed but does not have an adequate version of TLS or adequate cipher
suites, you have to manually enter the mailbox domain into this table.

However domains are automatically removed from the table when they have been in
the table for a year.

__domain__ Column: The mailbox domain that weaker TLS is to be tolerated for.

__insdate__ Column: The UTC timestamp for when the entry was created. This will
  automatically be created when the domain name is inserted.


`policyblacklist` Table
-----------------------

Sometimes a mailbox domain publishes an MTA-STS policy or has a STARTTLS
Everywhere policy that it does not actually comply with. This is usually the
result of sloppy system administration. What happens is they publish a policy
for their MX servers, then they switch to a different provider but do not
update the MTA-STS or STARTTLS Everywhere policy.

Domains are *never* automatically added to this table. When you encounter a
domain with a published policy that does not match what their `MX RRset` uses
__and__ you are sure the `MX RRset` is not fraudulent, you can manually add the
domain to this list and the published policy will not be used.

However domains are automatically removed from the table when they have been in
the table for six months.

__domain__ Column: The mailbox domain that does not have an `MX RRset` that
  matches their published policy.

__insdate__ Column: The UTC timestamp for when the entry was created. This will
  automatically be created when the domain name is inserted.


`improperdane` Table
--------------------

Sometimes an MX host has a `TLSA RRset` where none of the fingerprints match
the X509 certificate the host is using. That can be a MITM attempt but it also
often results from sloppy system administration, the administrator updates
their X509 certificate without updating the `TLSA` record set to match.

This table is updated using the [The DANE Fail List](https://danefail.org/) but
you also can manually enter domains you are sure are poor system administration
rather than attack taking place if you want.

Manually entered domains are automatically deleted after fifteen days, entries
from The DANE Fail List are deleted after four days if they have not been
updated or automatically removed before them.

__domain__ Column: The mailbox domain that should have a policy that does not
  use DANE applied to it.

__danefail__ Column: This column will have a value of 1 if the entry was made
  from the Dane FAIL list, otherwise it is 0.

__insdate__ Column: The timestamp for when the entry was created.


`lackstls` Table
----------------

While they are becoming rare, there are still mailbox domains for which the MX
servers simply do not support TLS. By default, most SMTP Relays will send mail
to them in a clear channel without encryption. However if you have your SMTP
Relay configured to require encryption by default but you still want to be able
to send messages to these domains, you can add them to this table.

This table is not automatically updated, domains have to be manually added to
it. Note that if a secure policy is discovered for any domain in this table,
the secure policy takes precedence.

However when a secure policy has *not* been discovered, then a policy will be
generated that tells your SMTP Relay it is okay to send messages to the domain
without encryption. In Postfix, this would be the Opportunistic TLS policy of
`may`.

Entries are automatically deleted from this table when they are a year old.

__domain__ Column: This column is for the mailbox domain where insecure clear
channel communication is allowed.

__insdate__ Column: The timestamp for when the entry was created.


`logmessages` Table
-------------------

Ordinarily the scripts used in this project are executed by the `cron` daemon
and are run in a silent mode where warnings and error messages are suppressed.

This table allows the scripts to insert log entries that you can either view by
logging into the MariaDB database, or in an e-mail that is sent to you once a
week.

Messages are ordinarily deleted from this table when they are over 28 days old.

__tstamp__ Column: The UTC time stamp for when the entry was made.

__type__ Column: The severity of the message. It will typically be `Error` for
something serious, `Warning` for something you should no about but is not
fatal, or `Notice` for a general message. Normally messages that would have a
type of `Notice` are not logged in the database, but in some cases they are.

__mode__ Column: The verbosity mode of the script when the log message was
inserted into the database. A value of `noisy` indicates a Python script that
was also spitting out messages to the console. You may have already seen the
message. A value of `silent` indicates a python script that was suppressing
output. It was not output to the console. A value of `web API` indicates the
message was generated by a PHP script.


`rootanchor` Table
------------------

The two JSON scripts, the DANE Fail List, and all of the MTA-STS policies are
retrieved from secure web servers that __must__ validate through the PKIX Chain
of Trust or the resource can not be trusted and can not be used.

This table contains the Subject Key Identifier (typically a 20 byte integer)
associated with the Certificate Authority Trust Anchors that are required for
the JSON scripts and the DANE Fail List, and the Subject Key Identifiers that
are most commonly used for MTA-STS policies.

This allows the `checkDomains.py` script to exit if your CA Certificate Bundle
does not contain the Trust Anchors needed for the JSON scripts and the DANE
Fail List, and to warn you when it lacks a Trust Anchor commonly used for
MTA-STS policy servers.

Only Subject Key Identifiers from Trust Anchors that are currently trusted by
Mozilla will be included, and only for the Trust Anchors that are most commonly
used by MTS-STS policy servers (plus the three needed for the JSON files and
for the DANE fail list). It will not be exhaustive, just enough to make sure
your CA bundle is modern enough to discover the vast majority of MTA-STS
policies and warn you if it is not.

__common__ Column: In the vast majority of cases, this will be identical to
  what is specified in the `CN` field of the Trust Anchor.

__ski__ Column: The 20-byte Subject Key Identifier for the needed Trust Anchor,
  hex encoded as a 40 character string.

__required__ Column: For a Trust Anchor required to retrieve the JSON files and
  the DANE Fail List, this value will be 1. The `checkDomains.py` script will
  terminate if your CA Certificate Bundle does not include matching Trust
  Anchors. All others will have a value of 0 and only a warning will be issued
  if your CA Certificate Bundle does not include matching Trust Anchors.

__expires__ Column: The UTC timestamp for when the corresponding Trust Anchor
  expires, so that it can be automatically removed from the table if it has not
  already been removed.

__insdate__ Column: A UTC timestamp for when the record was created or last
  updated.


INITIAL TABLE ROWS
==================

When the `setup.sql` file is imported, some rows are inserted.


__metadata__ Table: A single row describing the current version of the database
  table structure.

__rootanchor__ Table: Three rows containing the Subject Key Identifiers needed
  to retrieve the two JSON files and the DANE Fail List. Additional entries
  will be added by one of the two JSON files.

__mboxdomains__ Table: 161 rows containing an initial list of mailbox domains
  to monitor. Over 300 additional domains will be added by the STARTTLS
  Everywhere JSON file, and when you set up the scripts to extract domains from
  your mail server logs, that table will really grow.
