<?php
/*
  (c) 2019 Michael A. Peters except where otherwise noted.
  MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE
*/

// set this to the full path of wherever you put the settings.inc.php file
//  if it is put in different directory than this script.
$settingsFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.inc.php';

// Nothing below needs modification unless you don't like my logic.
if(php_sapi_name() === 'cli') {
  $phpcli = true;
} else {
  $phpcli = false;
}
if(! file_exists($settingsFile)) {
  if($phpcli) {
    print ('Could not locate settings file.' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
require($settingsFile);

if(! isset($pdo)) {
  if($phpcli) {
    print('No PDO object. This should not have happened' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
if(! checkDatabaseTableVersion($pdo, 'importEmail.php', '00000100')) {
  if($phpcli) {
    print('Incorrect Database Table Version' . "\n");
  } else {
    header("HTTP/1.1 500 Internal Server Error");
  }
  exit(1);
}
if(! isset($mtaTestingAsEnforce)) {
  $mtaTestingAsEnforce = false;
}
if(! isset($starttlsTestingAsEnforce)) {
  $starttlsTestingAsEnforce = false;
}
if(! isset($addBadDane)) {
  $addBadDane = true;
}
if(! isset($failedDanePolicyMode)) {
  $failedDanePolicyMode = 'encrypt';
}

if(! $phpcli) {
  header("Content-Type: text/plain");
}
$weakTLS = array();
$sql='SELECT domain FROM deprecatedtls';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $weakTLS[] = $result->domain;
  }
}
$badDane = array();
$sql='SELECT domain FROM improperdane';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $badDane[] = $result->domain;
  }
}
$insecure = array();
$securePolicy = array();
$sql='SELECT domain FROM lackstls';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  foreach($rs as $result) {
    $insecure[] = $result->domain;
  }
}

function getProtocolCiphers($domain, $arr) {
  $return = array();
  if(in_array($domain, $arr)) {
    $return[] = 'TLSv1.0';
    $return[] = 'medium';
  } else {
    $return[] = 'TLSv1.2';
    $return[] = 'high';
  }
  return $return;
}
function getDaneStatus($domain, $arr, $badMode) {
  if(in_array($domain, $arr)) {
    return $badMode;
  }
  return "dane-only";
}

$now = time();
$yest = ($now - 86400);
$yesterday = date('Y-m-d H:i:s', $yest);
$today = date('Y-m-d H:i:s', $now);

// mailbox domains where both MX RRset is DNSSEC protected and > 55% MX are DANE protected
$sql='SELECT domain FROM mboxdomains WHERE dane = 1 AND lastcheck > "' . $yesterday . '" ORDER BY domain';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# DANE only" . "\n");
  foreach($rs as $result) {
    $p = getProtocolCiphers($result->domain, $weakTLS);
    $d = getDaneStatus($result->domain, $badDane, $failedDanePolicyMode);
    print($result->domain . ' ' . $d . ' protocols=' . $p[0] . ' ciphers=' . $p[1] . "\n");
    $securePolicy[] = $result->domain;
  }
}

// These are mailbox domains that use a MX provider which provides DANE validated MX hosts, but the mailbox domain itself is not DNSSEC signed
$sql='SELECT mboxdomains.domain,mboxdomains.rawmx FROM mboxdomains,daneprovider WHERE mboxdomains.danealias = daneprovider.aliaskey AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 ORDER BY mboxdomains.danealias,mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# well-known DANE MX providers but MX RRset Not Signed" . "\n");
  foreach($rs as $result) {
    $p = getProtocolCiphers($result->domain, $weakTLS);
    $d = getDaneStatus($result->domain, $badDane, $failedDanePolicyMode);
    print($result->domain . ' ' . $d . ' protocols=' . $p[0] . ' ciphers=' . $p[1] . "\n");
    $securePolicy[] = $result->domain;
  }
}

// MTA-STS mode = enforce
$sql='SELECT mboxdomains.domain,mtacache.cleanmx FROM mboxdomains,mtacache WHERE mboxdomains.domain = mtacache.domain AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 1 AND mtacache.expires > "' . $today . '" AND mtacache.mode = "enforce" ORDER BY mtacache.domain';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# MTA-STS enforce mode" . "\n");
  foreach($rs as $result) {
    $p = getProtocolCiphers($result->domain, $weakTLS);
    print($result->domain . ' secure protocols=' . $p[0] . ' ciphers=' . $p[1] . ' match=' . $result->cleanmx . "\n");
    $securePolicy[] = $result->domain;
  }
}

// MTA-STS mode = testing
$sql='SELECT mboxdomains.domain,mtacache.cleanmx FROM mboxdomains,mtacache WHERE mboxdomains.domain = mtacache.domain AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 1 AND mtacache.expires > "' . $today . '" AND mtacache.mode = "testing" ORDER BY mtacache.domain';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# MTA-STS testing mode" . "\n");
  if($mtaTestingAsEnforce) {
    foreach($rs as $result) {
      $p = getProtocolCiphers($result->domain, $weakTLS);
      print($result->domain . ' secure protocols=' . $p[0] . ' ciphers=' . $p[1] . ' match=' . $result->cleanmx . "\n");
      $securePolicy[] = $result->domain;
    }
  } else {
    foreach($rs as $result) {
      $p = getProtocolCiphers($result->domain, $weakTLS);
      print($result->domain . ' encrypt protocols=' . $p[0] . ' ciphers=' . $p[1] . "\n");
      $securePolicy[] = $result->domain;
    }
  }
}

// These are mailbox domains that use an MX provider known to have PKIX validating MX servers
$sql='SELECT mboxdomains.domain,mboxdomains.rawmx FROM mboxdomains,pkixprovider WHERE mboxdomains.pkixalias = pkixprovider.aliaskey AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 AND mboxdomains.danealias IS NULL ORDER BY mboxdomains.pkixalias,mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# STARTTLS Everywhere well-known providers" . "\n");
  foreach($rs as $result) {
    $p = getProtocolCiphers($result->domain, $weakTLS);
    print($result->domain . ' secure protocols=' . $p[0] . ' ciphers=' . $p[1] . ' match=' . $result->rawmx . "\n");
    $securePolicy[] = $result->domain;
  }
}

// STARTTLS Everywhere mode = enforce
// this will presently have 0 rows because of the enforce mode requirement
$sql='SELECT mboxdomains.domain,stlscache.cleanmx FROM mboxdomains,stlscache WHERE mboxdomains.domain = stlscache.domain AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 AND mboxdomains.pkixalias IS NULL AND mboxdomains.danealias IS NULL AND stlscache.mode = "enforce" AND stlscache.cleanmx IS NOT NULL order by mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# STARTTLS Everywhere enforce mode" . "\n");
  foreach($rs as $result) {
    $p = getProtocolCiphers($result->domain, $weakTLS);
    print($result->domain . ' secure protocols=' . $p[0] . ' ciphers=' . $p[1] . ' match=' . $result->cleanmx . "\n");
    $securePolicy[] = $result->domain;
  }
}

// STARTTLS Everywhere mode = testing
$sql='SELECT mboxdomains.domain,stlscache.cleanmx FROM mboxdomains,stlscache WHERE mboxdomains.domain = stlscache.domain AND mboxdomains.dane = 0 AND mboxdomains.mtasts = 0 AND mboxdomains.pkixalias IS NULL AND mboxdomains.danealias IS NULL AND stlscache.mode = "testing" AND stlscache.cleanmx IS NOT NULL order by mboxdomains.id';
$q = $pdo->query($sql);
if ($rs = $q->fetchAll()) {
  print("# STARTTLS Everywhere testing mode" . "\n");
  if($starttlsTestingAsEnforce) {
    foreach($rs as $result) {
      $p = getProtocolCiphers($result->domain, $weakTLS);
      print($result->domain . ' secure protocols=' . $p[0] . ' ciphers=' . $p[1] . ' match=' . $result->cleanmx . "\n");
      $securePolicy[] = $result->domain;
    }
  } else {
    foreach($rs as $result) {
      $p = getProtocolCiphers($result->domain, $weakTLS);
      print($result->domain . ' encrypt protocols=' . $p[0] . ' ciphers=' . $p[1] . "\n");
      $securePolicy[] = $result->domain;
    }
  }
}

// BAD DANE
if($addBadDane) {
  $sql='SELECT improperdane.domain FROM improperdane WHERE improperdane.domain NOT IN (SELECT mboxdomains.domain from mboxdomains) ORDER BY domain';
  $q = $pdo->query($sql);
  if ($rs = $q->fetchAll()) {
    print("# NON-monitored domains with known bad DANE records" . "\n");
    foreach($rs as $result) {
      $p = getProtocolCiphers($result->domain, $weakTLS);
      print($result->domain . ' ' . $failedDanePolicyMode . ' protocols=' . $p[0] . ' ciphers=' . $p[1] . "\n");
      $securePolicy[] = $result->domain;
    }
  }
}

// 
$insecurePolicy = array();
foreach($insecure as $domain) {
  if(! in_array($domain, $securePolicy)) {
    $insecurePolicy[] = $domain;
  }
}

if (count($insecurePolicy) > 0) {
  print("# Whitelist of domains where TLS is not required." . "\n");
  foreach($insecurePolicy as $domain) {
    print($domain . ' may protocols=TLSv1.0 ciphers=medium' . "\n");
  }
}

if ($phpcli) {
  $message = 'Policy map generated via php-cli ';
} else {
  $message = 'Policy map generated at request of IP address ' . $_SERVER['REMOTE_ADDR'];
}
logMessage($pdo, 'Notice', 'policymap.php', $message);

?>