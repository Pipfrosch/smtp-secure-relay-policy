#!/bin/bash
#  change https://example.org/policymap.php to whatever URL you have for
#  for the policy map (line 20) and put this script in /etc/cron.daily/

# (c) 2019 Michael A. Peters except where otherwise noted.
# MIT license, see https://gitlab.com/Pipfrosch/smtp-secure-relay-policy/raw/master/LICENSE

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as the root user"
  exit 1
fi

TMP=`mktemp -d /tmp/mtapolicy.XXXXXXXX`
trap "rm -rf ${TMP}" EXIT TERM

pushd ${TMP} > /dev/null 2>&1
if [ -f /usr/local/libexec/policymap.php ]; then
  /usr/bin/php /usr/local/libexec/policymap.php > policymap.php
else
  /usr/bin/curl -Os https://example.org/policymap.php
  if [ $? -ne 0 ]; then
    exit 1
  fi
fi
n=`/bin/wc -l policymap.php |/bin/cut -d" " -f1`
if [ $n -gt 50 ]; then
  DEFAULTDANE=0
  if [ -x /usr/sbin/postconf ]; then
    DEFAULTDANE=`/usr/sbin/postconf smtp_tls_security_level |head -1 |grep -c "dane"`
  fi
  if [ "x${DEFAULTDANE}" == "x1" ]; then
    /bin/grep -v "dane-only protocols" policymap.php > /etc/postfix/smtp_tls_policymap
  else
    /bin/cat policymap.php > /etc/postfix/smtp_tls_policymap
  fi
  if [ -x /usr/sbin/postmap ]; then
    /usr/sbin/postmap /etc/postfix/smtp_tls_policymap
  else
    echo "Could not find /usr/sbin/postmap"
  fi
fi
popd > /dev/null 2>&1

# upload domains extracted from log file
if [ -f /usr/local/libexec/extractFromPostfix.py ]; then
  /usr/bin/python /usr/local/libexec/extractFromPostfix.py \
    --redis-pass=mySecretPass \
    --post-url=https://example.net/importEmail.php \
    --silent
fi

exit 0